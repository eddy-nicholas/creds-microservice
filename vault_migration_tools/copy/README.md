# copy

copy credential from 1 vault instance to another vault instance.


If source server is on another host, you could create a ssh tunnel for it.
e.g. your source server will be accessible at `127.0.0.1:8300` with the following command:
```bash
ssh -N -L 127.0.0.1:8300:127.0.0.1:8200 <src-vault-server>
```

