package main

import (
	vault "github.com/hashicorp/vault/api"
	log "github.com/sirupsen/logrus"
)

func copyCredential(srcClient, destClient *vault.Logical, srcSecretPath string, dryRun bool) error {
	var err error
	if dryRun {
		err = copySecretDryRun(srcClient, destClient, srcSecretPath)
	} else {
		err = copySecret(srcClient, destClient, srcSecretPath)
	}
	return err
}

func copySecret(srcClient, destClient *vault.Logical, srcSecretPath string) error {
	secret, err := srcClient.Read(srcSecretPath)
	if err != nil {
		log.WithField("path", srcSecretPath).WithError(err).Error("fail to read credential from src")
		return err
	}
	_, err = destClient.Write(srcSecretPath, secret.Data)
	if err != nil {
		log.WithField("path", srcSecretPath).WithError(err).Error("fail to copy credential")
		return err
	}
	return nil
}

func copySecretDryRun(srcClient, destClient *vault.Logical, srcSecretPath string) error {
	log.Info(srcSecretPath)
	return nil
}
