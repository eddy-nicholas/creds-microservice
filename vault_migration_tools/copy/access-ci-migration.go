package main

import (
	"fmt"
	vault "github.com/hashicorp/vault/api"
	log "github.com/sirupsen/logrus"
	"strings"
)

// this could be used in place of copyCredential to migrate username from XSEDE suffix to ACCESS-CI suffix.
func copyCredentialWithUpdatedUsername(srcClient, destClient *vault.Logical, srcSecretPath string, dryRun bool) error {
	segment := strings.Split(srcSecretPath, "/")
	if len(segment) != 4 {
		return fmt.Errorf("ill-formed path, %s", srcSecretPath)
	}
	owner := segment[1]
	if !strings.HasSuffix(owner, "@xsede.org") {
		// skip
		return nil
	}
	segment[1] = newOwner(owner)
	newPath := strings.Join(segment, "/")

	if dryRun {
		log.Info(newPath)
		return nil
	}

	secret, err := srcClient.Read(srcSecretPath)
	if err != nil {
		log.WithField("path", srcSecretPath).WithError(err).Error("fail to read credential from src")
		return err
	}
	_, err = destClient.Write(newPath, secret.Data)
	if err != nil {
		log.WithField("path", srcSecretPath).WithError(err).Error("fail to copy credential")
		return err
	}
	_, err = srcClient.Delete(srcSecretPath)
	if err != nil {
		log.WithField("path", srcSecretPath).WithError(err).Error("fail to delete old credential")
		return err
	}
	return nil
}

func newOwner(oldOwner string) string {
	return oldOwner[:len(oldOwner)-10] + "@access-ci.org"
}
