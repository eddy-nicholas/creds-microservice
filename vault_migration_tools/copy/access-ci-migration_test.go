package main

import (
	"testing"
)

func Test_newOwner(t *testing.T) {
	type args struct {
		oldOwner string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "",
			args: args{
				oldOwner: "foobar@xsede.org",
			},
			want: "foobar@access-ci.org",
		},
		{
			name: "",
			args: args{
				oldOwner: "f@xsede.org",
			},
			want: "f@access-ci.org",
		},
		{
			name: "",
			args: args{
				oldOwner: "@xsede.org",
			},
			want: "@access-ci.org",
		},
		{
			name: "",
			args: args{
				oldOwner: "aaaaaaaaaaaaaaaaaaaaaaaaaaaaa@xsede.org",
			},
			want: "aaaaaaaaaaaaaaaaaaaaaaaaaaaaa@access-ci.org",
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := newOwner(tt.args.oldOwner); got != tt.want {
				t.Errorf("newOwner() = %v, want %v", got, tt.want)
			}
		})
	}
}
