package main

import (
	"flag"
	"fmt"
	vault "github.com/hashicorp/vault/api"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/creds-microservice/adapters"
	"gitlab.com/cyverse/creds-microservice/adapters/storageschema"
	"gitlab.com/cyverse/creds-microservice/types"
	"os"
	"path"
	"strings"
	"time"
)

func main() {
	dryRun := flag.Bool("dry-run", false, "dry run, do not write to postgresql")
	flag.Parse()
	if *dryRun {
		log.Info("dry run")
	} else {
		log.Info("start in 5 seconds")
		time.Sleep(time.Second * 5)
		log.Info("starting...")
	}
	// read from vault
	// write to psql
	conf := types.Config{
		VaultAddress:     os.Getenv("VAULT_ADDR"),
		VaultToken:       os.Getenv("VAULT_TOKEN"),
		PostgresHost:     os.Getenv("POSTGRES_HOST"),
		PostgresUsername: os.Getenv("POSTGRES_USER"),
		PostgresPassword: os.Getenv("POSTGRES_PASSWORD"),
		PostgresDatabase: os.Getenv("POSTGRES_DB"),
		PostgresSSL:      false,
	}
	var srcClient *vault.Logical
	srcClient = newVaultClient(conf.VaultAddress, conf.VaultToken)

	psqlStorage := adapters.NewPostgresStorage(utcTimeSrc)
	psqlStorage.Init(conf)

	if *dryRun {
		err := psqlStorage.InitDatabase()
		if err != nil {
			log.WithError(err).Fatal()
		}
	}

	secretPathList, err := listVaultPathForAllCredentials(srcClient)
	if err != nil {
		log.WithError(err).Fatal()
	}
	for _, secretPath := range secretPathList {
		err = migrate(srcClient, secretPath, psqlStorage, *dryRun)
		if err != nil {
			log.WithField("path", secretPath).WithError(err).Error("fail to migrate secret")
			return
		}
	}
	log.WithField("count", len(secretPathList)).Infof("successfully copied all credential")
}

func utcTimeSrc() time.Time {
	return time.Now().UTC()
}

func newVaultClient(vaultAddr, vaultToken string) *vault.Logical {
	config := vault.DefaultConfig()
	config.Timeout = time.Second * 3
	config.Address = vaultAddr
	client, err := vault.NewClient(config)
	if err != nil {
		log.WithField("addr", vaultAddr).WithError(err).Fatal("fail to create client")
	}
	client.SetToken(vaultToken)
	return client.Logical()
}

// return a list of vault path
func listVaultPathForAllCredentials(client *vault.Logical) ([]string, error) {
	var secretPathList []string
	userList, err := vaultList(client, vaultNamespace)
	if err != nil {
		log.WithError(err).Error("fail to list all users")
		return nil, err
	}
	log.Infof("there are %d users", len(userList))
	for _, user := range userList {
		username := user
		if user[len(user)-1] == '/' {
			username = user[:len(user)-1]
		}
		credList, err := vaultList(client, vaultUserBasePath(username))
		if err != nil {
			log.WithField("username", username).WithError(err).Error("fail to list cred for user")
			return nil, err
		}
		log.WithField("username", username).Infof("there are %d credential for user", len(credList))
		for _, credID := range credList {
			secretPathList = append(secretPathList, vaultCredentialPath(username, credID))
		}
	}
	log.Infof("there are %d credentials in total", len(secretPathList))
	return secretPathList, nil
}

func vaultList(client *vault.Logical, secretPath string) ([]string, error) {
	var result []string
	list, err := client.List(secretPath)
	if err != nil {
		return nil, err
	}
	if list == nil {
		return nil, fmt.Errorf("list result from vault is nil")
	}
	if list.Data == nil {
		return nil, fmt.Errorf("list data from vault is nil")
	}
	dataRaw, ok := list.Data["keys"]
	if !ok {
		return nil, fmt.Errorf("key 'keys' missing")
	}
	listRaw, ok := dataRaw.([]interface{})
	if !ok {
		return nil, fmt.Errorf("'keys' is not []interface{}")
	}
	for _, elem := range listRaw {
		subPath, ok := elem.(string)
		if !ok {
			return nil, fmt.Errorf("element in 'keys' is not string")
		}
		result = append(result, subPath)
	}
	return result, nil
}

const vaultNamespace = "secret/"

func vaultUserBasePath(owner string) string {
	return vaultNamespace + owner + "/secrets/"
}

func vaultCredentialPath(owner, credID string) string {
	return vaultNamespace + owner + "/secrets/" + credID
}

func migrate(client *vault.Logical, secretPath string, storage *adapters.PostgresStorage, dryRun bool) error {
	secret, err := client.Read(secretPath)
	if err != nil {
		return err
	}
	cred, err := storageschema.VaultSecretToServiceCred(ownerFromPath(secretPath), idFromPath(secretPath), secret.Data)
	if err != nil {
		return err
	}
	if dryRun {
		return nil
	}
	err = storage.Import(cred)
	if err != nil {
		return err
	}
	return nil
}

func idFromPath(secretPath string) string {
	_, id := path.Split(secretPath)
	return id
}

// secret/<username>/secrets/<cred-id>
func ownerFromPath(secretPath string) string {
	splits := strings.Split(secretPath, "/")
	if len(splits) != 4 {
		panic("bad path, " + secretPath)
	}
	return splits[1]
}
