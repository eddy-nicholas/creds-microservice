package storageschema

import (
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/service"
	"testing"
	"time"
)

func TestVaultSecretToServiceCred(t *testing.T) {
	type args struct {
		username   string
		id         string
		secretData map[string]interface{}
	}
	tests := []struct {
		name    string
		args    args
		want    service.CredentialModel
		wantErr bool
	}{
		{
			name: "simple string value cred(v1)",
			args: args{
				username: "testuser-123",
				id:       "foobar",
				secretData: map[string]interface{}{
					"Type":        "type123",
					"Value":       "val1",
					"Description": "description123",
					"IsSystem":    false,
					"IsHidden":    false,
					"Visibility":  "",
					"Tags":        "",
					"CreatedAt":   "1655310000000",
					"UpdatedAt":   "1655310000000",
				},
			},
			want: service.CredentialModel{
				Session:           service.Session{},
				ID:                "foobar",
				Name:              "foobar",
				Username:          "testuser-123",
				Type:              "type123",
				Value:             "val1",
				Description:       "description123",
				IsSystem:          false,
				IsHidden:          false,
				Visibility:        "",
				Tags:              map[string]string{},
				CreatedAt:         StringMsToTime("1655310000000"),
				UpdatedAt:         StringMsToTime("1655310000000"),
				UpdatedBy:         "",
				UpdatedEmulatorBy: "",
			},
			wantErr: false,
		},
		{
			name: "example openstack application credential w/ tags(v1)",
			args: args{
				username: "testuser-123",
				id:       "foobar",
				secretData: map[string]interface{}{
					"CreatedAt":   "1655310000000",
					"Description": "openstack application credential created by CACAO for project aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
					"IsHidden":    false,
					"IsSystem":    false,
					"Tags":        "provider-aaaaaaaaaaaaaaaaaaaa:~`;;-~!!:application:~`;;-~!!:",
					"Type":        "openstack",
					"UpdatedAt":   "1655310000000",
					"Value":       `{"OS_APPLICATION_CREDENTIAL_ID":"aaaaaaa","OS_APPLICATION_CREDENTIAL_SECRET":"aaaaaaaaaaaaaaaa","OS_AUTH_TYPE":"v3applicationcredential","OS_AUTH_URL":"https://example.cyverse.org","OS_IDENTITY_API_VERSION":"3","OS_INTERFACE":"public","OS_REGION_NAME":"example-region"}`,
					"Visibility":  "",
				},
			},
			want: service.CredentialModel{
				Session:           service.Session{},
				ID:                "foobar",
				Name:              "foobar",
				Username:          "testuser-123",
				Type:              "openstack",
				Value:             `{"OS_APPLICATION_CREDENTIAL_ID":"aaaaaaa","OS_APPLICATION_CREDENTIAL_SECRET":"aaaaaaaaaaaaaaaa","OS_AUTH_TYPE":"v3applicationcredential","OS_AUTH_URL":"https://example.cyverse.org","OS_IDENTITY_API_VERSION":"3","OS_INTERFACE":"public","OS_REGION_NAME":"example-region"}`,
				Description:       "openstack application credential created by CACAO for project aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
				IsSystem:          false,
				IsHidden:          false,
				Visibility:        "",
				Tags:              map[string]string{"provider-aaaaaaaaaaaaaaaaaaaa": "", "application": ""},
				CreatedAt:         StringMsToTime("1655310000000"),
				UpdatedAt:         StringMsToTime("1655310000000"),
				UpdatedBy:         "",
				UpdatedEmulatorBy: "",
			},
			wantErr: false,
		},
		{
			name: "example ssh credential (v1)",
			args: args{
				username: "testuser-123",
				id:       "foobar",
				secretData: map[string]interface{}{
					"CreatedAt":   "1655310000000",
					"Description": "",
					"IsHidden":    false,
					"IsSystem":    false,
					"Tags":        "",
					"Type":        "ssh",
					"UpdatedAt":   "1655310000000",
					"Value":       "ssh-ed25519 AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
					"Visibility":  "",
				},
			},
			want: service.CredentialModel{
				Session:           service.Session{},
				ID:                "foobar",
				Name:              "foobar",
				Username:          "testuser-123",
				Type:              "ssh",
				Value:             "ssh-ed25519 AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
				Description:       "",
				IsSystem:          false,
				IsHidden:          false,
				Visibility:        "",
				Tags:              map[string]string{},
				CreatedAt:         StringMsToTime("1655310000000"),
				UpdatedAt:         StringMsToTime("1655310000000"),
				UpdatedBy:         "",
				UpdatedEmulatorBy: "",
			},
			wantErr: false,
		},
		{
			name: "empty owner",
			args: args{
				username: "",
				id:       "foobar",
				secretData: map[string]interface{}{
					"CreatedAt":   "1655310000000",
					"Description": "",
					"IsHidden":    false,
					"IsSystem":    false,
					"Tags":        "",
					"Type":        "ssh",
					"UpdatedAt":   "1655310000000",
					"Value":       "ssh-ed25519 AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
					"Visibility":  "",
				},
			},
			want:    service.CredentialModel{},
			wantErr: true,
		},
		{
			name: "empty id",
			args: args{
				username: "test-user123",
				id:       "",
				secretData: map[string]interface{}{
					"CreatedAt":   "1655310000000",
					"Description": "",
					"IsHidden":    false,
					"IsSystem":    false,
					"Tags":        "",
					"Type":        "ssh",
					"UpdatedAt":   "1655310000000",
					"Value":       "ssh-ed25519 AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
					"Visibility":  "",
				},
			},
			want:    service.CredentialModel{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := VaultSecretToServiceCred(tt.args.username, tt.args.id, tt.args.secretData)
			assert.Equalf(t, tt.want, got, "VaultSecretToServiceCred() = %v, want %v", got, tt.want)
			if tt.wantErr {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
			}

		})
	}
}

func TestServiceCredToVaultSecret(t *testing.T) {
	type args struct {
		cred service.CredentialModel
	}
	tests := []struct {
		name string
		args args
		want VaultSecretData
	}{
		{
			name: "simple string value cred",
			args: args{
				cred: service.CredentialModel{
					Session:           service.Session{},
					ID:                "foobar",
					Name:              "cred-name-123",
					Username:          "testuser-123",
					Type:              "type123",
					Value:             "val1",
					Description:       "description123",
					IsSystem:          false,
					IsHidden:          false,
					Visibility:        "",
					Tags:              map[string]string{},
					CreatedAt:         time.Unix(1655310000, 0),
					UpdatedAt:         time.Unix(1655310000, 0),
					UpdatedBy:         "",
					UpdatedEmulatorBy: "",
				},
			},
			want: VaultSecretData{
				"SchemaVersion":     storageV2,
				"Type":              "type123",
				"Name":              "cred-name-123",
				"Value":             "val1",
				"Description":       "description123",
				"IsSystem":          false,
				"IsHidden":          false,
				"Disabled":          false,
				"Visibility":        "",
				"TagsV2":            map[string]string{},
				"CreatedAt":         TimeToStringMs(time.Unix(1655310000, 0)),
				"UpdatedAt":         TimeToStringMs(time.Unix(1655310000, 0)),
				"UpdatedBy":         "",
				"UpdatedEmulatorBy": "",
			},
		},
		{
			name: "ignore empty tag",
			args: args{
				cred: service.CredentialModel{
					Session:           service.Session{},
					ID:                "foobar",
					Name:              "cred-name-123",
					Username:          "testuser-123",
					Type:              "type123",
					Value:             "val1",
					Description:       "description123",
					IsSystem:          false,
					IsHidden:          false,
					Visibility:        "",
					Tags:              map[string]string{"": ""}, // ignore empty tag
					CreatedAt:         time.Unix(1655310000, 0),
					UpdatedAt:         time.Unix(1655310000, 0),
					UpdatedBy:         "",
					UpdatedEmulatorBy: "",
				},
			},
			want: VaultSecretData{
				"SchemaVersion":     storageV2,
				"Type":              "type123",
				"Name":              "cred-name-123",
				"Value":             "val1",
				"Description":       "description123",
				"IsSystem":          false,
				"IsHidden":          false,
				"Disabled":          false,
				"Visibility":        "",
				"TagsV2":            map[string]string{},
				"CreatedAt":         TimeToStringMs(time.Unix(1655310000, 0)),
				"UpdatedAt":         TimeToStringMs(time.Unix(1655310000, 0)),
				"UpdatedBy":         "",
				"UpdatedEmulatorBy": "",
			},
		},
		{
			name: "key-value tag",
			args: args{
				cred: service.CredentialModel{
					Session:     service.Session{},
					ID:          "foobar",
					Name:        "cred-name-123",
					Username:    "testuser-123",
					Type:        "type123",
					Value:       "val1",
					Description: "description123",
					IsSystem:    false,
					IsHidden:    false,
					Visibility:  "",
					Tags: map[string]string{
						"tag-name-123": "tag-value-123",
						"tag-name-234": "tag-value-234",
					},
					CreatedAt:         time.Unix(1655310000, 0),
					UpdatedAt:         time.Unix(1655310000, 0),
					UpdatedBy:         "",
					UpdatedEmulatorBy: "",
				},
			},
			want: VaultSecretData{
				"SchemaVersion": storageV2,
				"Type":          "type123",
				"Name":          "cred-name-123",
				"Value":         "val1",
				"Description":   "description123",
				"IsSystem":      false,
				"IsHidden":      false,
				"Disabled":      false,
				"Visibility":    "",
				"TagsV2": map[string]string{
					"tag-name-123": "tag-value-123",
					"tag-name-234": "tag-value-234",
				},
				"CreatedAt":         TimeToStringMs(time.Unix(1655310000, 0)),
				"UpdatedAt":         TimeToStringMs(time.Unix(1655310000, 0)),
				"UpdatedBy":         "",
				"UpdatedEmulatorBy": "",
			},
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ServiceCredToVaultSecret(tt.args.cred)
			assert.Equalf(t, tt.want, got, "ServiceCredToVaultSecret() = %v, want %v", got, tt.want)
			assert.NoError(t, err)
		})
	}
}

func Test_vaultSecretToStorageModel(t *testing.T) {
	type args struct {
		owner      string
		credID     string
		secretData map[string]interface{}
	}
	tests := []struct {
		name    string
		args    args
		want    credStorageModelV2
		wantErr bool
	}{
		{
			name: "v1",
			args: args{
				owner:  "",
				credID: "user-defined-id",
				secretData: map[string]interface{}{
					"Type":        "type123",
					"Value":       "val1",
					"Description": "description123",
					"IsSystem":    false,
					"IsHidden":    false,
					"Visibility":  "",
					"Tags":        "",
					"CreatedAt":   "1655310000000",
					"UpdatedAt":   "1655310000000",
				},
			},
			want: credStorageModelV2{
				SchemaVersion:     storageV2,
				Name:              "user-defined-id",
				Value:             "val1",
				Type:              "type123",
				Description:       "description123",
				IsSystem:          false,
				IsHidden:          false,
				Disabled:          false,
				Visibility:        "",
				TagsV2:            map[string]string{},
				CreatedAt:         "1655310000000",
				UpdatedAt:         "1655310000000",
				UpdatedBy:         "",
				UpdatedEmulatorBy: "",
			},
			wantErr: false,
		},
		{
			name: "v1 w/ tags",
			args: args{
				owner:  "",
				credID: "user-defined-id",
				secretData: map[string]interface{}{
					"Type":        "type123",
					"Value":       "val1",
					"Description": "description123",
					"IsSystem":    false,
					"IsHidden":    false,
					"Visibility":  "Default",
					"Tags":        "tag1" + v1SecretTagDivider + "tag2",
					"CreatedAt":   "1655310000000",
					"UpdatedAt":   "1655310000000",
				},
			},
			want: credStorageModelV2{
				SchemaVersion: storageV2,
				Name:          "user-defined-id",
				Value:         "val1",
				Type:          "type123",
				Description:   "description123",
				IsSystem:      false,
				IsHidden:      false,
				Disabled:      false,
				Visibility:    "Default",
				TagsV2: map[string]string{
					"tag1": "",
					"tag2": "",
				},
				CreatedAt:         "1655310000000",
				UpdatedAt:         "1655310000000",
				UpdatedBy:         "",
				UpdatedEmulatorBy: "",
			},
			wantErr: false,
		},
		{
			name: "v1 openstack",
			args: args{
				owner:  "",
				credID: "user-defined-id",
				secretData: map[string]interface{}{
					"CreatedAt":   "1655310000000",
					"Description": "openstack application credential created by CACAO for project aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
					"IsHidden":    false,
					"IsSystem":    false,
					"Tags":        "provider-aaaaaaaaaaaaaaaaaaaa:~`;;-~!!:application:~`;;-~!!:",
					"Type":        "openstack",
					"UpdatedAt":   "1655310000000",
					"Value":       `{"OS_APPLICATION_CREDENTIAL_ID":"aaaaaaa","OS_APPLICATION_CREDENTIAL_SECRET":"aaaaaaaaaaaaaaaa","OS_AUTH_TYPE":"v3applicationcredential","OS_AUTH_URL":"https://example.cyverse.org","OS_IDENTITY_API_VERSION":"3","OS_INTERFACE":"public","OS_REGION_NAME":"example-region"}`,
					"Visibility":  "",
				},
			},
			want: credStorageModelV2{
				SchemaVersion: storageV2,
				Name:          "user-defined-id",
				Value:         `{"OS_APPLICATION_CREDENTIAL_ID":"aaaaaaa","OS_APPLICATION_CREDENTIAL_SECRET":"aaaaaaaaaaaaaaaa","OS_AUTH_TYPE":"v3applicationcredential","OS_AUTH_URL":"https://example.cyverse.org","OS_IDENTITY_API_VERSION":"3","OS_INTERFACE":"public","OS_REGION_NAME":"example-region"}`,
				Type:          "openstack",
				Description:   "openstack application credential created by CACAO for project aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
				IsSystem:      false,
				IsHidden:      false,
				Disabled:      false,
				Visibility:    "",
				TagsV2: map[string]string{
					"provider-aaaaaaaaaaaaaaaaaaaa": "",
					"application":                   "",
				},
				CreatedAt:         "1655310000000",
				UpdatedAt:         "1655310000000",
				UpdatedBy:         "",
				UpdatedEmulatorBy: "",
			},
			wantErr: false,
		},
		{
			name: "v1 ssh",
			args: args{
				owner:  "",
				credID: "user-defined-id",
				secretData: map[string]interface{}{
					"CreatedAt":   "1655310000000",
					"Description": "",
					"IsHidden":    false,
					"IsSystem":    false,
					"Tags":        "",
					"Type":        "ssh",
					"UpdatedAt":   "1655310000000",
					"Value":       "ssh-ed25519 AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
					"Visibility":  "",
				},
			},
			want: credStorageModelV2{
				SchemaVersion:     storageV2,
				Name:              "user-defined-id",
				Value:             "ssh-ed25519 AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
				Type:              "ssh",
				Description:       "",
				IsSystem:          false,
				IsHidden:          false,
				Disabled:          false,
				Visibility:        "",
				TagsV2:            map[string]string{},
				CreatedAt:         "1655310000000",
				UpdatedAt:         "1655310000000",
				UpdatedBy:         "",
				UpdatedEmulatorBy: "",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := vaultSecretToStorageModel(tt.args.owner, tt.args.credID, tt.args.secretData)
			if tt.wantErr {
				assert.Error(t, err)
				return
			} else {
				assert.NoError(t, err)
			}
			assert.Equalf(t, tt.want, got, "vaultSecretToStorageModel(%v, %v)", tt.args.owner, tt.args.secretData)
		})
	}
}

func Test_parseSchemaVersionFromSecretData(t *testing.T) {
	type args struct {
		secretData VaultSecretData
	}
	tests := []struct {
		name    string
		args    args
		want    schemaVersion
		wantErr bool
	}{
		{
			name: "v1",
			args: args{
				secretData: map[string]interface{}{
					"CreatedAt":   "1655310000000",
					"Description": "",
					"IsHidden":    false,
					"IsSystem":    false,
					"Tags":        "",
					"Type":        "ssh",
					"UpdatedAt":   "1655310000000",
					"Value":       "ssh-ed25519 AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
					"Visibility":  "",
				},
			},
			want:    1,
			wantErr: false,
		},
		{
			name: "v2",
			args: args{
				secretData: map[string]interface{}{
					"CreatedAt":         "1655310000000",
					"Description":       nil,
					"Disabled":          false,
					"IsHidden":          false,
					"IsSystem":          false,
					"Name":              "my-cred",
					"SchemaVersion":     2,
					"TagsV2":            map[string]string{},
					"Tags":              "",
					"Type":              "ssh",
					"UpdatedAt":         "1655310000000",
					"UpdatedBy":         nil,
					"UpdatedEmulatorBy": nil,
					"Value":             "ssh-ed25519 AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
					"Visibility":        nil,
				},
			},
			want:    2,
			wantErr: false,
		},
		{
			name: "v3",
			args: args{
				secretData: map[string]interface{}{
					"SchemaVersion": 3,
				},
			},
			want:    0,
			wantErr: true,
		},
		{
			name: "version not number",
			args: args{
				secretData: map[string]interface{}{
					"SchemaVersion": "foobar",
				},
			},
			want:    0,
			wantErr: true,
		},
		{
			name: "version is decimal",
			args: args{
				secretData: map[string]interface{}{
					"SchemaVersion": 1.2,
				},
			},
			want:    0,
			wantErr: true,
		},
		{
			name: "version is integer, json.Number",
			args: args{
				secretData: map[string]interface{}{
					"SchemaVersion": json.Number("2"),
				},
			},
			want:    2,
			wantErr: false,
		},
		{
			name: "version is decimal, json.Number",
			args: args{
				secretData: map[string]interface{}{
					"SchemaVersion": json.Number("1.2"),
				},
			},
			want:    0,
			wantErr: true,
		},
		{
			name: "version is not number, json.Number",
			args: args{
				secretData: map[string]interface{}{
					"SchemaVersion": json.Number("foobar"),
				},
			},
			want:    0,
			wantErr: true,
		},
		{
			name: "version parsed from json, integer",
			args: args{
				secretData: map[string]interface{}{
					"SchemaVersion": func() interface{} {
						var result interface{}
						err := json.Unmarshal([]byte("2"), &result)
						if err != nil {
							panic(err)
						}
						return result
					}(),
				},
			},
			want:    2,
			wantErr: false,
		},
		{
			name: "version parsed from json, float w/ no decimal",
			args: args{
				secretData: map[string]interface{}{
					"SchemaVersion": func() interface{} {
						var result interface{}
						err := json.Unmarshal([]byte("2.0"), &result)
						if err != nil {
							panic(err)
						}
						return result
					}(),
				},
			},
			want:    2,
			wantErr: false,
		},
		{
			name: "version parsed from json, float",
			args: args{
				secretData: map[string]interface{}{
					"SchemaVersion": func() interface{} {
						var result interface{}
						err := json.Unmarshal([]byte("1.2"), &result)
						if err != nil {
							panic(err)
						}
						return result
					}(),
				},
			},
			want:    0,
			wantErr: true,
		},
		{
			name: "version parsed from json, string",
			args: args{
				secretData: map[string]interface{}{
					"SchemaVersion": func() interface{} {
						var result interface{}
						err := json.Unmarshal([]byte("\"foobar\""), &result)
						if err != nil {
							panic(err)
						}
						return result
					}(),
				},
			},
			want:    0,
			wantErr: true,
		},
		{
			name: "version parsed from json, null",
			args: args{
				secretData: map[string]interface{}{
					"SchemaVersion": func() interface{} {
						var result interface{}
						err := json.Unmarshal([]byte("null"), &result)
						if err != nil {
							panic(err)
						}
						return result
					}(),
				},
			},
			want:    0,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := parseSchemaVersionFromSecretData(tt.args.secretData)
			if tt.wantErr {
				assert.Errorf(t, err, "parseSchemaVersionFromSecretData(%v)", tt.args.secretData)
			} else {
				assert.NoErrorf(t, err, "parseSchemaVersionFromSecretData(%v)", tt.args.secretData)
			}
			assert.Equalf(t, tt.want, got, "parseSchemaVersionFromSecretData(%v)", tt.args.secretData)
		})
	}
}
