package storageschema

import (
	"strconv"
	"time"
)

// StringMsToTime converts a string(Unix milliseconds) into a time.
// This is used for serialize timestamp into string when storing credential into vault.
func StringMsToTime(ms string) time.Time {
	msInt, err := strconv.ParseInt(ms, 10, 64)
	if err != nil {
		return time.Time{}
	}
	return time.Unix(0, msInt*int64(time.Millisecond)).UTC()
}

// TimeToStringMs converts a time into a string(Unix milliseconds).
// This is used for de-serialize timestamp from string when retrieve credential from vault.
func TimeToStringMs(time time.Time) (ms string) {
	return strconv.FormatInt(time.UTC().UnixNano()/1e6, 10)
}

// MilliSecPrecision return the time to millisecond precision and in UTC. The extra precisions are truncated.
func MilliSecPrecision(t time.Time) time.Time {
	return time.Unix(0, t.UnixNano()/1e6*int64(time.Millisecond)).UTC()
}
