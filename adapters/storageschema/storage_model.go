package storageschema

// The purpose of the struct like credStorageModelV1 is to have an explicit definition of the schema used to
// store credential in vault. This allows us to easily reason about the schema of the data in vault, and also
// it makes any schema changes explicit.
//
// Create credential:
// service-model => storage-model(latest schema, v2) => vault-secret (map[string]interface{}, effectively json obj)
// Fetch credential from vault:
// vault-secret => storage-model(whatever schema version that the secret is created as originally) => service-model

import (
	"encoding/json"
	"fmt"
	"github.com/mitchellh/mapstructure"
	"gitlab.com/cyverse/cacao-common/service"
	"reflect"
	"strings"
)

// VaultSecretData is the data of KV secret in Vault
type VaultSecretData map[string]interface{}

// schemaVersion is a schema version for the storage format of credential in vault.
type schemaVersion int

const (
	storageV1 schemaVersion = 1
	storageV2 schemaVersion = 2
)

// A unique pattern of characters to divide on. This is used for v1 only.
const v1SecretTagDivider = ":~`;;-~!!:"

// ServiceCredToVaultSecret converts service model of credential into vault secret
func ServiceCredToVaultSecret(cred service.CredentialModel) (VaultSecretData, error) {
	storageModelV2 := serviceCredToStorageModelV2(cred)
	var result map[string]interface{}
	err := mapstructure.Decode(storageModelV2, &result)
	if err != nil {
		return nil, err
	}
	return result, nil
}

// VaultSecretToServiceCred ...
func VaultSecretToServiceCred(owner, credID string, data VaultSecretData) (service.CredentialModel, error) {
	if owner == "" {
		return service.CredentialModel{}, fmt.Errorf("username(owner) cannot be empty")
	}
	if credID == "" {
		return service.CredentialModel{}, fmt.Errorf("credential ID cannot be empty")
	}
	storageModel, err := vaultSecretToStorageModel(owner, credID, data)
	if err != nil {
		return service.CredentialModel{}, err
	}
	return storageModel.ToServiceCred(owner, credID), nil
}

// able to read all storage schema versions of credential and convert into the latest schema version (v2)
func vaultSecretToStorageModel(owner, credID string, secretData VaultSecretData) (credStorageModelV2, error) {
	version, err := parseSchemaVersionFromSecretData(secretData)
	if err != nil {
		return credStorageModelV2{}, err
	}
	var result credStorageModelV2
	switch version {
	case storageV1:
		var v1Model credStorageModelV1
		err = mapstructure.Decode(secretData, &v1Model)
		if err != nil {
			return credStorageModelV2{}, err
		}
		result = v1Model.toV2(owner, credID)
	case storageV2:
		err = mapstructure.Decode(secretData, &result)
		if err != nil {
			return credStorageModelV2{}, err
		}
	default:
		panic("parseSchemaVersionFromSecretData() should only return 1 or 2 when no error")
	}
	return result, nil
}

func parseSchemaVersionFromSecretData(secretData VaultSecretData) (schemaVersion, error) {
	var version schemaVersion
	versionRaw, ok := secretData["SchemaVersion"]
	hasDecimal := func(num float64) bool { return num-float64(int64(num)) > 0.0 }
	if !ok {
		// if there isn't a "Version" field, then it is assumed to be v1
		return storageV1, nil
	}
	switch versionConverted := versionRaw.(type) {
	case int:
		version = schemaVersion(versionConverted)
	case int64:
		version = schemaVersion(versionConverted)
	case float64:
		if hasDecimal(versionConverted) {
			return 0, fmt.Errorf("storage schema version should be integer, no decimal")
		}
		version = schemaVersion(int(versionConverted))
	case json.Number:
		versionFloat, err := versionConverted.Float64()
		if err == nil {
			if hasDecimal(versionFloat) {
				return 0, fmt.Errorf("storage schema version should be integer, no decimal")
			}
		}
		versionInt, err := versionConverted.Int64()
		if err != nil {
			return 0, err
		}
		version = schemaVersion(versionInt)
	default:
		return 0, fmt.Errorf("bad type for storage schema version, %s", reflect.TypeOf(versionRaw))
	}
	if version == storageV1 || version == storageV2 {
		return version, nil
	}
	return 0, fmt.Errorf("bad storage schema version")
}

// credStorageModelV1 ...
type credStorageModelV1 struct {
	Value             string `mapstructure:"Value"`
	Type              string `mapstructure:"Type"`
	Description       string `mapstructure:"Description"`
	IsSystem          bool   `mapstructure:"IsSystem"`
	IsHidden          bool   `mapstructure:"IsHidden"`
	Visibility        string `mapstructure:"Visibility"`
	Tags              string `mapstructure:"Tags"`
	CreatedAt         string `mapstructure:"CreatedAt"`
	UpdatedAt         string `mapstructure:"UpdatedAt"`
	UpdatedBy         string `mapstructure:","`
	UpdatedEmulatorBy string `mapstructure:","`
}

func (m credStorageModelV1) ToServiceCred(owner, credID string) service.CredentialModel {
	return service.CredentialModel{
		Session:           service.Session{},
		ID:                credID,
		Name:              credID, // use ID as Name
		Username:          owner,
		Type:              service.CredentialType(m.Type),
		Value:             m.Value,
		Description:       m.Description,
		IsSystem:          m.IsSystem,
		IsHidden:          m.IsHidden,
		Visibility:        service.VisibilityType(m.Visibility),
		Tags:              m.keyValueTags(),
		CreatedAt:         StringMsToTime(m.CreatedAt),
		UpdatedAt:         StringMsToTime(m.UpdatedAt),
		UpdatedBy:         "",
		UpdatedEmulatorBy: "",
	}
}

func (m credStorageModelV1) splitTags() []string {
	if len(m.Tags) == 0 {
		return []string{}
	}
	var tags = make([]string, 0)
	split := strings.Split(m.Tags, v1SecretTagDivider)
	for _, tag := range split {
		if tag != "" {
			// only non-empty tag
			tags = append(tags, tag)
		}
	}
	return tags
}

func (m credStorageModelV1) keyValueTags() map[string]string {
	var result = make(map[string]string)
	for _, tag := range m.splitTags() {
		result[tag] = ""
	}
	return result
}

func (m credStorageModelV1) toV2(owner, credID string) credStorageModelV2 {
	tagV2 := make(map[string]string)
	for _, tagV1 := range m.splitTags() {
		tagV2[tagV1] = ""
	}
	return credStorageModelV2{
		SchemaVersion:     2,
		Name:              credID,
		Value:             m.Value,
		Type:              m.Type,
		Description:       m.Description,
		IsSystem:          m.IsSystem,
		IsHidden:          m.IsHidden,
		Disabled:          false,
		Visibility:        m.Visibility,
		TagsV2:            tagV2,
		CreatedAt:         m.CreatedAt,
		UpdatedAt:         m.UpdatedAt,
		UpdatedBy:         owner,
		UpdatedEmulatorBy: owner,
	}
}

// credStorageModelV2 is a new schema for storing credential.
// It adds SchemaVersion, TagsV2, Name, and actually stores UpdatedBy, UpdatedEmulatorBy.
type credStorageModelV2 struct {
	SchemaVersion     schemaVersion     `mapstructure:"SchemaVersion"`
	Name              string            `mapstructure:"Name"` // a user defined name
	Value             string            `mapstructure:"Value"`
	Type              string            `mapstructure:"Type"`
	Description       string            `mapstructure:"Description"`
	IsSystem          bool              `mapstructure:"IsSystem"`
	IsHidden          bool              `mapstructure:"IsHidden"`
	Disabled          bool              `mapstructure:"Disabled"`
	Visibility        string            `mapstructure:"Visibility"`
	TagsV2            map[string]string `mapstructure:"TagsV2"` // use a different field name than v1
	CreatedAt         string            `mapstructure:"CreatedAt"`
	UpdatedAt         string            `mapstructure:"UpdatedAt"`
	UpdatedBy         string            `mapstructure:"UpdatedBy"`
	UpdatedEmulatorBy string            `mapstructure:"UpdatedEmulatorBy"`
}

func serviceCredToStorageModelV2(cred service.CredentialModel) credStorageModelV2 {
	var tagsV2 = make(map[string]string, len(cred.Tags))
	for tagName, tagValue := range cred.Tags {
		if tagName == "" {
			// ignore empty tag
			continue
		}
		tagsV2[tagName] = tagValue
	}
	return credStorageModelV2{
		SchemaVersion:     storageV2,
		Name:              cred.Name,
		Value:             cred.Value,
		Type:              string(cred.Type),
		Description:       cred.Description,
		IsSystem:          cred.IsSystem,
		IsHidden:          cred.IsHidden,
		Disabled:          false, // TODO extract from service model when service model has the field
		Visibility:        string(cred.Visibility),
		TagsV2:            tagsV2,
		CreatedAt:         TimeToStringMs(cred.CreatedAt),
		UpdatedAt:         TimeToStringMs(cred.UpdatedAt),
		UpdatedBy:         "",
		UpdatedEmulatorBy: "",
	}
}

func (m credStorageModelV2) ToServiceCred(owner, credID string) service.CredentialModel {
	credName := m.Name
	if credName == "" {
		credName = credID // use ID as Name if Name is empty
	}
	return service.CredentialModel{
		Session:           service.Session{},
		ID:                credID,
		Name:              credName,
		Username:          owner,
		Type:              service.CredentialType(m.Type),
		Value:             m.Value,
		Description:       m.Description,
		IsSystem:          m.IsSystem,
		IsHidden:          m.IsHidden,
		Visibility:        service.VisibilityType(m.Visibility),
		Tags:              m.TagsV2,
		CreatedAt:         StringMsToTime(m.CreatedAt),
		UpdatedAt:         StringMsToTime(m.UpdatedAt),
		UpdatedBy:         "",
		UpdatedEmulatorBy: "",
	}
}
