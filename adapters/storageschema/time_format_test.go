package storageschema

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

// this ensures that the time format conversion is "reversible" with millisecond precision.
func TestTime(t *testing.T) {
	t.Run("now", func(t *testing.T) {
		t1 := time.Now().UTC()
		t1Str := TimeToStringMs(t1)
		t1Converted := StringMsToTime(t1Str)
		assert.Equal(t, MilliSecPrecision(t1), t1Converted)
	})
	t.Run("fixed time", func(t *testing.T) {
		t1 := time.Date(2022, 6, 30, 15, 30, 0, 0, time.UTC)
		t1Str := TimeToStringMs(t1)
		t1Converted := StringMsToTime(t1Str)
		assert.Equal(t, MilliSecPrecision(t1), t1Converted)
		assert.Equal(t, "1656603000000", t1Str)
	})
}

func TestMilliSecPrecision(t *testing.T) {
	t.Run("now", func(t *testing.T) {
		t1 := time.Now().UTC()
		t1Nano := t1.UnixNano()
		t2 := MilliSecPrecision(t1)
		t2Nano := t2.UnixNano()
		assert.Equal(t, t1Nano/1e6, t2Nano/1e6)
		assert.Equal(t, time.UTC, t2.Location())
	})
	t.Run("now local", func(t *testing.T) {
		t1 := time.Now()
		t1Nano := t1.UnixNano()
		t2 := MilliSecPrecision(t1)
		t2Nano := t2.UnixNano()
		assert.Equal(t, t1Nano/1e6, t2Nano/1e6)
		assert.Equal(t, time.UTC, t2.Location())
	})
}
