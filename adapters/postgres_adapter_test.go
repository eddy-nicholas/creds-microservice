package adapters

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/creds-microservice/adapters/storageschema"
	"gitlab.com/cyverse/creds-microservice/ports"
	"gitlab.com/cyverse/creds-microservice/types"
	"os"
	"reflect"
	"testing"
	"time"
)

func TestPostgresStorage_Get(t *testing.T) {
	if skipIfPostgresEnvNotSet(t) {
		return
	}

	storageCreator := func(timeSrc ports.TimeSrc) ports.PersistentStoragePort {
		cfg := types.Config{
			PostgresHost:     os.Getenv("POSTGRES_HOST"),
			PostgresUsername: os.Getenv("POSTGRES_USER"),
			PostgresPassword: os.Getenv("POSTGRES_PASSWORD"),
			PostgresDatabase: os.Getenv("POSTGRES_DB"),
			PostgresSSL:      false,
		}
		storage := &PostgresStorage{
			getCurrentTime: timeSrc,
		}
		storage.Init(cfg)
		err := storage.InitDatabase()
		if err != nil {
			// create tables and ignore error in case already created
			t.Log(err)
		}

		_, err = storage.db.Exec("DELETE FROM credential_tags;")
		if err != nil {
			panic(err)
		}
		_, err = storage.db.Exec("DELETE FROM credential;")
		if err != nil {
			panic(err)
		}

		return storage
	}
	testPersistentStorage(t, storageCreator)
	testPersistentStorageExtra(t, storageCreator)
}

// return a constructor for PostgresStorage for integration test (require a real instance of postgres)
func newPsqlStorageCreator(t *testing.T) func(timeSrc ports.TimeSrc) ports.PersistentStoragePort {
	storageCreator := func(timeSrc ports.TimeSrc) ports.PersistentStoragePort {
		cfg := types.Config{
			PostgresHost:     os.Getenv("POSTGRES_HOST"),
			PostgresUsername: os.Getenv("POSTGRES_USER"),
			PostgresPassword: os.Getenv("POSTGRES_PASSWORD"),
			PostgresDatabase: os.Getenv("POSTGRES_DB"),
			PostgresSSL:      false,
		}
		storage := &PostgresStorage{
			getCurrentTime: timeSrc,
		}
		storage.Init(cfg)
		err := storage.InitDatabase()
		if err != nil {
			// create tables and ignore error in case already created
			t.Log(err)
		}

		_, err = storage.db.Exec("DELETE FROM credential_tags;")
		if err != nil {
			panic(err)
		}
		_, err = storage.db.Exec("DELETE FROM credential;")
		if err != nil {
			panic(err)
		}

		return storage
	}
	return storageCreator
}

func TestPostgresStorage_Update(t *testing.T) {
	if skipIfPostgresEnvNotSet(t) {
		return
	}

	storageCreator := newPsqlStorageCreator(t)
	now := time.Now()
	getCurrentTime := func() time.Time {
		return now
	}

	t.Run("empty struct", func(t *testing.T) {
		ps := storageCreator(getCurrentTime)

		var credUpdate types.CredUpdate
		err := ps.Update(credUpdate)
		if !assert.Error(t, err) {
			return
		}
	})
	t.Run("no update", func(t *testing.T) {
		ps := storageCreator(getCurrentTime)
		originalCred := service.CredentialModel{
			Session:     service.Session{},
			ID:          "cred-aaaaaaaaaaaaaaaaaaaa",
			Name:        "credname123",
			Username:    "testuser123",
			Type:        "generic",
			Value:       "value123",
			Description: "description123",
		}
		// create cred
		err := ps.Create(originalCred)
		if err != nil {
			panic(err)
		}
		var credUpdate = types.CredUpdate{
			SessionActor:    "testuser123",
			SessionEmulator: "",
			Username:        "testuser123",
			ID:              "cred-aaaaaaaaaaaaaaaaaaaa",
			Update:          service.CredentialUpdate{},
		}
		err = ps.Update(credUpdate)
		if !assert.NoError(t, err) {
			return
		}

		// re-fetch to check if name is updated
		refetchedCred, err := ps.Get(service.CredentialModel{
			Session:  service.Session{},
			ID:       "cred-aaaaaaaaaaaaaaaaaaaa",
			Username: "testuser123",
		})
		if !assert.NoError(t, err) {
			return
		}
		assert.Equal(t, service.CredentialModel{
			Session:           service.Session{},
			ID:                originalCred.ID,
			Name:              originalCred.Name,
			Username:          originalCred.Username,
			Type:              originalCred.Type,
			Value:             originalCred.Value,
			Description:       originalCred.Description,
			IsSystem:          originalCred.IsSystem,
			IsHidden:          originalCred.IsHidden,
			Disabled:          originalCred.Disabled,
			Visibility:        originalCred.Visibility,
			Tags:              map[string]string{},
			CreatedAt:         storageschema.MilliSecPrecision(now),
			UpdatedAt:         storageschema.MilliSecPrecision(now), // Note because the time source we used always return the same time, the creation and update timestamp are the same
			UpdatedBy:         "",
			UpdatedEmulatorBy: "",
		}, refetchedCred)
	})
	t.Run("cred not found", func(t *testing.T) {
		ps := storageCreator(getCurrentTime)
		// credential is not created, Update() should return not found
		var credUpdate = types.CredUpdate{
			SessionActor:    "foobar",
			SessionEmulator: "",
			Username:        "foobar",
			ID:              "cred-aaaaaaaaaaaaaaaaaaaa",
			Update: service.CredentialUpdate{
				Name: stringPtr("does not matter"),
			},
		}
		err := ps.Update(credUpdate)
		if !assert.Error(t, err) {
			return
		}
		_, ok := err.(*service.CacaoNotFoundError)

		assert.Truef(t, ok, "error is not of type *service.CacaoNotFoundError, %s, %s", reflect.TypeOf(err).String(), err.Error())
	})
	t.Run("update name", func(t *testing.T) {
		ps := storageCreator(getCurrentTime)

		originalCred := service.CredentialModel{
			Session:     service.Session{},
			ID:          "cred-aaaaaaaaaaaaaaaaaaaa",
			Name:        "credname123",
			Username:    "testuser123",
			Type:        "generic",
			Value:       "value123",
			Description: "description123",
		}
		// create cred
		err := ps.Create(originalCred)
		if err != nil {
			panic(err)
		}

		var credUpdate = types.CredUpdate{
			SessionActor:    "testuser123",
			SessionEmulator: "",
			Username:        "testuser123",
			ID:              "cred-aaaaaaaaaaaaaaaaaaaa",
			Update: service.CredentialUpdate{
				Name: stringPtr("newName"),
			},
		}
		err = ps.Update(credUpdate)
		if !assert.NoError(t, err) {
			return
		}

		// re-fetch to check if name is updated
		refetchedCred, err := ps.Get(service.CredentialModel{
			Session:  service.Session{},
			ID:       "cred-aaaaaaaaaaaaaaaaaaaa",
			Username: "testuser123",
		})
		if !assert.NoError(t, err) {
			return
		}
		assert.Equal(t, "newName", refetchedCred.Name)
		assert.Equal(t, service.CredentialModel{
			Session:           service.Session{},
			ID:                originalCred.ID,
			Name:              "newName",
			Username:          originalCred.Username,
			Type:              originalCred.Type,
			Value:             originalCred.Value,
			Description:       originalCred.Description,
			IsSystem:          originalCred.IsSystem,
			IsHidden:          originalCred.IsHidden,
			Disabled:          originalCred.Disabled,
			Visibility:        originalCred.Visibility,
			Tags:              map[string]string{},
			CreatedAt:         storageschema.MilliSecPrecision(now),
			UpdatedAt:         storageschema.MilliSecPrecision(now), // Note because the time source we used always return the same time, the creation and update timestamp are the same
			UpdatedBy:         "testuser123",
			UpdatedEmulatorBy: "",
		}, refetchedCred)
	})
	t.Run("update description (emulated)", func(t *testing.T) {
		ps := storageCreator(getCurrentTime)

		originalCred := service.CredentialModel{
			Session:     service.Session{},
			ID:          "cred-aaaaaaaaaaaaaaaaaaaa",
			Name:        "credname123",
			Username:    "testuser123",
			Type:        "generic",
			Value:       "value123",
			Description: "description123",
		}
		// create cred
		err := ps.Create(originalCred)
		if err != nil {
			panic(err)
		}

		var credUpdate = types.CredUpdate{
			SessionActor:    "testuser123",
			SessionEmulator: "testuser456",
			Username:        "testuser123",
			ID:              "cred-aaaaaaaaaaaaaaaaaaaa",
			Update: service.CredentialUpdate{
				Description: stringPtr("newDescription"),
			},
		}
		err = ps.Update(credUpdate)
		if !assert.NoError(t, err) {
			return
		}

		// re-fetch to check if name is updated
		refetchedCred, err := ps.Get(service.CredentialModel{
			Session:  service.Session{},
			ID:       "cred-aaaaaaaaaaaaaaaaaaaa",
			Username: "testuser123",
		})
		if !assert.NoError(t, err) {
			return
		}
		assert.Equal(t, "newDescription", refetchedCred.Description)
		assert.Equal(t, "testuser456", refetchedCred.UpdatedEmulatorBy)
	})
	t.Run("update value", func(t *testing.T) {
		ps := storageCreator(getCurrentTime)

		originalCred := service.CredentialModel{
			Session:     service.Session{},
			ID:          "cred-aaaaaaaaaaaaaaaaaaaa",
			Name:        "credname123",
			Username:    "testuser123",
			Type:        "generic",
			Value:       "value123",
			Description: "description123",
		}
		// create cred
		err := ps.Create(originalCred)
		if err != nil {
			panic(err)
		}

		var credUpdate = types.CredUpdate{
			SessionActor:    "testuser123",
			SessionEmulator: "",
			Username:        "testuser123",
			ID:              "cred-aaaaaaaaaaaaaaaaaaaa",
			Update: service.CredentialUpdate{
				Value: stringPtr("new-val"),
			},
		}
		err = ps.Update(credUpdate)
		if !assert.NoError(t, err) {
			return
		}

		// re-fetch to check if name is updated
		refetchedCred, err := ps.Get(service.CredentialModel{
			Session:  service.Session{},
			ID:       "cred-aaaaaaaaaaaaaaaaaaaa",
			Username: "testuser123",
		})
		if !assert.NoError(t, err) {
			return
		}
		assert.Equal(t, "new-val", refetchedCred.Value)
	})
	t.Run("add 1 tag", func(t *testing.T) {
		ps := storageCreator(getCurrentTime)

		originalCred := service.CredentialModel{
			Session:     service.Session{},
			ID:          "cred-aaaaaaaaaaaaaaaaaaaa",
			Name:        "credname123",
			Username:    "testuser123",
			Type:        "generic",
			Value:       "value123",
			Description: "description123",
		}
		// create cred
		err := ps.Create(originalCred)
		if err != nil {
			panic(err)
		}

		var credUpdate = types.CredUpdate{
			SessionActor:    "testuser123",
			SessionEmulator: "",
			Username:        "testuser123",
			ID:              "cred-aaaaaaaaaaaaaaaaaaaa",
			Update: service.CredentialUpdate{
				UpdateOrAddTags: map[string]string{
					"tagName123": "tagValue123",
				},
			},
		}
		err = ps.Update(credUpdate)
		if !assert.NoError(t, err) {
			return
		}

		// re-fetch to check if name is updated
		refetchedCred, err := ps.Get(service.CredentialModel{
			Session:  service.Session{},
			ID:       "cred-aaaaaaaaaaaaaaaaaaaa",
			Username: "testuser123",
		})
		if !assert.NoError(t, err) {
			return
		}
		assert.Equal(t, map[string]string{
			"tagName123": "tagValue123",
		}, refetchedCred.Tags)
		assert.Equal(t, service.CredentialModel{
			Session:     service.Session{},
			ID:          originalCred.ID,
			Name:        originalCred.Name,
			Username:    originalCred.Username,
			Type:        originalCred.Type,
			Value:       originalCred.Value,
			Description: originalCred.Description,
			IsSystem:    originalCred.IsSystem,
			IsHidden:    originalCred.IsHidden,
			Disabled:    originalCred.Disabled,
			Visibility:  originalCred.Visibility,
			Tags: map[string]string{
				"tagName123": "tagValue123",
			},
			CreatedAt:         storageschema.MilliSecPrecision(now),
			UpdatedAt:         storageschema.MilliSecPrecision(now), // Note because the time source we used always return the same time, the creation and update timestamp are the same
			UpdatedBy:         "testuser123",
			UpdatedEmulatorBy: "",
		}, refetchedCred)
	})
	t.Run("delete tags 1", func(t *testing.T) {
		ps := storageCreator(getCurrentTime)
		defer ps.(*PostgresStorage).Close()

		originalCred := service.CredentialModel{
			Session:     service.Session{},
			ID:          "cred-aaaaaaaaaaaaaaaaaaaa",
			Name:        "credname123",
			Username:    "testuser123",
			Type:        "generic",
			Value:       "value123",
			Description: "description123",
			Tags: map[string]string{
				"tagName123": "tagValue123",
			},
		}
		// create cred
		err := ps.Create(originalCred)
		if err != nil {
			panic(err)
		}

		var credUpdate = types.CredUpdate{
			SessionActor:    "testuser123",
			SessionEmulator: "",
			Username:        "testuser123",
			ID:              "cred-aaaaaaaaaaaaaaaaaaaa",
			Update: service.CredentialUpdate{
				DeleteTags: map[string]struct{}{
					"tagName123": {},
				},
			},
		}
		err = ps.Update(credUpdate)
		if !assert.NoError(t, err) {
			return
		}

		// re-fetch to check if name is updated
		refetchedCred, err := ps.Get(service.CredentialModel{
			Session:  service.Session{},
			ID:       "cred-aaaaaaaaaaaaaaaaaaaa",
			Username: "testuser123",
		})
		if !assert.NoError(t, err) {
			return
		}
		assert.Equal(t, service.CredentialModel{
			Session:           service.Session{},
			ID:                originalCred.ID,
			Name:              originalCred.Name,
			Username:          originalCred.Username,
			Type:              originalCred.Type,
			Value:             originalCred.Value,
			Description:       originalCred.Description,
			IsSystem:          originalCred.IsSystem,
			IsHidden:          originalCred.IsHidden,
			Disabled:          originalCred.Disabled,
			Visibility:        originalCred.Visibility,
			Tags:              map[string]string{},
			CreatedAt:         storageschema.MilliSecPrecision(now),
			UpdatedAt:         storageschema.MilliSecPrecision(now), // Note because the time source we used always return the same time, the creation and update timestamp are the same
			UpdatedBy:         "testuser123",
			UpdatedEmulatorBy: "",
		}, refetchedCred)
	})
	t.Run("delete tags that does not exist", func(t *testing.T) {
		ps := storageCreator(getCurrentTime)

		originalCred := service.CredentialModel{
			Session:     service.Session{},
			ID:          "cred-aaaaaaaaaaaaaaaaaaaa",
			Name:        "credname123",
			Username:    "testuser123",
			Type:        "generic",
			Value:       "value123",
			Description: "description123",
		}
		// create cred
		err := ps.Create(originalCred)
		if err != nil {
			panic(err)
		}

		var credUpdate = types.CredUpdate{
			SessionActor:    "testuser123",
			SessionEmulator: "",
			Username:        "testuser123",
			ID:              "cred-aaaaaaaaaaaaaaaaaaaa",
			Update: service.CredentialUpdate{
				DeleteTags: map[string]struct{}{
					"tagName123": {},
				},
			},
		}
		err = ps.Update(credUpdate)
		if !assert.NoError(t, err) { // delete tags that does not exist should NOT result in error
			return
		}

		// re-fetch to check if name is updated
		refetchedCred, err := ps.Get(service.CredentialModel{
			Session:  service.Session{},
			ID:       "cred-aaaaaaaaaaaaaaaaaaaa",
			Username: "testuser123",
		})
		if !assert.NoError(t, err) {
			return
		}
		assert.Equal(t, service.CredentialModel{
			Session:     service.Session{},
			ID:          originalCred.ID,
			Name:        originalCred.Name,
			Username:    originalCred.Username,
			Type:        originalCred.Type,
			Value:       originalCred.Value,
			Description: originalCred.Description,
			IsSystem:    originalCred.IsSystem,
			IsHidden:    originalCred.IsHidden,
			Disabled:    originalCred.Disabled,
			Visibility:  originalCred.Visibility,
			Tags:        map[string]string{},
			CreatedAt:   storageschema.MilliSecPrecision(now),
			// Note even though there is no update, we should update the timestamp
			UpdatedAt:         storageschema.MilliSecPrecision(now), // Note because the time source we used always return the same time, the creation and update timestamp are the same
			UpdatedBy:         "testuser123",
			UpdatedEmulatorBy: "",
		}, refetchedCred)
	})
	t.Run("modify tags & name", func(t *testing.T) {
		ps := storageCreator(getCurrentTime)

		originalCred := service.CredentialModel{
			Session:     service.Session{},
			ID:          "cred-aaaaaaaaaaaaaaaaaaaa",
			Name:        "credname123",
			Username:    "testuser123",
			Type:        "generic",
			Value:       "value123",
			Description: "description123",
			Tags: map[string]string{
				"tagName123": "tagValue123",
				"tagName456": "tagValue456",
				"badTag123":  "badTag",
			},
		}
		// create cred
		err := ps.Create(originalCred)
		if err != nil {
			panic(err)
		}

		var credUpdate = types.CredUpdate{
			SessionActor:    "testuser123",
			SessionEmulator: "",
			Username:        "testuser123",
			ID:              "cred-aaaaaaaaaaaaaaaaaaaa",
			Update: service.CredentialUpdate{
				Name: stringPtr("newName"),
				UpdateOrAddTags: map[string]string{
					"goodTag123": "foobar1",         // add new tag
					"goodTag456": "foobar4",         // add new tag
					"goodTag789": "foobar7",         // add new tag
					"tagName123": "diffTagValue123", // update tag value of existing tag
				},
				DeleteTags: map[string]struct{}{
					"badTag123": {}, // delete existing tag
					"badTag456": {}, // delete non-existing tag
				},
			},
		}
		err = ps.Update(credUpdate)
		if !assert.NoError(t, err) {
			return
		}

		// re-fetch to check if name is updated
		refetchedCred, err := ps.Get(service.CredentialModel{
			Session:  service.Session{},
			ID:       "cred-aaaaaaaaaaaaaaaaaaaa",
			Username: "testuser123",
		})
		if !assert.NoError(t, err) {
			return
		}
		assert.Equal(t, service.CredentialModel{
			Session:     service.Session{},
			ID:          originalCred.ID,
			Name:        "newName",
			Username:    originalCred.Username,
			Type:        originalCred.Type,
			Value:       originalCred.Value,
			Description: originalCred.Description,
			IsSystem:    originalCred.IsSystem,
			IsHidden:    originalCred.IsHidden,
			Disabled:    originalCred.Disabled,
			Visibility:  originalCred.Visibility,
			Tags: map[string]string{
				"tagName123": "diffTagValue123",
				"tagName456": "tagValue456",
				"goodTag456": "foobar4",
				"goodTag789": "foobar7",
				"goodTag123": "foobar1",
			},
			CreatedAt:         storageschema.MilliSecPrecision(now),
			UpdatedAt:         storageschema.MilliSecPrecision(now), // Note because the time source we used always return the same time, the creation and update timestamp are the same
			UpdatedBy:         "testuser123",
			UpdatedEmulatorBy: "",
		}, refetchedCred)
	})
}

func stringPtr(s string) *string {
	return &s
}

func boolPtr(b bool) *bool {
	return &b
}
