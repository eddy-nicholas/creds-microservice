package adapters

import (
	"context"
	"fmt"
	"github.com/nats-io/nats.go"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
	"sync"
	"time"

	"gitlab.com/cyverse/creds-microservice/types"
)

// QueryAdapter is an example of a Driver Adapter.
type QueryAdapter struct {
	config        types.Config
	getDomainChan chan types.QueryMessage
	nc            *nats.Conn
	sub           *nats.Subscription
}

// NewNATSQueryAdapter ...
func NewNATSQueryAdapter() *QueryAdapter {
	return &QueryAdapter{}
}

// Init is the init function required per the Port interface
func (q *QueryAdapter) Init(c types.Config) {
	log.Debug("starting adapters.QueryAdapter.Init()")
	q.config = c
	log.WithFields(log.Fields{
		"NatsURL":     q.config.NatsConfig.URL,
		"NatsSubject": q.config.NatsConfig.WildcardSubject,
	}).Info()
}

// InitChannel initialize the shared channel with the Domain object
func (q *QueryAdapter) InitChannel(dc chan types.QueryMessage, waitgroup *sync.WaitGroup) {
	q.getDomainChan = dc
}

// Start tells the adapter to start listening for updates. In this case the NATS subscriber would
// listen to a message and then call doAction whenever a message is received.
func (q *QueryAdapter) Start(ctx context.Context) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "QueryAdapter.Start",
	})
	logger.Debug("starting adapter")

	q.nc = natsConnect(logger, q.config.NatsConfig)
	q.natsSubscribe(logger, q.config.NatsConfig)
}

func (q *QueryAdapter) natsSubscribe(logger *log.Entry, natsConfig messaging.NatsConfig) {
	logger.WithField("subject", natsConfig.WildcardSubject).Debug("subscribing on subject")
	sub, err := q.nc.QueueSubscribe(natsConfig.WildcardSubject, natsConfig.QueueGroup, q.handleNATSMsg)
	if err != nil {
		logger.WithError(err).Fatal("cannot create a queued async subscription")
		// terminate process if error
	}
	q.sub = sub
}

func (q *QueryAdapter) handleNATSMsg(msg *nats.Msg) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "QueryAdapter.handleNATSMsg",
	})
	query, err := natsMsgToQuery(msg)
	if err != nil {
		// skip if cannot convert to QueryMessage
		log.WithError(err).Error("cannot convert to QueryMessage")
		return
	}
	logger.Trace("sending domain data")
	select {
	case q.getDomainChan <- query:
		logger.Debug("done sending domain data")
	case <-time.After(time.Second * 30):
		logger.Error("query chan blocked for too long, cannot send msg")
		break
	}

	logger.Trace("waiting for domain to reply with data...")
	resp := <-query.Response
	log.Debug("received response from domain, sending back to NATS")

	replyBytes := marshalQueryResponse(resp, query.Type)
	err = msg.Respond(replyBytes)
	if err != nil {
		logger.WithError(err).Error("fail to write reply to NATS")
	}
}

// Close closes the connection
func (q *QueryAdapter) Close() {
	log.Info("closing NATS connection for query adapter")
	q.nc.Close()
}

func natsMsgToQuery(msg *nats.Msg) (types.QueryMessage, error) {
	ce, err := messaging.ConvertNats(msg)
	if err != nil {
		return types.QueryMessage{}, err
	}

	if ce.Data() == nil {
		return types.QueryMessage{}, fmt.Errorf("cloudevent data was nil")
	}

	var queryObj interface{}
	switch ce.Type() {
	case service.NatsSubjectCredentialsGet:
		var dataFormat service.CredentialGetRequest
		if err := ce.DataAs(&dataFormat); err != nil {
			return types.QueryMessage{}, fmt.Errorf("fail to unmarshal cloudevent data, %w", err)
		}
		queryObj = dataFormat
	case service.NatsSubjectCredentialsList:
		var dataFormat service.CredentialListRequest
		if err := ce.DataAs(&dataFormat); err != nil {
			return types.QueryMessage{}, fmt.Errorf("fail to unmarshal cloudevent data, %w", err)
		}
		queryObj = dataFormat
	default:
		return types.QueryMessage{}, fmt.Errorf("unknown query, %s", ce.Type())
	}

	return types.QueryMessage{
		Type:     ce.Type(),
		Obj:      queryObj,
		Response: make(chan types.QueryMessage),
	}, nil
}

func marshalQueryResponse(response types.QueryMessage, queryType string) []byte {

	switch queryType {
	case "cyverse.credentials.get":
		idata := response.Obj.(service.CredentialGetReply)
		return CreateResponseCE("cyverse.creds", response.Type, idata)
	case "cyverse.credentials.list":
		idata := response.Obj.(service.CredentialListReply)
		return CreateResponseCE("cyverse.creds", response.Type, idata.List)
	default:
		panic("unknown query, should not get here")
	}
}

func natsConnect(logger *log.Entry, natsConfig messaging.NatsConfig) *nats.Conn {
	nc, err := nats.Connect(natsConfig.URL, nats.MaxReconnects(messaging.DefaultNatsMaxReconnect), nats.ReconnectWait(messaging.DefaultNatsReconnectWait))
	if err != nil {
		logger.WithFields(log.Fields{
			"url":           natsConfig.URL,
			"maxReconnect":  messaging.DefaultNatsMaxReconnect,
			"reconnectWait": messaging.DefaultNatsReconnectWait,
		}).WithError(err).Fatal("cannot connect to nats")
		// terminate process if error
	}
	logger.Debug("connected to nats")
	return nc
}
