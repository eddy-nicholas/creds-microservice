package adapters

import (
	"context"
	"encoding/json"
	"github.com/nats-io/stan.go"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/creds-microservice/types"
	"sync"
	"testing"
	"time"
)

func TestStanEventAdapterIncoming(t *testing.T) {
	if skipIfSTANEnvNotSet(t) {
		return
	}
	testSetup := newStanTestSetup(defaultTestConfig())
	testSetup.Setup()
	defer testSetup.Cleanup()

	t.Run("create cred", func(t *testing.T) {
		credRequest := &service.CredentialCreateRequest{
			Session: service.Session{
				SessionActor:    "testuser-123",
				SessionEmulator: "testemulator-123",
			},
			ID:                "cred-123",
			Name:              "cred-123",
			Username:          "testuser-123",
			Type:              "type123",
			Value:             "value123",
			Description:       "",
			IsSystem:          false,
			IsHidden:          false,
			Visibility:        "",
			Tags:              nil,
			CreatedAt:         time.Time{},
			UpdatedAt:         time.Time{},
			UpdatedBy:         "",
			UpdatedEmulatorBy: "",
		}

		tid := messaging.NewTransactionID()
		testSetup.publishSTANMsg(service.EventCredentialAddRequested, tid, credRequest)
		select {
		case event := <-testSetup.eventChan:
			assert.Equal(t, string(service.EventCredentialAddRequested), event.Type)
			assert.Equal(t, tid, event.TID)
			assert.NotNil(t, event.Obj)
			if !assert.IsType(t, service.CredentialCreateRequest{}, event.Obj) {
				return
			}
			cred := event.Obj.(service.CredentialCreateRequest)
			assert.Equal(t, *credRequest, cred)

		case <-time.After(time.Second * 5):
			assert.Fail(t, "timed out, no event received from channel")
			return
		}
	})

	t.Run("delete cred", func(t *testing.T) {
		credRequest := &service.CredentialDeleteRequest{
			Session: service.Session{
				SessionActor:    "testuser-123",
				SessionEmulator: "testemulator-123",
			},
			Username: "testuser-123",
			ID:       "cred-123",
		}

		tid := messaging.NewTransactionID()
		testSetup.publishSTANMsg(service.EventCredentialDeleteRequested, tid, credRequest)
		select {
		case event := <-testSetup.eventChan:
			assert.Equal(t, string(service.EventCredentialDeleteRequested), event.Type)
			assert.Equal(t, tid, event.TID)
			assert.NotNil(t, event.Obj)
			if !assert.IsType(t, service.CredentialDeleteRequest{}, event.Obj) {
				return
			}
			cred := event.Obj.(service.CredentialDeleteRequest)
			assert.Equal(t, *credRequest, cred)

		case <-time.After(time.Second * 5):
			assert.Fail(t, "timed out, no event received from channel")
			return
		}
	})

	t.Run("update cred", func(t *testing.T) {
		credRequest := &service.CredentialUpdateRequest{
			Session: service.Session{
				SessionActor:    "testuser-123",
				SessionEmulator: "testemulator-123",
			},
			Username: "testuser-123",
			ID:       "cred-123",
			Update: service.CredentialUpdate{
				Name: stringPtr("new-name123"),
			},
		}

		tid := messaging.NewTransactionID()
		testSetup.publishSTANMsg(service.EventCredentialUpdateRequested, tid, credRequest)
		select {
		case event := <-testSetup.eventChan:
			assert.Equal(t, string(service.EventCredentialUpdateRequested), event.Type)
			assert.Equal(t, tid, event.TID)
			assert.NotNil(t, event.Obj)
			if !assert.IsType(t, service.CredentialUpdateRequest{}, event.Obj) {
				return
			}
			cred := event.Obj.(service.CredentialUpdateRequest)
			assert.Equal(t, *credRequest, cred)

		case <-time.After(time.Second * 5):
			assert.Fail(t, "timed out, no event received from channel")
			return
		}
	})
}

func TestStanEventAdapterOutgoing(t *testing.T) {
	if skipIfSTANEnvNotSet(t) {
		return
	}
	config := defaultTestConfig()

	testSetup := newStanTestSetup(config)
	testSetup.Setup()
	defer testSetup.Cleanup()

	tid := messaging.NewTransactionID()
	payloadObj := service.CredentialModel{
		Session: service.Session{
			SessionActor:    "testuser-123",
			SessionEmulator: "",
		},
		Username: "testuser-123",
		Value:    "",
		Type:     "",
		ID:       "cred-123",
	}

	// setup assertion connection and subscription to listen for events
	doneChan := make(chan struct{})
	assertionConn := testSetup.stanAssert(t, service.EventCredentialAdded, tid, payloadObj, doneChan)
	defer assertionConn.Close()

	// publish events via adapter
	err := testSetup.adapter.PublishEvent(service.EventCredentialAdded, payloadObj, tid)
	if err != nil {
		panic(err)
	}

	// check if assertion subscription has captured a matching events
	select {
	case <-doneChan:
		assert.Truef(t, true, "msg is received")
	case <-time.After(time.Second * 2):
		assert.Fail(t, "timed out, did not receive the msg sent")
	}
}

type eventTestSetup struct {
	adapter   *StanEventAdapter
	wg        *sync.WaitGroup
	eventChan chan types.EventMessage
	config    types.Config
	cancel    context.CancelFunc
}

func newStanTestSetup(config types.Config) eventTestSetup {
	config.NatsConfig.ClientID = newStanClientID() // generate a new client ID per test setup
	adapter := &StanEventAdapter{}
	adapter.Init(config)

	channel := make(chan types.EventMessage)
	var wg sync.WaitGroup
	wg.Add(1)
	adapter.InitChannel(channel, &wg)

	return eventTestSetup{
		adapter:   adapter,
		wg:        &wg,
		eventChan: channel,
		config:    config,
	}
}

func (setup *eventTestSetup) Setup() {
	ctx, cancel := context.WithCancel(context.Background())
	setup.cancel = cancel

	go setup.adapter.Start(ctx)
	time.Sleep(time.Second * 2) // wait for adapter to connect to STAN
}

func (setup *eventTestSetup) Cleanup() {
	if setup.cancel != nil {
		setup.cancel()
	}
	setup.adapter.Close()
}

func (setup *eventTestSetup) publishSTANMsg(eventType common.EventType, tid common.TransactionID, data interface{}) {
	conn, err := messaging.ConnectStanForServiceClient(&messaging.NatsConfig{
		URL:             setup.config.NatsConfig.URL,
		QueueGroup:      setup.config.NatsConfig.QueueGroup,
		WildcardSubject: setup.config.NatsConfig.WildcardSubject,
		ClientID:        newStanClientID(),
		MaxReconnects:   0,
		ReconnectWait:   0,
		RequestTimeout:  0,
	}, &messaging.StanConfig{
		ClusterID:     setup.config.StanConfig.ClusterID,
		DurableName:   setup.config.StanConfig.DurableName,
		EventsTimeout: 0,
	})
	if err != nil {
		panic(err)
	}
	err = conn.PublishWithTransactionID(eventType, data, tid)
	if err != nil {
		panic(err)
	}
}

// setup assertion connection and subscription to listen for events, and check on the event that it received.
func (setup *eventTestSetup) stanAssert(t *testing.T, expectedEventType common.EventType, expectedTID common.TransactionID, payloadObj service.CredentialModel, doneChan chan<- struct{}) stan.Conn {
	conn, err := stan.Connect(setup.config.StanConfig.ClusterID, newStanClientID(), stan.NatsURL(setup.config.NatsConfig.URL))
	if err != nil {
		panic(err)
	}
	_, err = conn.Subscribe(common.EventsSubject, func(msg *stan.Msg) {
		ce, err := messaging.ConvertStan(msg)
		assert.NoError(t, err)
		var received map[string]interface{}
		err = json.Unmarshal(ce.Data(), &received)
		assert.NoError(t, err)
		payloadMap := credToJsonToMap(payloadObj)
		assert.Equal(t, payloadMap, received)

		doneChan <- struct{}{}
	})
	if err != nil {
		panic(err)
	}
	return conn
}

func newStanClientID() string {
	return common.NewID("cred-svc-unit-test").String()
}

func credToJsonToMap(cred service.CredentialModel) map[string]interface{} {
	bytesMarshaled, err := json.Marshal(cred)
	if err != nil {
		panic(err)
	}
	var result map[string]interface{}
	err = json.Unmarshal(bytesMarshaled, &result)
	if err != nil {
		panic(err)
	}
	return result
}
