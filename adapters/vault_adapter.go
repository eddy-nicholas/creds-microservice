// Package adapters provides functions and structs for interacting with Vault for
// storing user secrets.
package adapters

import (
	"fmt"
	vault "github.com/hashicorp/vault/api"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/creds-microservice/adapters/storageschema"
	"gitlab.com/cyverse/creds-microservice/ports"
	"gitlab.com/cyverse/creds-microservice/types"
	"time"
)

const (
	// mount path of the KV (version 1) secret engine, a path prefix for all credentials
	vaultNamespace = "secret/"
)

// NewVaultCredentialStorage creates a new Vault client for interacting with the Vault server
func NewVaultCredentialStorage(cfg types.Config, timeSrc ports.TimeSrc) (*VaultCredentialStorage, error) {
	vaultClient, err := vaultClientFromConfig(cfg)
	if err != nil {
		return nil, err
	}
	err = pingVault(vaultClient)
	if err != nil {
		return nil, err
	}
	return &VaultCredentialStorage{
		vapi: vaultAPI{
			// client for logical backend (including KV secret engine)
			client: vaultClient.Logical(),
		},
		getCurrentTime: timeSrc,
	}, nil
}

func vaultClientFromConfig(cfg types.Config) (*vault.Client, error) {
	config := vault.DefaultConfig()
	config.Timeout = time.Second * 3
	config.Address = cfg.VaultAddress
	client, err := vault.NewClient(config)
	if err != nil {
		log.WithError(err).Error("Error creating client")
		return nil, err
	}
	client.SetToken(cfg.VaultToken)
	return client, err
}

// send a request to "ping" vault api to check if it is available
func pingVault(vaultClient *vault.Client) error {
	_, err := vaultClient.Logical().Read(vaultNamespace)
	if err != nil {
		log.WithError(err).Error("fail to ping vault server")
		return err
	}
	return nil
}

// VaultCredentialStorage is a storage backend based on Vault's KV (version 1) secret engine
type VaultCredentialStorage struct {
	vapi           vaultAPI
	getCurrentTime ports.TimeSrc
}

// Init do nothing, just implement interface PersistentStoragePort
func (storage VaultCredentialStorage) Init(c types.Config) {}

// Create creates a credential.
// Note: this will overwrite the credential with the same ID and username(owner).
func (storage VaultCredentialStorage) Create(credModel service.CredentialModel) error {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "VaultCredentialStorage.Create",
		"username": credModel.Username,
		"ID":       credModel.ID,
	})
	now := storage.getCurrentTime()
	credModel.CreatedAt = now
	credModel.UpdatedAt = now

	secretData, err := storageschema.ServiceCredToVaultSecret(credModel)
	if err != nil {
		logger.WithError(err).Error("fail to create convert credential to vault secret")
		return err
	}
	err = storage.vapi.Write(credModel.Username, credModel.ID, secretData)
	if err != nil {
		logger.WithError(err).Error("fail to create cred")
		return err
	}
	logger.Info("cred created")
	return nil
}

// Update ...
// TODO This function should likely be looked at in the future, as of right now it will just overwrite any existing
// secret with the same key name, but it could be troublesome under certain update sequences.
func (storage VaultCredentialStorage) Update(credUpdate types.CredUpdate) error {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "VaultCredentialStorage.Update",
		"username": credUpdate.Username,
		"ID":       credUpdate.ID,
	})
	logger.Warn("update operation is not supported with vault storage adapter")
	return nil
}

// Delete deletes a credential by ID and username(owner).
func (storage VaultCredentialStorage) Delete(credModel service.CredentialModel) error {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "VaultCredentialStorage.Delete",
		"username": credModel.Username,
		"ID":       credModel.ID,
	})
	err := storage.vapi.Delete(credModel.Username, credModel.ID)
	if err != nil {
		logger.WithError(err).Error("fail to delete cred")
		return err
	}
	logger.Info("cred deleted")
	return nil
}

// Get fetches a credential based on ID and username(owner).
func (storage VaultCredentialStorage) Get(credModel service.CredentialModel) (service.CredentialModel, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "VaultCredentialStorage.Get",
		"username": credModel.Username,
		"ID":       credModel.ID,
	})
	credential, err := storage.getCredential(logger, credModel.Username, credModel.ID)
	if err != nil {
		return service.CredentialModel{}, err
	}
	logger.Debug("cred fetched")
	return credential, nil
}

func (storage VaultCredentialStorage) getCredential(logger *log.Entry, username, credID string) (service.CredentialModel, error) {
	secretData, err := storage.vapi.Get(username, credID)
	if err != nil {
		logger.WithError(err).Error("fail to fetch cred")
		return service.CredentialModel{}, err
	}
	cred, err := storageschema.VaultSecretToServiceCred(username, credID, secretData)
	if err != nil {
		logger.WithError(err).Error("fail to convert vault secret to service model")
		return service.CredentialModel{}, err
	}
	return cred, nil
}

// List returns a list of all credential owned by a user.
// Note: invalid credential(credential that missing required field) are discarded.
// TODO consider make concurrent calls for fetching list items
func (storage VaultCredentialStorage) List(filter types.ListQueryData) ([]service.CredentialModel, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "VaultCredentialStorage.List",
		"username": filter.Username,
	})
	var result []service.CredentialModel

	credIDList, err := storage.vapi.ListIDs(filter.Username)
	if err != nil {
		logger.WithError(err).Error("fail to list credential names for user")
		return nil, err
	}
	logger.WithField("len", len(credIDList)).Debug("listed all credential IDs for user")

	// TODO consider make concurrent calls for fetching list items
	for _, id := range credIDList {
		cred, err := storage.readListItem(logger, filter.Username, id)
		if err != nil {
			logger.WithFields(log.Fields{
				"credID": id,
			}).WithError(err).Error("skip errored list item")
			continue
		}
		result = append(result, cred)
	}
	logger.WithField("len", len(result)).Info("fetched all credential for user")
	if len(credIDList) != len(result) {
		logger.Warn("failed to retrieve some item in the list")
	}
	return result, nil
}

func (storage VaultCredentialStorage) readListItem(logger *log.Entry, username, credID string) (service.CredentialModel, error) {
	credential, err := storage.getCredential(logger, username, credID)
	if err != nil {
		return service.CredentialModel{}, err
	}
	// redact value field for list output
	return storage.redactCredentialValue(credential), nil
}

func (storage VaultCredentialStorage) redactCredentialValue(cred service.CredentialModel) service.CredentialModel {
	cred.Value = "REDACTED"
	return cred
}

// encapsulate vault API calls and error handling
type vaultAPI struct {
	// client for logical backend (including KV secret engine)
	client *vault.Logical
}

// Write writes a credential, this will overwrite the credential with same owner and ID.
func (vapi vaultAPI) Write(owner, credID string, cred storageschema.VaultSecretData) error {
	_, err := vapi.client.Write(vaultCredentialPath(owner, credID), cred)
	if err != nil {
		return err
	}
	return nil
}

// Delete deletes a credential
func (vapi vaultAPI) Delete(owner, credID string) error {
	secret, err := vapi.client.Delete(vaultCredentialPath(owner, credID))
	if err != nil {
		return err
	}
	if secret == nil {
		// not found
		log.WithFields(log.Fields{
			"package":  "aadpters",
			"function": "vaultAPI.Delete",
		}).Debug("delete non-existent credential")
		return nil
	}
	return nil
}

// Get fetches a single credential
func (vapi vaultAPI) Get(owner, credID string) (storageschema.VaultSecretData, error) {
	secret, err := vapi.client.Read(vaultCredentialPath(owner, credID))
	if err != nil {
		return nil, err
	}
	if secret == nil {
		return nil, fmt.Errorf("credential '%s' not found", credID)
	}
	if !isValidSecret(secret) {
		return nil, fmt.Errorf("credential '%s' was found, but appears to be corrupted", credID)
	}
	if secret.Data == nil {
		// return empty map instead
		return storageschema.VaultSecretData{}, nil
	}
	return secret.Data, nil
}

// ListIDs lists all credential IDs for a user, reutrn []string{} if user has no credentials.
func (vapi vaultAPI) ListIDs(owner string) ([]string, error) {
	list, err := vapi.client.List(vaultUserBasePath(owner))
	if err != nil {
		return nil, err
	}
	if list == nil {
		// if empty list (user has no cred), vault will return nil
		return []string{}, nil
	}
	return vapi.extractIDListFromSecret(list)
}

func (vapi vaultAPI) extractIDListFromSecret(list *vault.Secret) ([]string, error) {
	if len(list.Data) == 0 {
		return nil, fmt.Errorf("vault returns empty data in list response")
	}
	credIDListRaw, ok := list.Data["keys"]
	if !ok {
		return nil, fmt.Errorf("'keys' key missing from list response from vault")
	}
	credIDRawList, ok := credIDListRaw.([]interface{})
	if !ok {
		return nil, fmt.Errorf("list response does not return []interface for credential names")
	}
	var credIDList = make([]string, 0)
	for _, credIDRaw := range credIDRawList {
		credID, ok := credIDRaw.(string)
		if !ok {
			return nil, fmt.Errorf("list response does not return string for credential name")
		}
		credIDList = append(credIDList, credID)
	}
	return credIDList, nil
}

func isValidSecret(secret *vault.Secret) bool {
	if secret.Data == nil {
		return false
	}
	if _, ok := secret.Data["Value"]; !ok {
		return false
	}
	if _, ok := secret.Data["Type"]; !ok {
		return false
	}
	return true
}

// base path for a single user's credentials
func vaultUserBasePath(owner string) string {
	return vaultNamespace + owner + "/secrets/"
}

func vaultCredentialPath(owner, credID string) string {
	return vaultNamespace + owner + "/secrets/" + credID
}
