package adapters

import (
	"context"
	"fmt"
	"github.com/kelseyhightower/envconfig"
	"github.com/nats-io/nats.go"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/creds-microservice/types"
	"sync"
	"testing"
	"time"
)

// Stand up a test NATS server and point query adapter to it.
// Publish msg to the test NATS server and check the query channel.
// This tests the external behavior of the query adapter using only the public methods.
func TestNATSQueryAdapterChannel(t *testing.T) {
	if skipIfSTANEnvNotSet(t) {
		return
	}
	var testSetup queryTestSetup
	testSetup.Setup(t)
	defer testSetup.Cleanup(t)

	t.Run("get cred", func(t *testing.T) {
		// publish query as NATS msg
		credential := service.CredentialModel{
			Session:  service.Session{SessionActor: "cacao-admin", SessionEmulator: ""},
			Username: "cacao-admin",
			ID:       "Test",
		}
		testSetup.publishNATSMsg(credential, service.NatsSubjectCredentialsGet)

		// wait for query in channel
		query, err := testSetup.waitForQueryFromChannel()
		assert.NoError(t, err)

		// check query
		assert.Equal(t, service.NatsSubjectCredentialsGet, query.Type)
		if !assert.NotNil(t, query.Obj) {
			return
		}
		assert.IsType(t, service.CredentialGetRequest{}, query.Obj)
		if !assert.NotNil(t, query.Response) {
			return
		}

		// reply
		query.Response <- types.QueryMessage{
			Type: "",
			Obj: service.CredentialGetReply{
				Username: "cacao-admin",
				ID:       "Test",
				Value:    "value123",
			},
			Response: nil,
		}
	})

	t.Run("list cred", func(t *testing.T) {
		// publish query as NATS msg
		credential := service.CredentialModel{
			Session:  service.Session{SessionActor: "cacao-admin", SessionEmulator: ""},
			Username: "cacao-admin",
			ID:       "Test",
		}
		testSetup.publishNATSMsg(credential, service.NatsSubjectCredentialsList)

		// wait for query in channel
		query, err := testSetup.waitForQueryFromChannel()
		assert.NoError(t, err)

		// check query
		assert.Equal(t, service.NatsSubjectCredentialsList, query.Type)
		if !assert.NotNil(t, query.Obj) {
			return
		}
		assert.IsType(t, service.CredentialListRequest{}, query.Obj)
		if !assert.NotNil(t, query.Response) {
			return
		}

		// reply
		query.Response <- types.QueryMessage{
			Type: "",
			Obj: service.CredentialListReply{
				Session:  service.Session{SessionActor: "cacao-admin", SessionEmulator: ""},
				Username: "cacao-admin",
				List: []service.CredentialModel{
					{
						Username: "cacao-admin",
						ID:       "Test",
						Value:    "value123",
					},
				},
			},
			Response: nil,
		}
	})
}

type queryTestSetup struct {
	ctx          context.Context
	cancel       context.CancelFunc
	config       types.Config
	queryChannel chan types.QueryMessage
	wg           *sync.WaitGroup
	queryAdapter *QueryAdapter
}

func (setup *queryTestSetup) Setup(t *testing.T) {

	ctx, cancel := context.WithCancel(context.Background())
	setup.ctx = ctx
	setup.cancel = cancel

	setup.queryChannel = make(chan types.QueryMessage)
	setup.wg = &sync.WaitGroup{}
	setup.wg.Add(1)

	setup.config = defaultTestConfig()

	setup.setupQueryAdapter()
}

func (setup *queryTestSetup) setupQueryAdapter() {
	setup.queryAdapter = &QueryAdapter{}
	setup.queryAdapter.Init(setup.config)
	setup.queryAdapter.InitChannel(setup.queryChannel, setup.wg)

	// launch on separate go routine because Start() is blocking
	go setup.queryAdapter.Start(setup.ctx)

	// this waits for query adapter to connect to NATS, otherwise it won't receive the msg we publish later
	time.Sleep(2 * time.Second)
}

func (setup *queryTestSetup) Cleanup(t *testing.T) {
	// close the domain channel and then wait
	setup.cancel()
	t.Log("waiting for adapter to close")
	//setup.wg.Wait()
	t.Log("closing domainChannel")
	close(setup.queryChannel)
}

func (setup *queryTestSetup) publishNATSMsg(cred service.CredentialModel, queryType string) {
	nc, err := nats.Connect(setup.config.NatsConfig.URL)
	if err != nil {
		panic(err)
	}

	event, err := messaging.CreateCloudEvent(cred, queryType, "clientID")
	if err != nil {
		panic(err)
	}
	marshal, _ := event.MarshalJSON()

	msg := nats.NewMsg(queryType)
	msg.Data = marshal
	msg.Reply = nats.NewInbox()
	err = nc.PublishMsg(msg)
	if err != nil {
		panic(err)
	}
	err = nc.Flush()
	if err != nil {
		panic(err)
	}
	nc.Close()
}

func (setup *queryTestSetup) waitForQueryFromChannel() (types.QueryMessage, error) {
	select {
	case query := <-setup.queryChannel:
		return query, nil
	case <-time.After(time.Second * 5):
		return types.QueryMessage{}, fmt.Errorf("timed out, no query received from channel")
	}
}

func defaultTestConfig() types.Config {
	var config types.Config
	err := envconfig.Process("", &config)
	if err != nil {
		panic(err)
	}
	config.ProcessDefaults()
	return config
}
