package adapters

import (
	"context"
	"database/sql"
	_ "embed"
	"fmt"
	_ "github.com/jackc/pgx/v5/stdlib"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/creds-microservice/adapters/storageschema"
	"gitlab.com/cyverse/creds-microservice/ports"
	"gitlab.com/cyverse/creds-microservice/types"
	"time"
)

// PostgresStorage is a persistent storage for credential based on postgres.
type PostgresStorage struct {
	db             *sql.DB
	getCurrentTime ports.TimeSrc
}

// NewPostgresStorage ...
func NewPostgresStorage(src ports.TimeSrc) *PostgresStorage {
	return &PostgresStorage{
		db:             nil,
		getCurrentTime: src,
	}
}

func pgxConnStr(conf types.Config) string {
	// postgres://username:password@localhost:5432/database_name
	return fmt.Sprintf("postgres://%s:%s@%s/%s", conf.PostgresUsername, conf.PostgresPassword, conf.PostgresHost, conf.PostgresDatabase)
}

// Init initialize database connection from config
func (ps *PostgresStorage) Init(conf types.Config) {
	connStr := pgxConnStr(conf)
	db, err := sql.Open("pgx", connStr)
	if err != nil {
		log.WithError(err).Fatal()
	}
	ps.db = db
	ctx, cancelFunc := context.WithTimeout(context.Background(), time.Second*10)
	defer cancelFunc()
	err = ps.db.PingContext(ctx)
	if err != nil {
		log.WithError(err).Fatal()
	}
	log.Info("successfully connected to postgres")

	if conf.PostgresCreateTablesIfMissing {
		exists, err := ps.checkTableExists()
		if err != nil {
			log.WithError(err).Fatal()
		}
		if !exists {
			err := ps.InitDatabase()
			if err != nil {
				log.WithError(err).Fatal()
			}
		}
	}
}

// Create ...
func (ps *PostgresStorage) Create(cred service.CredentialModel) error {
	ctx, cancelFunc := ps.newCtx()
	defer cancelFunc()

	tx, err := ps.db.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	var createEmulatedBy = "" // TODO we dont have this information right now, leave this as NULL for now; update this when we added it the credential model.

	now := ps.getCurrentTime()
	cred.CreatedAt = storageschema.MilliSecPrecision(now)
	cred.UpdatedAt = storageschema.MilliSecPrecision(now)

	const sqlInsertMain = `INSERT INTO credential (id, name, username, type, description, value,
is_system, visibility, disabled, created_at, updated_at, created_emulator_by, updated_by, updated_emulator_by)
VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14);`
	_, err = tx.Exec(sqlInsertMain, cred.ID, cred.Name, cred.Username, cred.Type, cred.Description, cred.Value,
		cred.IsSystem, cred.Visibility, false, cred.CreatedAt, cred.UpdatedAt, createEmulatedBy, cred.UpdatedBy, cred.UpdatedEmulatorBy)
	if err != nil {
		return err
	}
	const sqlInsertTags = `INSERT INTO credential_tags (id, tag_name, tag_value) VALUES ($1, $2, $3);`
	for tagName, tagValue := range cred.Tags {
		_, err = tx.Exec(sqlInsertTags, cred.ID, tagName, tagValue)
		if err != nil {
			return err
		}
	}
	err = tx.Commit()
	if err != nil {
		return err
	}
	log.WithFields(log.Fields{
		"username": cred.Username,
		"id":       cred.ID,
		"name":     cred.Name,
	}).Info("credential created")
	return nil
}

// Import imports a credential, timestamps (creation, update) are  not overwritten.
func (ps *PostgresStorage) Import(cred service.CredentialModel) error {
	ctx, cancelFunc := ps.newCtx()
	defer cancelFunc()

	tx, err := ps.db.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	var createEmulatedBy = "" // TODO we dont have this information right now, leave this as NULL for now; update this when we added it the credential model.

	// do not overwrite CreatedAt and UpdatedAt timestamp

	const sqlInsertMain = `INSERT INTO credential (id, name, username, type, description, value,
is_system, visibility, disabled, created_at, updated_at, created_emulator_by, updated_by, updated_emulator_by)
VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14);`
	_, err = tx.Exec(sqlInsertMain, cred.ID, cred.Name, cred.Username, cred.Type, cred.Description, cred.Value,
		cred.IsSystem, cred.Visibility, false, cred.CreatedAt, cred.UpdatedAt, createEmulatedBy, cred.UpdatedBy, cred.UpdatedEmulatorBy)
	if err != nil {
		return err
	}
	const sqlInsertTags = `INSERT INTO credential_tags (id, tag_name, tag_value) VALUES ($1, $2, $3);`
	for tagName, tagValue := range cred.Tags {
		_, err = tx.Exec(sqlInsertTags, cred.ID, tagName, tagValue)
		if err != nil {
			return err
		}
	}
	err = tx.Commit()
	if err != nil {
		return err
	}
	log.WithFields(log.Fields{
		"username": cred.Username,
		"id":       cred.ID,
		"name":     cred.Name,
	}).Info("credential imported")
	return nil
}

// Delete ...
func (ps *PostgresStorage) Delete(cred service.CredentialModel) error {
	ctx, cancelFunc := ps.newCtx()
	defer cancelFunc()

	tx, err := ps.db.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	// Check if credential exists, lock the row if exists
	const sqlSelect = `SELECT id FROM credential WHERE id=$1 AND username=$2 FOR UPDATE;`
	row := tx.QueryRow(sqlSelect, cred.ID, cred.Username) // pass in username to ensure that the credential belongs to user
	err = row.Err()
	if err == sql.ErrNoRows {
		// if credential does not exist then return error
		return fmt.Errorf("credential not found")
	} else if err != nil {
		log.WithError(err).Error("fail to query credential")
		return err
	} else {
		// scan the row to close it
		var credID string
		_ = row.Scan(&credID)
	}

	const sqlDeleteTags = `DElETE FROM credential_tags WHERE id=$1`
	_, err = tx.Exec(sqlDeleteTags, cred.ID)
	if err != nil {
		log.WithError(err).Error("fail to delete tags")
		return err
	}
	const sqlDelete = `DElETE FROM credential WHERE id=$1 AND username=$2;`
	_, err = tx.Exec(sqlDelete, cred.ID, cred.Username)
	if err != nil {
		log.WithError(err).Error("fail to delete cred")
		return err
	}

	err = tx.Commit()
	if err != nil {
		log.WithError(err).Error("fail to commit deletion")
		return err
	}
	log.WithFields(log.Fields{
		"username": cred.Username,
		"id":       cred.ID,
	}).Info("credential deleted")
	return nil
}

// Get ...
func (ps *PostgresStorage) Get(cred service.CredentialModel) (service.CredentialModel, error) {
	const sqlGet = `SELECT credential.id, credential.name, credential.username, credential.type,
credential.description, credential.value, credential.is_system, credential.visibility, credential.created_at, credential.updated_at,
credential.updated_by, credential.updated_emulator_by,
credential_tags.tag_name, credential_tags.tag_value
FROM credential LEFT JOIN credential_tags ON (credential.id = credential_tags.id) WHERE credential.id = $1 AND credential.username = $2`

	ctx, cancelFunc := ps.newCtx()
	defer cancelFunc()

	rows, err := ps.db.QueryContext(ctx, sqlGet, cred.ID, cred.Username)
	if err != nil {
		return service.CredentialModel{}, err
	}
	defer rows.Close()

	var emptyResult = true
	var result service.CredentialModel
	var tagName *string
	var tagValue *string
	result.Tags = map[string]string{}
	for rows.Next() {
		emptyResult = false
		err = rows.Scan(
			&result.ID, &result.Name, &result.Username, &result.Type, &result.Description, &result.Value,
			&result.IsSystem, &result.Visibility, &result.CreatedAt, &result.UpdatedAt, &result.UpdatedBy, &result.UpdatedEmulatorBy,
			&tagName, &tagValue,
		)
		if err != nil {
			return service.CredentialModel{}, err
		}
		if tagName != nil {
			if tagValue != nil {
				result.Tags[*tagName] = *tagValue
			} else {
				result.Tags[*tagName] = ""
			}
		}
	}
	if err = rows.Err(); err != nil {
		return service.CredentialModel{}, err
	}
	if emptyResult {
		return service.CredentialModel{}, fmt.Errorf("credential not found")
	}
	result.CreatedAt = result.CreatedAt.In(time.UTC)
	result.UpdatedAt = result.UpdatedAt.In(time.UTC)
	if len(result.Tags) == 0 {
		result.Tags = map[string]string{}
	}
	log.WithFields(log.Fields{
		"username": result.Username,
		"id":       result.ID,
		"name":     result.Name,
	}).Info("fetched credential")
	return result, nil
}

// Update updates certain fields of a credential.
func (ps *PostgresStorage) Update(credUpdate types.CredUpdate) error {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "PostgresStorage.Update",
		"username": credUpdate.Username,
		"credID":   credUpdate.ID,
	})
	if credUpdate.Username == "" || credUpdate.ID == "" || credUpdate.SessionActor == "" {
		err := service.NewCacaoGeneralError("BUG, one of username/id/actor field is empty")
		logger.WithError(err).Error("BUG")
		return err
	}

	if !ps.needToUpdate(credUpdate) {
		logger.Debug("no need to update")
		return nil
	}

	now := ps.getCurrentTime()
	updateTimestamp := storageschema.MilliSecPrecision(now)

	ctx, cancelFunc := ps.newCtx()
	defer cancelFunc()
	tx, err := ps.db.BeginTx(ctx, nil)
	if err != nil {
		logger.WithError(err).Error("BUG")
		return err
	}
	defer tx.Rollback()

	var cred service.CredentialModel

	// we always want to lock credential row even if we only update tags, this is to sync between concurrent req that update tags
	const sqlLockRow = `SELECT id FROM credential WHERE id=$1 AND username=$2 FOR UPDATE;`
	row := tx.QueryRow(sqlLockRow, credUpdate.ID, credUpdate.Username)
	err = row.Scan(&cred.ID)
	if err != nil {
		if err == sql.ErrNoRows {
			return service.NewCacaoNotFoundError(fmt.Sprintf("credential %s not found", credUpdate.ID))
		}
		logger.WithError(err).Error("fail to lock credential row")
		return err
	}

	needToUpdateTags := len(credUpdate.Update.UpdateOrAddTags) > 0 || len(credUpdate.Update.DeleteTags) > 0
	if needToUpdateTags {
		// Note there is one caveat, this does not lock credential_tags rows that are concurrently inserted
		const sqlLockTagRows = `SELECT id FROM credential_tags WHERE id=$1 FOR UPDATE;`
		var rows *sql.Rows
		rows, err = tx.Query(sqlLockTagRows, credUpdate.ID)
		if err != nil {
			logger.WithError(err).Error("fail to lock tag rows")
			return err
		}
		err = rows.Close()
		if err != nil {
			logger.WithError(err).Error("fail to close rows result of locked tags rows")
			return err
		}
	}

	err = ps.updateCredentialInCredentialTable(tx, credUpdate, updateTimestamp)
	if err != nil {
		logger.WithError(err).Error("fail to update fields")
		return err
	}

	if needToUpdateTags {
		// insert or update aka upsert
		const sqlUpsertTag = `INSERT INTO credential_tags (id, tag_name, tag_value) VALUES ($1, $2, $3) ON CONFLICT (id, tag_name) DO UPDATE SET tag_value=$3 WHERE credential_tags.id=$1 AND credential_tags.tag_name=$2;`
		for tagName, tagValue := range credUpdate.Update.UpdateOrAddTags {
			_, err = tx.Exec(sqlUpsertTag, credUpdate.ID, tagName, tagValue)
			if err != nil {
				logger.WithError(err).Error("fail to upsert tag")
				return err
			}
		}
		const sqlDeleteTags = `DELETE FROM credential_tags WHERE id=$1 AND tag_name=$2;`
		for tagName := range credUpdate.Update.DeleteTags {
			_, err = tx.Exec(sqlDeleteTags, credUpdate.ID, tagName)
			if err != nil {
				logger.WithError(err).Error("fail to del tag")
				return err
			}
		}
	}

	err = tx.Commit()
	if err != nil {
		return err
	}

	logger.WithFields(log.Fields{"username": credUpdate.Username}).Info("updated credential")
	return nil
}

func (ps *PostgresStorage) needToUpdate(credUpdate types.CredUpdate) bool {
	if credUpdate.Update.Name != nil {
		return true
	}
	if credUpdate.Update.Value != nil {
		return true
	}
	if credUpdate.Update.Description != nil {
		return true
	}
	if credUpdate.Update.Disabled != nil {
		return true
	}
	if credUpdate.Update.Visibility != nil {
		return true
	}
	if len(credUpdate.Update.UpdateOrAddTags) > 0 || len(credUpdate.Update.DeleteTags) > 0 {
		return true
	}
	return false

}

func (ps *PostgresStorage) updateCredentialInCredentialTable(tx *sql.Tx, credUpdate types.CredUpdate, updateTimestamp time.Time) error {
	fieldUpdated := false

	if credUpdate.Update.Name != nil {
		_, err := tx.Exec("UPDATE credential SET name=$2 WHERE id=$1;", credUpdate.ID, credUpdate.Update.Name)
		if err != nil {
			return err
		}
		fieldUpdated = true
	}
	if credUpdate.Update.Value != nil {
		_, err := tx.Exec("UPDATE credential SET value=$2 WHERE id=$1;", credUpdate.ID, credUpdate.Update.Value)
		if err != nil {
			return err
		}
		fieldUpdated = true
	}
	if credUpdate.Update.Description != nil {
		_, err := tx.Exec("UPDATE credential SET description=$2 WHERE id=$1;", credUpdate.ID, credUpdate.Update.Description)
		if err != nil {
			return err
		}
		fieldUpdated = true
	}
	if credUpdate.Update.Disabled != nil {
		_, err := tx.Exec("UPDATE credential SET disabled=$2 WHERE id=$1;", credUpdate.ID, credUpdate.Update.Disabled)
		if err != nil {
			return err
		}
		fieldUpdated = true
	}
	if credUpdate.Update.Visibility != nil {
		_, err := tx.Exec("UPDATE credential SET visibility=$2 WHERE id=$1;", credUpdate.ID, credUpdate.Update.Visibility)
		if err != nil {
			return err
		}
		fieldUpdated = true
	}

	needToUpdateTags := len(credUpdate.Update.UpdateOrAddTags) > 0 || len(credUpdate.Update.DeleteTags) > 0
	if fieldUpdated || needToUpdateTags {
		_, err := tx.Exec("UPDATE credential SET updated_at=$2, updated_by=$3, updated_emulator_by=$4 WHERE id=$1;", credUpdate.ID, updateTimestamp, credUpdate.SessionActor, credUpdate.SessionEmulator)
		if err != nil {
			log.WithError(err).Error("fail to update timestamp & actor")
			return err
		}
	}
	return nil
}

// List lists all credentials for a user. Value field is left as "REDACTED".
func (ps *PostgresStorage) List(queryData types.ListQueryData) ([]service.CredentialModel, error) {
	const sqlList = `SELECT credential.id, credential.name, credential.username, credential.type, credential.description,
credential.is_system, credential.visibility, credential.created_at, credential.updated_at, credential.updated_by, credential.updated_emulator_by,
ct.tag_name, ct.tag_value
FROM credential LEFT JOIN credential_tags ct on credential.id = ct.id WHERE credential.username = $1`
	rows, err := ps.db.Query(sqlList, queryData.Username)
	if err != nil {
		return nil, err
	}
	// Because this is left join, a credential will have multiple rows if it has multiple tags.
	var list []service.CredentialModel
	var credentialMap = map[string]service.CredentialModel{}
	var current service.CredentialModel
	for rows.Next() {
		current.Tags = map[string]string{}
		var tagName *string
		var tagValue *string
		err = rows.Scan(&current.ID, &current.Name, &current.Username, &current.Type, &current.Description,
			&current.IsSystem, &current.Visibility, &current.CreatedAt, &current.UpdatedAt, &current.UpdatedBy, &current.UpdatedEmulatorBy,
			&tagName, &tagValue)
		if err != nil {
			return nil, err
		}
		if _, ok := credentialMap[current.ID]; !ok {
			//current.CreatedAt = current.CreatedAt.In(time.UTC)
			//current.UpdatedAt = current.UpdatedAt.In(time.UTC)
			current.Value = "REDACTED"
			if len(current.Tags) == 0 {
				current.Tags = map[string]string{}
			}
			credentialMap[current.ID] = current
		}
		if tagName != nil {
			if tagValue != nil {
				credentialMap[current.ID].Tags[*tagName] = *tagValue
			} else {
				credentialMap[current.ID].Tags[*tagName] = ""
			}
		}
	}
	for _, cred := range credentialMap {
		list = append(list, cred)
	}
	log.WithFields(log.Fields{"username": queryData.Username, "actor": queryData.Actor, "emulator": queryData.Emulator, "len": len(list)}).Info("listed credential")
	return list, nil
}

// Close ...
func (ps *PostgresStorage) Close() {
	err := ps.db.Close()
	if err != nil {
		log.WithError(err).Error("fail to close database")
	}
}

// return a context with a timeout
func (ps *PostgresStorage) newCtx() (context.Context, context.CancelFunc) {
	return context.WithTimeout(context.Background(), time.Second*2)
}

//go:embed credential.sql
var postgresCreateTableSQL string

// InitDatabase ...
func (ps *PostgresStorage) InitDatabase() error {
	_, err := ps.db.Exec(postgresCreateTableSQL)
	if err != nil {
		return err
	}
	log.Info("database initialized, tables created")
	return err
}

// check if `credential` table exists
func (ps *PostgresStorage) checkTableExists() (bool, error) {
	const sqlStr = `SELECT EXISTS (
    SELECT FROM 
        information_schema.tables 
    WHERE 
        table_schema LIKE 'public' AND 
        table_type LIKE 'BASE TABLE' AND
        table_name = 'credential'
    );`
	row := ps.db.QueryRow(sqlStr)
	if err := row.Err(); err != nil {
		return false, err
	}
	var exists bool
	err := row.Scan(&exists)
	if err != nil {
		return false, err
	}
	return exists, nil
}
