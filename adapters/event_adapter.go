package adapters

import (
	"context"
	"fmt"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/nats-io/nats.go"
	"github.com/nats-io/stan.go"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
	"sync"

	"gitlab.com/cyverse/creds-microservice/types"
)

// StanEventAdapter is an event adapter that listens for and publish events on NATS Streaming (aka STAN). Events received are sent to domain by channel.
type StanEventAdapter struct {
	config    types.Config
	eventChan chan<- types.EventMessage
	nc        *nats.Conn
	sc        stan.Conn
	sub       stan.Subscription
}

// NewStanEventAdapter ...
func NewStanEventAdapter() *StanEventAdapter {
	return &StanEventAdapter{}
}

// Init is the init function required per the Port interface
func (e *StanEventAdapter) Init(c types.Config) {
	e.config = c
	log.WithFields(log.Fields{
		"function":  "StanEventAdapter.Init",
		"stanURL":   e.config.NatsConfig.URL,
		"clusterID": e.config.StanConfig.ClusterID,
	}).Info()
}

// InitChannel initialize the shared channel with the Domain object
func (e *StanEventAdapter) InitChannel(dc chan types.EventMessage, wg *sync.WaitGroup) {
	e.eventChan = dc
}

// Start function tells the adapter to start listening for events. Events received will be pushed into the event channel (set by InitChannel).
func (e *StanEventAdapter) Start(ctx context.Context) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "StanEventAdapter.Start",
	})
	logger.Debug("start")

	e.stanConnect(logger)
	e.stanSubScribe(logger)

	go e.watchContextCancellation(ctx)
}

func (e *StanEventAdapter) stanConnect(logger *log.Entry) {
	e.nc = natsConnect(logger, e.config.NatsConfig)

	sc, err := stan.Connect(e.config.StanConfig.ClusterID, e.config.NatsConfig.ClientID, stan.NatsConn(e.nc))
	if err != nil {
		logger.WithFields(log.Fields{
			"clusterID": e.config.StanConfig.ClusterID,
		}).WithError(err).Fatal("cannot connect to nats streaming")
		// terminate process if error
	}
	e.sc = sc
	logger.Debug("connected to nats streaming")
}

func (e *StanEventAdapter) stanSubScribe(logger *log.Entry) {
	logger.WithField("subject", common.EventsSubject).Debug("subscribing on subject")
	sub, err := e.sc.QueueSubscribe(common.EventsSubject, e.config.NatsConfig.QueueGroup, e.handleEvent, stan.DurableName(e.config.StanConfig.DurableName))
	if err != nil {
		logger.WithError(err).Fatal("cannot create a queued subscription")
		// terminate process if error
	}
	e.sub = sub
}

// watch for cancellation of context, and close subscription(close subscription not unsubscribe) with context is cancelled.
// this is a blocking call, call as go routine.
func (e *StanEventAdapter) watchContextCancellation(ctx context.Context) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "StanEventAdapter.watchContextCancellation",
	})
	<-ctx.Done()
	if e.sub == nil {
		logger.Warn("STAN subscription is nil")
		return
	}
	err := e.sub.Close()
	if err != nil {
		logger.WithError(err).Info("fail to close STAN subscription")
		return
	}
	logger.Info("STAN subscription is closed due to context")
}

func (e *StanEventAdapter) handleEvent(msg *stan.Msg) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "StanEventAdapter.handleEvent",
	})
	logger.WithField("subject", msg.Subject).Debug("received message")

	ce, err := e.stanMsgToCloudEvent(msg)
	if err != nil {
		logger.WithError(err).Error()
		return
	}
	message, err := e.ceToEvent(ce)
	if err != nil {
		logger.WithError(err).Error()
		return
	}

	logger = logger.WithFields(log.Fields{
		"ceID": ce.ID(),
		"tid":  message.TID,
	})
	logger.Debug("pushing to event channel")
	e.eventChan <- message
	logger.Debug("pushed to event channel")
}

func (e *StanEventAdapter) stanMsgToCloudEvent(msg *stan.Msg) (cloudevents.Event, error) {
	ce, err := messaging.ConvertStan(msg)
	if err != nil {
		return cloudevents.Event{}, fmt.Errorf("fail to convert STAN msg, %w", err)
	}

	if ce.Data() == nil {
		return cloudevents.Event{}, fmt.Errorf("cloudevent data is nil")
	}
	return ce, nil
}

func (e *StanEventAdapter) ceToEvent(ce cloudevents.Event) (types.EventMessage, error) {
	tid := messaging.GetTransactionID(&ce)
	var message = types.EventMessage{
		Type: ce.Type(),
		TID:  tid,
		//Obj:  *cePayload,
	}
	switch common.EventType(ce.Type()) {
	case service.EventCredentialAddRequested:
		var cePayload service.CredentialCreateRequest
		err := ce.DataAs(&cePayload)
		if err != nil {
			return types.EventMessage{}, err
		}
		message.Obj = cePayload
	case service.EventCredentialDeleteRequested:
		var cePayload service.CredentialDeleteRequest
		err := ce.DataAs(&cePayload)
		if err != nil {
			return types.EventMessage{}, err
		}
		message.Obj = cePayload
	case service.EventCredentialUpdateRequested:
		var cePayload service.CredentialUpdateRequest
		err := ce.DataAs(&cePayload)
		if err != nil {
			return types.EventMessage{}, err
		}
		message.Obj = cePayload
	default:
		return types.EventMessage{}, fmt.Errorf("unknown event type, %s", ce.Type())
	}
	return message, nil
}

// PublishEvent publish an event to STAN. eventPayload is put into cloudevents.Event.Data.
func (e *StanEventAdapter) PublishEvent(ev common.EventType, eventPayload interface{}, transactionID common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "StanEventAdapter.PublishEvent",
	})
	logger.WithField("eventType", ev).Debug()

	if e.sc == nil {
		logger.Warn("no STAN connection available, connection should be established before calling PublishEvent()")
		return fmt.Errorf("no STAN connection available, likely a BUG")
	}

	ce, err := messaging.CreateCloudEventWithTransactionID(eventPayload, string(ev), e.config.NatsConfig.ClientID, transactionID)
	if err != nil {
		return err
	}
	payload, err := ce.MarshalJSON()
	if err != nil {
		return err
	}

	logger.WithField("eventType", ev).Debug("sync publishing the event")
	err = e.sc.Publish(common.EventsSubject, payload)
	if err != nil {
		return err
	}

	return nil
}

// Close closes subscription and connections
func (e *StanEventAdapter) Close() {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "StanEventAdapter.Close",
	})
	// close subscription will not remove durable name
	err := e.sub.Close()
	if err != nil {
		logger.WithError(err).Error("fail to close subscription")
	}
	err = e.sc.Close()
	if err != nil {
		logger.WithError(err).Error("fail to close STAN conn")
	}
	e.nc.Close()
}
