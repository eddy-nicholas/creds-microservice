package adapters

import (
	"os"
	"testing"
)

// CI_INTEGRATION_VAULT needs to be set in order to run integration tests that requires a vault instance
func skipIfVaultEnvNotSet(t *testing.T) (skipped bool) {
	if os.Getenv("CI_INTEGRATION_VAULT") != "true" {
		t.Skip("CI_INTEGRATION_VAULT is not 'true', skipping test")
		return true
	}
	return false
}

// CI_INTEGRATION_STAN needs to be set in order to run integration tests that requires a NATS or STAN instance
func skipIfSTANEnvNotSet(t *testing.T) (skipped bool) {
	if os.Getenv("CI_INTEGRATION_STAN") != "true" {
		t.Skip("CI_INTEGRATION_STAN is not 'true', skipping test")
		return true
	}
	return false
}

// CI_INTEGRATION_PSQL needs to be set in order to run integration tests that requires a vault instance
func skipIfPostgresEnvNotSet(t *testing.T) (skipped bool) {
	if os.Getenv("CI_INTEGRATION_PSQL") != "true" {
		t.Skip("CI_INTEGRATION_PSQL is not 'true', skipping test")
		return true
	}
	return false
}
