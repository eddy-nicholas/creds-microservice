package adapters

import (
	"encoding/json"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	guuid "github.com/google/uuid"
)

// CreateResponseCE creates a response cloudevents
func CreateResponseCE(source string, eventType string, obj interface{}) []byte {
	event := cloudevents.NewEvent()
	id := guuid.New()
	event.SetID(id.String())
	event.SetSource(source)
	event.SetType(eventType)
	_ = event.SetData(cloudevents.ApplicationJSON, obj)
	bytes, _ := json.Marshal(event)

	return bytes
}
