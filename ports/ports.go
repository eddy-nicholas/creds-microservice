package ports

import (
	"context"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"sync"
	"time"

	"gitlab.com/cyverse/creds-microservice/types"
)

// Port is the base interface for Ports, in which all should support an Init() function (but it may be noop)
type Port interface {
	Init(c types.Config)
}

// AsyncPort is a special type of port that will have an asynchronous behavior to it, such as being event-driven
// outside of the primary domain thread (e.g. incoming message queues). This ports should communicate with the
// domain object using a threaded approach (i.e. go routine, go channels and possibly waitgroups)
type AsyncPort interface {
	Port
	Start(context.Context)
}

// IncomingQueryPort is an example interface for a query port.
// Internal to the adapter there may be additional methods, but at the very least, it is an async port
// Also, incoming and outgoing ports are declared separately for illustrative purposes
// method set signatures will allow one adapter fulfill the purposes of both income and outgoing query ports
type IncomingQueryPort interface {
	AsyncPort
	InitChannel(dc chan types.QueryMessage, wg *sync.WaitGroup)
}

// IncomingEventPort is an example interface for an event port.
type IncomingEventPort interface {
	AsyncPort
	InitChannel(dc chan types.EventMessage, wg *sync.WaitGroup)
}

// OutgoingEventsPort is the port for events
type OutgoingEventsPort interface {
	Port
	PublishEvent(ev common.EventType, dcredential interface{}, transactionID common.TransactionID) error
}

// PersistentStoragePort is a port to store messages.
// The persistence storage should ensure the uniqueness of the combination of username and ID.
type PersistentStoragePort interface {
	Port
	Create(d service.CredentialModel) error
	Delete(d service.CredentialModel) error
	Get(d service.CredentialModel) (service.CredentialModel, error)
	Update(credUpdate types.CredUpdate) error
	List(d types.ListQueryData) ([]service.CredentialModel, error)
}

// TimeSrc returns the current time
type TimeSrc func() time.Time

// CredIDGenerator generates a new unique credential ID
type CredIDGenerator func() common.ID

// GenerateCredentialID is default implementation of CredIDGenerator
func GenerateCredentialID() common.ID {
	return common.NewID("cred")
}
