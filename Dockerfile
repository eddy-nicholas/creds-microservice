# syntax=docker/dockerfile:1

# These ARGs statements are to declare default values
ARG CACAO_BUILD_IMAGE="golang:1.20"

FROM ${CACAO_BUILD_IMAGE} as base
ARG CACAO_BUILD_IMAGE
ARG SKAFFOLD_GO_GCFLAGS
COPY ./ /creds-microservice/
WORKDIR /creds-microservice/
RUN --mount=type=cache,target=/root/.cache/go-build eval go build -gcflags="${SKAFFOLD_GO_GCFLAGS}"

FROM gcr.io/distroless/base-debian10
ARG GOTRACEBACK
ENV GOTRACEBACK="$GOTRACEBACK"
COPY --from=base /creds-microservice/ /
CMD ["/creds-microservice"]