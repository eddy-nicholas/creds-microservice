package types

import (
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/service"
	"testing"
	"time"
)

// For the list operation, the request is in the form of service.CredentialModel, but is unmarshal as ListQueryData in query adapter.
// this test is to enforce this marshal/unmarshal relationship.
func TestMarshalFetchCredsData(t *testing.T) {
	var fetch ListQueryData
	var credModel = service.CredentialModel{
		Session: service.Session{
			SessionActor:    "test-actor-123",
			SessionEmulator: "test-emulator-123",
			ErrorType:       "",
			ErrorMessage:    "",
			ServiceError:    service.CacaoErrorBase{},
		},
		Username:          "testuser-123",
		Value:             "val123",
		Type:              "type123",
		ID:                "id123",
		Description:       "desc123",
		IsSystem:          false,
		IsHidden:          false,
		Visibility:        "Default",
		Tags:              nil,
		CreatedAt:         time.Unix(1655110000, 0),
		UpdatedAt:         time.Unix(1655220000, 0),
		UpdatedBy:         "",
		UpdatedEmulatorBy: "",
	}
	marshal, err := json.Marshal(credModel)
	assert.NoError(t, err)

	err = json.Unmarshal(marshal, &fetch)
	assert.NoError(t, err)
	// only type & username are unmarshalled
	assert.Equal(t, ListQueryData{
		Actor:    "test-actor-123",
		Emulator: "test-emulator-123",
		Username: "testuser-123",
	}, fetch)
}
