package types

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/messaging"
)

// Config is the configuration settings, which can be used by the Domain object or Adapters
type Config struct {
	VaultAddress string `envconfig:"VAULT_ADDRESS" default:"http://127.0.0.1:8200"`
	VaultToken   string `envconfig:"VAULT_TOKEN" default:"s.Nq9oyXBwpermKn5ZpMqS23r4"`

	PostgresHost                  string `envconfig:"POSTGRES_HOST"`
	PostgresUsername              string `envconfig:"POSTGRES_USER"`
	PostgresPassword              string `envconfig:"POSTGRES_PASSWORD"`
	PostgresDatabase              string `envconfig:"POSTGRES_DB"`
	PostgresSSL                   bool   `envconfig:"POSTGRES_SSL" default:"false"`
	PostgresCreateTablesIfMissing bool   `envconfig:"POSTGRES_CREATE_TABLES_IF_MISSING" default:"true"` // create tables on startup if missing

	// set this the nats subject to listen, by default it's everything
	NatsConfig messaging.NatsConfig
	StanConfig messaging.StanConfig

	// set the log level
	LogLevel string `envconfig:"LOG_LEVEL" default:"debug"`
}

// ProcessDefaults will take a Config object and process the config object further, including
// populating any null values
// TODO: TEMPLATE: should figure out a consistent way of generating a unique but repeatable nats client id for the case when
// a microservice is part of a ReplicaSet. It might be the case that the same client doesn't need the same client id
// as long as the clients are part of the same qgroup and durable name
func (c *Config) ProcessDefaults() {
	log.Debug("ProcessDefaults() started")
	if c.NatsConfig.ClientID == "" {
		c.NatsConfig.ClientID = DefaultNatsClientID
	}
	if c.NatsConfig.QueueGroup == "" {
		c.NatsConfig.QueueGroup = DefaultNatsQGroup
	}
	if c.StanConfig.DurableName == "" {
		c.StanConfig.DurableName = DefaultNatsDurableName
	}
	if c.NatsConfig.WildcardSubject == "" || c.NatsConfig.WildcardSubject == "cyverse.>" {
		c.NatsConfig.WildcardSubject = DefaultNatsCoreSubject
	}

	l, err := log.ParseLevel(c.LogLevel)
	if err != nil {
		log.Warn("Could not parse config loglevel, setting to 'info', c.LogLevel was '" + c.LogLevel + "'")
		log.SetLevel(log.InfoLevel)
	} else {
		log.SetLevel(l)
	}
}
