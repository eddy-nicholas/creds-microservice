package types

// TODO: DEVELOPER should changes these e.g. instead of "mythical*", change to "user*" for user microservice, "credentials*", "workspaces*", etc
const (
	DefaultNatsClientID    = "creds"
	DefaultNatsQGroup      = "creds"
	DefaultNatsDurableName = "creds"
	DefaultNatsCoreSubject = "cyverse.credentials.>" // this listens to all credential related subjects
)

// Given that the credential ID and username will go into the path of the credential in the external storage, it is a good practice to set a max limit.
const (
	MaxCredIDLength       = 100
	MaxCredUsernameLength = 256
)

// QueryReplyTypePostfix is the postfix that will be appended to the type field of cloudevent of the reply.
const QueryReplyTypePostfix = ".response"
