package types

import (
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"time"
)

// This file contains types which are used across the entire microservice, which may not be more appropriate elsewhere
// CredsData is the primary data type when receiving and sending data externally, and possibly destined to a persistent store (if applicable)
// As a implementor you should change this struct to meet the needs (e.g. a user ms would have username, first name, last name, email, etc)

// QueryMessage is a query msg, for read-only operations (e.g. read/list)
type QueryMessage struct {
	Type     string `json:"type"`
	Obj      interface{}
	Response chan QueryMessage `json:"-"`
}

// EventMessage is a event msg
type EventMessage struct {
	Type string               `json:"type"`
	TID  common.TransactionID `json:"TID"`
	Obj  interface{}
}

// ListQueryData is an intermediate struct that is used for unmarshalling list operation request.
// List request is marshal as service.CredentialModel in service client (cacao-common/service), but is unmarshal as this struct.
type ListQueryData struct {
	Actor    string `json:"actor"`
	Emulator string `json:"emulator"`
	Username string `json:"username"`
}

// QueryOp is a type to represent query operations
type QueryOp string

// EventType is a type to represent event operations
type EventType string

// Filter is the filter that this service supports for the list operation.
// TODO merge the fields into ListQueryData
type Filter struct {
	Owner         string
	HasTagName    []string
	TagValueMatch []struct {
		Name  string
		Value string
	}
	CreatedOrUpdatedAfter *time.Time
	Deleted               *bool
	NameMatch             []string
	NameSimilar           *string
	DescriptionContains   *string
}

// CredUpdate contains the necessary info to perform a update in storage adapter
type CredUpdate struct {
	SessionActor    string
	SessionEmulator string
	Username        string // owner
	ID              string
	Update          service.CredentialUpdate
}

// ApplyCredentialUpdate ...
func ApplyCredentialUpdate(cred service.CredentialModel, update service.CredentialUpdate) service.CredentialModel {
	if update.Name != nil {
		cred.Name = *update.Name
	}
	if update.Value != nil {
		cred.Value = *update.Value
	}
	if update.Description != nil {
		cred.Description = *update.Description
	}
	if update.Disabled != nil {
		cred.Disabled = *update.Disabled
	}
	if update.Visibility != nil {
		cred.Visibility = *update.Visibility
	}
	if update.UpdateOrAddTags != nil {
		if len(cred.Tags) == 0 {
			cred.Tags = update.UpdateOrAddTags
		} else {
			for name, value := range update.UpdateOrAddTags {
				cred.Tags[name] = value
			}
		}
	}
	if len(update.DeleteTags) > 0 {
		for name := range update.DeleteTags {
			delete(cred.Tags, name)
		}
	}
	return cred
}
