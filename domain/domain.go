package domain

import (
	"context"
	"sync"

	log "github.com/sirupsen/logrus"

	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/creds-microservice/ports"
	"gitlab.com/cyverse/creds-microservice/types"
)

// Domain is the base struct for the domain service
type Domain struct {
	QueryIn         ports.IncomingQueryPort
	EventsIn        ports.IncomingEventPort
	EventsOut       ports.OutgoingEventsPort
	VaultOut        ports.PersistentStoragePort
	CredIDGenerator ports.CredIDGenerator
}

// Init initializes all the specified adapters
func (d Domain) Init(c types.Config) {
	log.Debug("domain.Init() starting")
}

const (
	defaultChannelBufferSize = 100 // TODO consider change this to lower value
)

// Start will start the domain object, and in turn start all the async adapters
func (d Domain) Start(ctx context.Context) {
	log.Debug("domain.Start() starting")

	// using waitgroups to block termination gracefully
	var wg sync.WaitGroup

	// It is here that we need to handle asynchronous adapters
	// create channel for incoming queries, qchan
	dc := make(chan types.QueryMessage, defaultChannelBufferSize)
	d.QueryIn.InitChannel(dc, &wg)
	wg.Add(1)
	go d.QueryIn.Start(ctx)

	em := make(chan types.EventMessage, defaultChannelBufferSize)
	if d.EventsIn != nil {
		d.EventsIn.InitChannel(em, &wg)
		wg.Add(1)
		go d.EventsIn.Start(ctx)
	}

	// start the domain's query & event worker
	wg.Add(1)
	go d.processQueryWorker(ctx, dc, &wg)
	wg.Add(1)
	go d.processEventWorker(ctx, em, &wg)

	wg.Wait()
}

func (d Domain) createCredential(info service.CredentialModel) error {
	return d.VaultOut.Create(info)
}
func (d Domain) deleteCredential(info service.CredentialModel) error {
	return d.VaultOut.Delete(info)
}

func (d Domain) processEventWorker(ctx context.Context, em chan types.EventMessage, wg *sync.WaitGroup) {
	logger := log.WithFields(log.Fields{
		"package":  "domain",
		"function": "Domain.processEventWorker",
	})
	logger.Debug("starting")
	defer wg.Done()

	var creationHandler credentialCreationHandler

	for {
		select {
		case event := <-em:
			logger.WithField("type", event.Type).Debug("received new event")
			switch event.Type {
			case string(service.EventCredentialAddRequested):
				creationHandler.handleCreateEvent(event, d.VaultOut, d.EventsOut, d.CredIDGenerator)
				break
			case string(service.EventCredentialDeleteRequested):
				handleDeleteEvent(event, d.VaultOut, d.EventsOut)
				break
			case string(service.EventCredentialUpdateRequested):
				handleUpdateEvent(event, d.VaultOut, d.EventsOut)
				break
			}

		case <-ctx.Done():
			logger.Debug("context cancellation")
			return
		}
	}
}

func (d Domain) processQueryWorker(ctx context.Context, queryChan chan types.QueryMessage, wg *sync.WaitGroup) {
	logger := log.WithFields(log.Fields{
		"package":  "domain",
		"function": "Domain.processQueryWorker",
	})
	logger.Debug("starting")
	defer wg.Done()

	for {
		select {
		case query := <-queryChan:
			logger.WithField("type", query.Type).Debug("received new query")

			switch query.Type {
			case service.NatsSubjectCredentialsGet:
				handleGetQuery(query, d.VaultOut)
				break
			case service.NatsSubjectCredentialsList:
				handleListQuery(query, d.VaultOut)
				break
			}
		case <-ctx.Done():
			logger.Debug("context cancellation")
			return
		}
	}
}
