package domain

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/creds-microservice/domain/validation"
	"gitlab.com/cyverse/creds-microservice/ports"
	"gitlab.com/cyverse/creds-microservice/types"
	"reflect"
	"sync"
)

type credentialCreationHandler struct {
	nameUniquenessCheckLock sync.Mutex
}

func (h *credentialCreationHandler) handleCreateEvent(event types.EventMessage, storage ports.PersistentStoragePort, eventOut ports.OutgoingEventsPort, credIDGenerator func() common.ID) {
	logger := log.WithFields(log.Fields{
		"package":  "domain",
		"function": "handleCreateEvent",
	})
	request, ok := event.Obj.(service.CredentialCreateRequest)
	if !ok {
		panic("the data in EventMessage should be of type service.CredentialModel, " + reflect.TypeOf(event.Obj).Name())
	}
	request.ID = credIDGenerator().String()

	logger = logger.WithFields(log.Fields{
		"actor":    request.SessionActor,
		"emulator": request.SessionEmulator,
		"username": request.Username,
		"id":       request.ID,
		"type":     request.Type,
	})
	err := validateCreationRequest(service.CredentialModel(request))
	if err != nil {
		logger.WithError(err).Error("creation request event is invalid")
		publishCreateErrorEvent(logger, eventOut, request, err, event.TID)
		return
	}

	logger.Debug("domain is creating a secret")

	credNameUnique, err := h.checkIfNameIsUnique(storage, request.Username, request.ID, request.Name)
	if err != nil {
		logger.WithError(err).Error("fail to check if credential name is unique")
		publishCreateErrorEvent(logger, eventOut, request, err, event.TID)
		return
	}
	if !credNameUnique {
		logger.WithError(err).Error("creation name is not unique")
		publishCreateErrorEvent(logger, eventOut, request, service.NewCacaoInvalidParameterError("credential name is not unique"), event.TID)
		return
	}
	err = storage.Create(service.CredentialModel(request))
	if err != nil {
		logger.WithError(err).Error("fail to create credential in storage")
		publishCreateErrorEvent(logger, eventOut, request, err, event.TID)
		return
	}
	publishCreateSuccessEvent(logger, eventOut, request, event.TID)
}

func validateCreationRequest(request service.CredentialModel) error {
	if request.GetSessionActor() != request.Username {
		return service.NewCacaoUnauthorizedError("only owner is allowed to create")
	}
	err := validation.ValidateCredential(request)
	if err != nil {
		return err
	}
	return nil
}

// check if credential name is unique within a user's scope. aka, the combination of username and credential name is unique.
// FIXME this uniqueness check on name is not 100% safe for concurrency, even though mutex is used, other instances of credential service will have a race condition on this.
// The reason for race condition is that there is a gap between the check and creation, thus multiple creation could interleave, result in duplicate credential name.
func (h *credentialCreationHandler) checkIfNameIsUnique(storage ports.PersistentStoragePort, username, credID, credName string) (isUnique bool, err error) {
	h.nameUniquenessCheckLock.Lock()
	defer h.nameUniquenessCheckLock.Unlock()

	credList, err := storage.List(types.ListQueryData{
		Username: username,
	})
	if err != nil {
		return false, err
	}
	for _, cred := range credList {
		if cred.ID == credID {
			// skip check against self.
			// if cred.ID == credID, then it is a "rename"
			continue
		}
		if cred.Name == credName {
			// name already exists, not unique
			return false, err
		}
	}
	return true, err
}

func publishCreateSuccessEvent(logger *log.Entry, eventOut ports.OutgoingEventsPort, request service.CredentialCreateRequest, tid common.TransactionID) {
	err := eventOut.PublishEvent(service.EventCredentialAdded, service.CredentialCreateResponse{
		Session: service.Session{
			SessionActor:    request.GetSessionActor(),
			SessionEmulator: request.GetSessionEmulator(),
		},
		Username: request.Username,
		ID:       request.ID,
	}, tid)
	if err != nil {
		logger.WithError(err).Error("fail to publish event")
	}
}

func publishCreateErrorEvent(logger *log.Entry, eventOut ports.OutgoingEventsPort, request service.CredentialCreateRequest, err error, tid common.TransactionID) {
	svcErr, ok := err.(service.CacaoError)
	if !ok {
		svcErr = service.NewCacaoGeneralError(err.Error())
	}
	eventOutErr := eventOut.PublishEvent(service.EventCredentialAddError, service.CredentialCreateResponse{
		Session: service.Session{
			SessionActor:    request.GetSessionActor(),
			SessionEmulator: request.GetSessionEmulator(),
			ErrorType:       err.Error(), // populate the old error type, this won't be necessary after svc client is updated to use svc error
			ErrorMessage:    "",
			ServiceError:    svcErr.GetBase(), // populate the service error
		},
		Username: request.Username,
		ID:       request.ID,
	}, tid)
	if eventOutErr != nil {
		logger.WithError(eventOutErr).Error("fail to publish event")
	}
}
