package validation

import (
	"fmt"
	"gitlab.com/cyverse/cacao-common/service"
	"gopkg.in/yaml.v3"
	"net/url"
)

// KubeConfig describes schema for storing kubeconfig credential.
type KubeConfig struct {
	APIVersion     string                 `yaml:"apiVersion"`
	Clusters       []KubeConfigCluster    `yaml:"clusters"`
	Contexts       []KubeConfigContext    `yaml:"contexts"`
	CurrentContext string                 `yaml:"current-context"`
	Kind           string                 `yaml:"kind"`
	Preferences    map[string]interface{} `yaml:"preferences"`
	Users          []KubeConfigUser       `yaml:"users"`
}

type KubeConfigCluster struct {
	Cluster struct {
		CertificateAuthorityData string `yaml:"certificate-authority-data"`
		Server                   string `yaml:"server"`
	} `yaml:"cluster"`
	Name string `yaml:"name"`
}

type KubeConfigContext struct {
	Context struct {
		Cluster string `yaml:"cluster"`
		User    string `yaml:"user"`
	} `yaml:"context"`
	Name string `yaml:"name"`
}

type KubeConfigUser struct {
	Name string `yaml:"name"`
	User struct {
		ClientCertificateData string `yaml:"client-certificate-data"`
		ClientKeyData         string `yaml:"client-key-data"`
		Token                 string `yaml:"token"`
	} `yaml:"user"`
}

func validateKubeConfig(cred service.CredentialModel) error {
	if len(cred.Value) == 0 {
		return fmt.Errorf("kubeconfig credential cannot be empty")
	}
	var kubeconfig KubeConfig
	var err error
	err = yaml.Unmarshal([]byte(cred.Value), &kubeconfig)
	if err != nil {
		return err
	}
	if kubeconfig.Kind != "Config" {
		return fmt.Errorf("kind is not 'Config'")
	}
	switch kubeconfig.APIVersion {
	case "v1":
	default:
		return fmt.Errorf("unknown api version in kubeconfig")
	}
	for _, cluster := range kubeconfig.Clusters {
		if err = validateKubeConfigCluster(cluster); err != nil {
			return err
		}
	}
	for _, user := range kubeconfig.Users {
		if err = validateKubeConfigUser(user); err != nil {
			return err
		}
	}
	var currentCtxFound bool
	for _, clusterCtx := range kubeconfig.Contexts {
		if kubeconfig.CurrentContext == clusterCtx.Name {
			currentCtxFound = true
			break
		}
	}
	if !currentCtxFound {
		return fmt.Errorf("current context is missing from list of contexts")
	}

	for _, context := range kubeconfig.Contexts {
		var clusterFound bool
		for _, cluster := range kubeconfig.Clusters {
			if cluster.Name == context.Context.Cluster {
				clusterFound = true
				break
			}
		}
		if !clusterFound {
			return fmt.Errorf("cluster in context is missing from list of clusters")
		}

		var userFound bool
		for _, user := range kubeconfig.Users {
			if user.Name == context.Context.User {
				userFound = true
				break
			}
		}
		if !userFound {
			return fmt.Errorf("user in context is missing from list of users")
		}
	}
	return nil
}

func validateKubeConfigCluster(cluster KubeConfigCluster) error {
	if cluster.Name == "" {
		return fmt.Errorf("cluster name cannot be empty")
	}
	if cluster.Cluster.Server == "" {
		return fmt.Errorf("cluster server url cannot be empty")
	}

	_, err := url.Parse(cluster.Cluster.Server)
	if err != nil {
		return fmt.Errorf("cluster server url is invalid, %w", err)
	}
	if cluster.Cluster.CertificateAuthorityData == "" {
		return fmt.Errorf("cluster certificate authority data cannot be empty")
	}
	return nil
}

func validateKubeConfigUser(user KubeConfigUser) error {
	if user.Name == "" {
		return fmt.Errorf("user name cannot be empty")
	}
	if user.User.Token != "" {
		return nil
	}
	if user.User.ClientKeyData == "" || user.User.ClientCertificateData == "" {
		return fmt.Errorf("user key or cert is empty")
	}
	return nil
}

func (kubeconfig KubeConfig) validateContexts() error {
	var currentCtxFound bool
	for _, clusterCtx := range kubeconfig.Contexts {
		if kubeconfig.CurrentContext == clusterCtx.Name {
			currentCtxFound = true
		}
		var userFound bool
		for _, user := range kubeconfig.Users {
			if clusterCtx.Context.User == user.Name {
				userFound = true
				break
			}
		}
		if !userFound {
			return fmt.Errorf("user not found in context")
		}
		var clusterFound bool
		for _, cluster := range kubeconfig.Clusters {
			if clusterCtx.Context.Cluster == cluster.Name {
				clusterFound = true
				break
			}
		}
		if !clusterFound {
			return fmt.Errorf("cluster not found in context")
		}
	}
	if !currentCtxFound {
		return fmt.Errorf("current context is missing from list of contexts")
	}
	return nil
}
