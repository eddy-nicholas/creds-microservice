package validation

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/service"
	"testing"
)

func Test_validateAWSCredential(t *testing.T) {
	type args struct {
		cred service.CredentialModel
	}
	tests := []struct {
		name    string
		args    args
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "valid",
			args: args{
				cred: service.CredentialModel{
					Value: `
{
  "AWS_ACCESS_KEY_ID": "aaaaaaaaaaaaaaa",
  "AWS_SECRET_ACCESS_KEY": "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb"
}`,
				},
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.NoError(t, err)
			},
		},
		{
			name: "empty string",
			args: args{
				cred: service.CredentialModel{
					Value: "",
				},
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.Error(t, err)
			},
		},
		{
			name: "bad JSON",
			args: args{
				cred: service.CredentialModel{
					Value: "{",
				},
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.Error(t, err)
			},
		},
		{
			name: "missing secret key",
			args: args{
				cred: service.CredentialModel{
					Value: `
{
  "AWS_ACCESS_KEY_ID": "foo"
}`,
				},
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.Error(t, err)
			},
		},
		{
			name: "missing access key",
			args: args{
				cred: service.CredentialModel{
					Value: `
{
  "AWS_SECRET_ACCESS_KEY": "bar"
}`,
				},
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.Error(t, err)
			},
		},
		{
			name: "empty",
			args: args{
				cred: service.CredentialModel{
					Value: `
{
  "AWS_ACCESS_KEY_ID": "",
  "AWS_SECRET_ACCESS_KEY": ""
}`,
				},
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.Error(t, err)
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.wantErr(t, validateAWSCredential(tt.args.cred), fmt.Sprintf("validateAWSCredential(%v)", tt.args.cred))
		})
	}
}
