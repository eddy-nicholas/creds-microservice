package validation

import (
	"fmt"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/creds-microservice/types"
	"unicode"
)

// CredentialValidator ...
type CredentialValidator func(service.CredentialModel) error

// ValidateCredential validates a credential object
func ValidateCredential(cred service.CredentialModel) error {
	err := validateCredential(cred)
	if err != nil {
		// wrap the error in a cacao service error
		return service.NewCacaoInvalidParameterError(err.Error())
	}
	return nil
}

func validateCredential(cred service.CredentialModel) error {
	var err error
	if err = validateGenericCredential(cred); err != nil {
		return err
	}
	switch cred.Type {
	case "openstack":
		err = validateOpenStackCredential(cred)
	case "aws":
		err = validateAWSCredential(cred)
	case "ssh":
		err = validateSSH(cred)
	case "ssh_private":
		err = validateSSHPrivateKey(cred)
	case "git":
		err = validateGitCredential(cred)
	case "kubeconfig":
		err = validateKubeConfig(cred)
	}
	return err
}

const (
	maxCredNameLength        = 100
	maxCredTypeLength        = 64
	maxCredValueLength       = 16 * 1024 // 16k
	maxCredDescriptionLength = 1000
	maxTagCount              = 50 // max number of tag per credential
	maxTagKeyLength          = 63
	maxTagValueLength        = 63
)

func validateGenericCredential(cred service.CredentialModel) error {
	var err error
	if err = validateCredentialName(cred.Name); err != nil {
		return err
	}
	if err = validateCredentialUsername(cred.Username); err != nil {
		return err
	}
	if err = validateCredentialType(cred.Type); err != nil {
		return err
	}
	if err = checkCredentialValueLength(cred.Value); err != nil {
		return err
	}
	if err = stringIsASCIIAndNoControl(cred.Value); err != nil {
		return err
	}
	if err = checkCredentialDescriptionLength(cred.Description); err != nil {
		return err
	}
	if err = validateCredentialVisibility(cred.Visibility); err != nil {
		return err
	}
	if err = validateCredentialTags(cred.Tags); err != nil {
		return err
	}
	return nil
}

// any printable ASCII (letters/digit/punct)
func validateCredentialName(credName string) error {
	if credName == "" {
		return fmt.Errorf("credential name cannot be empty")
	}
	if len(credName) > maxCredNameLength {
		return fmt.Errorf("credential name too long")
	}
	for i, r := range credName {
		if r > unicode.MaxASCII {
			// only allow ASCII
			return fmt.Errorf("invalid character in credential name at index %d", i)
		}
		if r < ' ' || r > '~' {
			// check if it is outside of range of normal ASCII characters (not digit, not letter, not punct, not space)
			return fmt.Errorf("invalid character in credential name at index %d", i)
		}
	}
	return nil
}

// alphanumeric + '-' + '_', start with letter
func validateCredentialType(credType service.CredentialType) error {
	if credType == "" {
		return fmt.Errorf("credential type cannot be empty")
	}
	if len(credType) > maxCredTypeLength {
		return fmt.Errorf("credential type too long")
	}
	for i, r := range credType {
		if r > unicode.MaxASCII {
			// only allow ASCII
			return fmt.Errorf("invalid character in credential type at index %d", i)
		}
		if i == 0 {
			if unicode.IsLetter(r) {
				continue
			}
		} else {
			if unicode.IsLetter(r) || unicode.IsDigit(r) || r == '-' || r == '_' {
				continue
			}
		}
		return fmt.Errorf("invalid character in credential type at index %d", i)
	}
	return nil
}

func validateCredentialUsername(username string) error {
	if username == "" {
		return fmt.Errorf("username(owner) of the credential cannot be empty")
	}
	if len(username) > types.MaxCredUsernameLength {
		return fmt.Errorf("username(owner) of the credential too long")
	}
	return nil
}

func checkCredentialValueLength(value string) error {
	if len(value) > maxCredValueLength {
		return fmt.Errorf("credential value too long")
	}
	return nil
}

func checkCredentialDescriptionLength(desc string) error {
	if len(desc) > maxCredDescriptionLength {
		return fmt.Errorf("credential description too long")
	}
	return nil
}

func validateCredentialVisibility(visibility service.VisibilityType) error {
	switch visibility {
	case "":
	case "Default":
	default:
		return fmt.Errorf("unknown visibility option")
	}
	return nil
}

func validateCredentialTags(credTags map[string]string) error {
	var err error
	if len(credTags) > maxTagCount {
		return fmt.Errorf("too many tags")
	}
	for tagKey, tagVal := range credTags {
		if err = validateTagKey(tagKey); err != nil {
			return err
		}
		err = validateTagValue(tagVal)
		if err != nil {
			return err
		}
	}
	return nil
}

// Note: Restrictions on tag values is determined by the common denominator of AWS/GCP/Azure tags and GCP labels.
// This is so that if we decide to propagate the tags to resources in those cloud, we can do so.
func validateTagKey(tagKey string) error {
	if tagKey == "" {
		return fmt.Errorf("tag key cannot be empty")
	}
	if len(tagKey) > maxTagKeyLength {
		return fmt.Errorf("tag key too long")
	}
	for i, r := range tagKey {
		if i == 0 {
			// start with letter.
			// Note: GCP tags/labels only allow lower case for key, but to accommodate existing data (e.g. openstack project name), upper case is allowed as well.
			if !unicode.IsLetter(r) {
				return fmt.Errorf("invalid character in credential tag '%s' at index %d", tagKey, i)
			}
		}
		if !isAllowedTagKeyCharacters(r) {
			return fmt.Errorf("invalid character in credential tag '%s' at index %d", tagKey, i)
		}
	}
	return nil
}

func isAllowedTagKeyCharacters(r rune) bool {
	if r > unicode.MaxASCII {
		return false
	}
	// Note: GCP tags/labels only allow lower case for key, but to accommodate existing data (e.g. openstack project name), upper case is allowed as well.
	// ofc, another approach is to toLower when apply to GCP
	if unicode.IsLetter(r) {
		return true
	}
	if unicode.IsDigit(r) {
		return true
	}
	switch r {
	case '-':
		return true
	case '_':
		return true
	}
	return false
}

// Note: Restrictions on tag values is determined by the common denominator of AWS/GCP/Azure tags and GCP labels.
// This is so that if we decide to propagate the tags to resources in those cloud, we can do so.
// TODO validate tag value when service.CredentialModel is changed to key-value
func validateTagValue(tagVal string) error {
	if len(tagVal) > maxTagValueLength {
		return fmt.Errorf("tag value too long")
	}
	for i, r := range tagVal {
		if !isAllowedTagValueCharacters(r) {
			return fmt.Errorf("invalid character in credential tag '%s' at index %d", tagVal, i)
		}
	}
	return nil
}

func isAllowedTagValueCharacters(r rune) bool {
	if r > unicode.MaxASCII {
		return false
	}
	// Note: GCP labels only allow lower case for value. But if we switch to key-value tags entirely (REST api && service(NATS) && storage),
	// some keys might become values, so we might still need to allow upper case.
	// e.g. "<openstack-project-name>": "" => "project": "<openstack-project-name>"
	// ofc, another approach is to toLower when apply to GCP
	if unicode.IsLetter(r) {
		return true
	}
	if unicode.IsDigit(r) {
		return true
	}
	switch r {
	case '-':
		return true
	case '_':
		return true
	case '@':
		return true
	case '=':
		return true
	case '+':
		return true
	case ':':
		return true
	}
	return false
}

func notASCII(r rune) bool {
	if r > unicode.MaxASCII {
		return true
	}
	return false
}

func isASCIIControl(r rune) bool {
	if (r >= ' ' && r <= '~') || r == '\t' || r == '\n' || r == '\r' {
		return false
	}
	if notASCII(r) {
		return false
	}
	return true
}

func stringIsASCIIAndNoControl(s string) error {
	for _, c := range s {
		fmt.Printf("%d\n", c)
		if notASCII(c) {
			return fmt.Errorf("credential value must be ASCII only")
		}
		if isASCIIControl(c) {
			return fmt.Errorf("credential value cannot have ASCII control character")
		}
	}
	return nil
}
