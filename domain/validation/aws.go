package validation

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/cyverse/cacao-common/service"
)

type AWSCredential struct {
	AccessKey string `json:"AWS_ACCESS_KEY_ID" mapstructure:"AWS_ACCESS_KEY_ID"`
	SecretKey string `json:"AWS_SECRET_ACCESS_KEY" mapstructure:"AWS_SECRET_ACCESS_KEY"`
}

func validateAWSCredential(cred service.CredentialModel) error {
	if len(cred.Value) == 0 {
		return fmt.Errorf("aws credential cannot be empty")
	}
	if len(cred.Value) > 4000 {
		return fmt.Errorf("aws credential is too long")
	}

	var awsCred AWSCredential
	var err error
	if err = json.Unmarshal([]byte(cred.Value), &awsCred); err != nil {
		return fmt.Errorf("aws credential should be a json encoded string, %w", err)
	}
	if len(awsCred.AccessKey) == 0 {
		return errors.New("aws credential should not have empty access key")
	}
	if len(awsCred.SecretKey) == 0 {
		return errors.New("aws credential should not have empty secret key")
	}
	return nil
}
