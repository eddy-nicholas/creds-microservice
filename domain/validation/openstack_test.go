package validation

import (
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/service"
	"strings"
	"testing"
)

const (
	exampleOpenStackUsernameCred = `
{
  "OS_REGION_NAME": "region-name",
  "OS_PROJECT_DOMAIN_ID": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
  "OS_INTERFACE": "public",
  "OS_PROJECT_ID":"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
  "OS_USER_DOMAIN_NAME": "user-domain-name",
  "OS_IDENTITY_API_VERSION": "3",
  "OS_AUTH_URL": "https://www.cyverse.org",
  "OS_USERNAME": "aaaaaa",
  "OS_PASSWORD": "aaaaaa"
}
`

	exampleOpenStackAppCred = `
{
  "OS_REGION_NAME": "region-name",
  "OS_INTERFACE": "public",
  "OS_IDENTITY_API_VERSION": "3",
  "OS_AUTH_URL": "https://www.cyverse.org",
  "OS_AUTH_TYPE": "v3applicationcredential",
  "OS_APPLICATION_CREDENTIAL_ID": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
  "OS_APPLICATION_CREDENTIAL_SECRET": "aaaaaa"
}
`

	exampleOpenStackTokenCred = `
{
  "OS_REGION_NAME": "region-name",
  "OS_INTERFACE": "public",
  "OS_IDENTITY_API_VERSION": "3",
  "OS_AUTH_URL": "https://www.cyverse.org",
  "OS_AUTH_TYPE": "v3token",
  "OS_TOKEN": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
}
`
)

type openstackArgs struct {
	cred service.CredentialModel
}
type openstackTestCase struct {
	name    string
	args    openstackArgs
	wantErr bool
}

func TestValidateOpenStackCredential(t *testing.T) {
	tests := []openstackTestCase{
		{
			name: "empty type",
			args: openstackArgs{
				cred: service.CredentialModel{
					Value: "{}",
					Type:  "",
				},
			},
			wantErr: true,
		},
		{
			name: "not type openstack",
			args: openstackArgs{
				cred: service.CredentialModel{
					Value: "{}",
					Type:  "not-openstack",
				},
			},
			wantErr: true,
		},
		{
			name: "empty value",
			args: openstackArgs{
				cred: service.CredentialModel{
					Value: "",
					Type:  "openstack",
				},
			},
			wantErr: true,
		},
		{
			name: "non-json value",
			args: openstackArgs{
				cred: service.CredentialModel{
					Value: "foo: bar",
					Type:  "openstack",
				},
			},
			wantErr: true,
		},
		{
			name: "value too long",
			args: openstackArgs{
				cred: service.CredentialModel{
					Value: func() string {
						const valueLength = 4000

						var result strings.Builder
						result.WriteString(`{"a":"`)
						const s = "0123456789"
						for i := 0; i < valueLength/len(s); i++ {
							result.WriteString(s)
						}
						result.WriteString(`"}`)
						return result.String()
					}(),
					Type: "openstack",
				},
			},
			wantErr: true,
		},
		{
			name: "username&password",
			args: openstackArgs{
				cred: service.CredentialModel{
					Value: exampleOpenStackUsernameCred,
					Type:  "openstack",
				},
			},
			wantErr: false,
		},
		{
			name: "username&password project domain name",
			args: openstackArgs{
				cred: service.CredentialModel{
					Value: jsonReplaceField(exampleOpenStackUsernameCred, "OS_PROJECT_DOMAIN_ID", "OS_PROJECT_DOMAIN_NAME", "proj-domain-name"),
					Type:  "openstack",
				},
			},
			wantErr: false,
		},
		{
			name: "username&password user domain id",
			args: openstackArgs{
				cred: service.CredentialModel{
					Value: jsonReplaceField(exampleOpenStackUsernameCred, "OS_USER_DOMAIN_NAME", "OS_USER_DOMAIN_ID", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"),
					Type:  "openstack",
				},
			},
			wantErr: false,
		},
		{
			name: "v3applicationcredential",
			args: openstackArgs{
				cred: service.CredentialModel{
					Value: exampleOpenStackAppCred,
					Type:  "openstack",
				},
			},
			wantErr: false,
		},
		{
			name: "v3token unscoped",
			args: openstackArgs{
				cred: service.CredentialModel{
					Value: exampleOpenStackTokenCred,
					Type:  "openstack",
				},
			},
			wantErr: false,
		},
		{
			name: "v3token scoped - proj & domain id",
			args: openstackArgs{
				cred: service.CredentialModel{
					Value: jsonAddFields(exampleOpenStackTokenCred, map[string]string{
						"OS_PROJECT_DOMAIN_ID": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
						"OS_PROJECT_ID":        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
					}),
					Type: "openstack",
				},
			},
			wantErr: false,
		},
		{
			name: "v3token scoped - proj & domain name",
			args: openstackArgs{
				cred: service.CredentialModel{
					Value: jsonAddFields(exampleOpenStackTokenCred, map[string]string{
						"OS_PROJECT_DOMAIN_NAME": "proj-domain-name",
						"OS_PROJECT_NAME":        "proj-name",
					}),
					Type: "openstack",
				},
			},
			wantErr: false,
		},
		{
			name: "v3token scoped - proj id & domain name",
			args: openstackArgs{
				cred: service.CredentialModel{
					Value: jsonAddFields(exampleOpenStackTokenCred, map[string]string{
						"OS_PROJECT_DOMAIN_ID": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
						"OS_PROJECT_NAME":      "proj-name",
					}),
					Type: "openstack",
				},
			},
			wantErr: false,
		},
		{
			name: "v3token scoped - proj name & domain id",
			args: openstackArgs{
				cred: service.CredentialModel{
					Value: jsonAddFields(exampleOpenStackTokenCred, map[string]string{
						"OS_PROJECT_DOMAIN_NAME": "proj-domain-name",
						"OS_PROJECT_ID":          "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
					}),
					Type: "openstack",
				},
			},
			wantErr: false,
		},
		{
			name: "v3token scoped - proj name & no domain",
			args: openstackArgs{
				cred: service.CredentialModel{
					Value: jsonAddFields(exampleOpenStackTokenCred, map[string]string{
						"OS_PROJECT_NAME": "proj-name",
					}),
					Type: "openstack",
				},
			},
			// project name is not enough to uniquely identify a project, domain is needed
			wantErr: true,
		},
		{
			name: "v3token scoped - proj id & no domain",
			args: openstackArgs{
				cred: service.CredentialModel{
					Value: jsonAddFields(exampleOpenStackTokenCred, map[string]string{
						"OS_PROJECT_ID": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
					}),
					Type: "openstack",
				},
			},
			// project id is unique, therefore domain is no necessary
			wantErr: false,
		},
		{
			name: "v3token scoped - no proj  & domain id",
			args: openstackArgs{
				cred: service.CredentialModel{
					Value: jsonAddFields(exampleOpenStackTokenCred, map[string]string{
						"OS_PROJECT_DOMAIN_ID": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
					}),
					Type: "openstack",
				},
			},
			wantErr: false,
		},
	}
	tests = append(tests, generateOpenStackUsernameCredTestCases()...)
	tests = append(tests, generateOpenStackAppCredTestCases()...)
	tests = append(tests, generateOpenStackTokenCredTestCases()...)
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := validateOpenStackCredential(tt.args.cred)
			if tt.wantErr {
				assert.Errorf(t, err, "ValidateOpenStackCredential(%s) error = %v", tt.args.cred.Value, err)
			} else {
				assert.NoErrorf(t, err, "ValidateOpenStackCredential(%s) error = %v", tt.args.cred.Value, err)
			}
		})
	}
}

func generateOpenStackUsernameCredTestCases() []openstackTestCase {
	requiredFieldNames := []string{
		"OS_REGION_NAME",
		"OS_INTERFACE",
		"OS_IDENTITY_API_VERSION",
		"OS_AUTH_URL",
		"OS_USERNAME",
		"OS_PASSWORD",
	}
	return generateOpenStackMissingAndEmptyFieldTestCases("username&password", exampleOpenStackUsernameCred, requiredFieldNames)
}

func generateOpenStackAppCredTestCases() []openstackTestCase {
	requiredFieldNames := []string{
		"OS_REGION_NAME",
		"OS_INTERFACE",
		"OS_IDENTITY_API_VERSION",
		"OS_AUTH_URL",
		"OS_AUTH_TYPE",
		"OS_APPLICATION_CREDENTIAL_ID",
		"OS_APPLICATION_CREDENTIAL_SECRET",
	}
	return generateOpenStackMissingAndEmptyFieldTestCases("v3applicationcredential", exampleOpenStackAppCred, requiredFieldNames)
}

func generateOpenStackTokenCredTestCases() []openstackTestCase {
	requiredFieldNames := []string{
		"OS_REGION_NAME",
		"OS_INTERFACE",
		"OS_IDENTITY_API_VERSION",
		"OS_AUTH_URL",
		"OS_AUTH_TYPE",
		"OS_TOKEN",
	}
	return generateOpenStackMissingAndEmptyFieldTestCases("v3token", exampleOpenStackTokenCred, requiredFieldNames)
}

func generateOpenStackMissingAndEmptyFieldTestCases(testCasePrefix string, exampleCred string, requiredFieldNames []string) []openstackTestCase {
	var result []openstackTestCase
	for _, fieldName := range requiredFieldNames {
		// generate case for missing and empty field
		result = append(result, openstackTestCase{
			name: fmt.Sprintf("%s missing %s", testCasePrefix, fieldName),
			args: openstackArgs{
				cred: service.CredentialModel{
					Value: jsonMissingField(exampleCred, fieldName),
					Type:  "openstack",
				},
			},
			wantErr: true,
		})
		result = append(result, openstackTestCase{
			name: fmt.Sprintf("%s empty %s", testCasePrefix, fieldName),
			args: openstackArgs{
				cred: service.CredentialModel{
					Value: jsonEmptyStringField(exampleCred, fieldName),
					Type:  "openstack",
				},
			},
			wantErr: true,
		})
	}
	return result
}

func jsonMissingField(jsonBlob, fieldName string) string {
	var staging map[string]interface{}
	err := json.Unmarshal([]byte(jsonBlob), &staging)
	if err != nil {
		panic(err)
	}
	delete(staging, fieldName)
	marshal, err := json.Marshal(staging)
	if err != nil {
		panic(err)
	}
	return string(marshal)
}

func jsonEmptyStringField(jsonBlob, fieldName string) string {
	var staging map[string]interface{}
	err := json.Unmarshal([]byte(jsonBlob), &staging)
	if err != nil {
		panic(err)
	}
	staging[fieldName] = ""
	marshal, err := json.Marshal(staging)
	if err != nil {
		panic(err)
	}
	return string(marshal)
}

func jsonReplaceField(jsonBlob, fieldName, replacedFieldName, replacedFieldValue string) string {
	return jsonAddField(jsonMissingField(jsonBlob, fieldName), replacedFieldName, replacedFieldValue)
}

func jsonAddField(jsonBlob, fieldName, fieldValue string) string {
	var staging map[string]interface{}
	err := json.Unmarshal([]byte(jsonBlob), &staging)
	if err != nil {
		panic(err)
	}
	staging[fieldName] = fieldValue
	marshal, err := json.Marshal(staging)
	if err != nil {
		panic(err)
	}
	return string(marshal)
}

func jsonAddFields(jsonBlob string, fields map[string]string) string {
	var staging map[string]interface{}
	err := json.Unmarshal([]byte(jsonBlob), &staging)
	if err != nil {
		panic(err)
	}
	for key, val := range fields {
		staging[key] = val
	}
	marshal, err := json.Marshal(staging)
	if err != nil {
		panic(err)
	}
	return string(marshal)
}
