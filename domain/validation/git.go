package validation

import (
	"encoding/json"
	"fmt"
	"gitlab.com/cyverse/cacao-common/service"
)

// GitCredential describes schema for storing git password credential.
type GitCredential struct {
	Username string `json:"GIT_USERNAME" mapstructure:"GIT_USERNAME"`
	Password string `json:"GIT_PASSWORD" mapstructure:"GIT_PASSWORD"`
}

func validateGitCredential(cred service.CredentialModel) error {
	if len(cred.Value) == 0 {
		return fmt.Errorf("git credential cannot be empty")
	}

	var git GitCredential
	var err error
	err = json.Unmarshal([]byte(cred.Value), &git)
	if err != nil {
		return err
	}
	if len(git.Username) == 0 {
		return fmt.Errorf("git credential username is empty")
	}
	if len(git.Password) == 0 {
		return fmt.Errorf("git credential password is empty")
	}
	return nil
}
