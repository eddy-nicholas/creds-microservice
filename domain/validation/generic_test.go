package validation

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test_stringIsASCIIAndNoControl(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name    string
		args    args
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "all",
			args: args{
				s: "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|]~ \t\r\n",
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.NoError(t, err)
			},
		},
		{
			name: "\\0",
			args: args{
				s: "foo\u0000",
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.Error(t, err)
			},
		},
		{
			name: "DEL",
			args: args{
				s: "\u007f",
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.Error(t, err)
			},
		},
		{
			name: "unit separator",
			args: args{
				s: "\u001F",
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.Error(t, err)
			},
		},
		{
			name: "emoji",
			args: args{
				s: "foo\U0001F642",
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.Error(t, err)
			},
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.wantErr(t, stringIsASCIIAndNoControl(tt.args.s), fmt.Sprintf("stringIsASCIIAndNoControl(%v)", tt.args.s))
		})
	}
}
