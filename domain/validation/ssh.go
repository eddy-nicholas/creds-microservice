package validation

import (
	"fmt"
	"gitlab.com/cyverse/cacao-common/service"
	"golang.org/x/crypto/ssh"
	"strings"
)

// validateSSH validate ssh type credential.
// Only rsa or ed25519 public keys are acceptable (No private key). rsa keys must be at least 2048 bit long.
// In the event that we need to support private ssh key, it should be a separate type.
func validateSSH(cred service.CredentialModel) error {
	if len(cred.Value) == 0 {
		return fmt.Errorf("ssh credential cannot be empty")
	}
	if len(cred.Value) > 2048 {
		return fmt.Errorf("ssh credential too long")
	}
	if strings.HasPrefix(cred.Value, "-----BEGIN ") {
		return fmt.Errorf("private ssh key are not acceptable for type 'ssh'")
	}
	key, _, _, _, err := ssh.ParseAuthorizedKey([]byte(cred.Value))
	if err != nil {
		return err
	}
	switch key.Type() {
	case ssh.KeyAlgoRSA:
		if err = checkRSAPPublicKeyLength(cred.Value); err != nil {
			return err
		}
	case ssh.KeyAlgoDSA:
		return fmt.Errorf("dsa keys are not supported")
	}
	return nil
}

func checkRSAPPublicKeyLength(credValue string) error {
	fields := strings.Split(credValue, " ")
	if len(fields) < 2 {
		// there should be at least 2 field sperated by space e.g. "ssh-rsa" "AAAAAAAAAAA"
		return fmt.Errorf("ill-formated rsa ssh key")
	}
	// 372 is the length of a 2048 bits ssh-rsa key in the key file.
	// Note that 2047 bits also has the same length, so this is only a rough check on the length
	if len(fields[1]) < 372 {
		return fmt.Errorf("rsa key is too short, please uses at least 2048 bits")
	}
	return nil
}

func validateSSHPrivateKey(cred service.CredentialModel) error {
	if len(cred.Value) == 0 {
		return fmt.Errorf("ssh credential cannot be empty")
	}
	if !strings.HasPrefix(cred.Value, "-----BEGIN ") {
		return fmt.Errorf("private ssh key need to start with -----BEGIN")
	}
	key, err := ssh.ParsePrivateKey([]byte(cred.Value))
	if err != nil {
		// dsa key will fail here
		return err
	}
	switch key.PublicKey().Type() {
	case ssh.KeyAlgoRSA:
		// length for rsa 2048 bit
		const rsa2048MagicNumber = 279
		if len(key.PublicKey().Marshal()) < rsa2048MagicNumber {
			return fmt.Errorf("rsa key is too short, please uses at least 2048 bits")
		}
	case ssh.KeyAlgoDSA:
		// dsa key should be failing with ssh.ParsePrivateKey()
		return fmt.Errorf("dsa keys are not supported")
	}
	return nil
}
