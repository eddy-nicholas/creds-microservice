package validation

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/service"
	"os"
	"os/exec"
	"path"
	"testing"
)

// test validation for public ssh key
func TestValidateSSH(t *testing.T) {
	type args struct {
		cred service.CredentialModel
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "ssh-dss",
			args: args{
				cred: service.CredentialModel{
					Value: "ssh-dss AAAAB3NzaC1kc3MAAACBAOGHoO00cg0iNN/KjUEBNx34oNkS/ZiPlXQOog0axwTcY1ctqyMnCCdC9vE2x5IPwUGK0lsrwDxcJoGB5YKQWlqXGuYGJ00CudY8mJjkb7RjNgkBL6FyMDb9+TfFO1cdWwrhE3DfXWCrkQkkvviE+sPfpV1tvufll/FbF5h0w/QBAAAAFQDK1tiuR5/V2zSwCYd6VyRT+JEYpwAAAIAsOB3zfS5gsroC/vY2aGoKOacofv+C9dr91x7+qYXBPacFI5g0/TiC0cTJCLazH8ZJ5NAVPJ9Hp836I4xaTXMoSrSo/0Lj7Lg7M6PxYSixL5y6nyemh1ABw5t2GJJloXHRTNM3iCpwdVdEgGiHys8b+g+/ZzD0sGs9SrmJUPBHrAAAAIBn9ctQ4CTzGT+VL9oVexhcHQ4NxhUltxY7UEuivp672UhM+IxlkyKdXk+iE4arKvCiD+7PGwO9TFV+lzSoNi8psGQrw3Vu8T0L1n6cG6d5Twe/DJsOPxC6zrF6Lg1+hlk3nMF28vDG0brqb1GL+tVxCmKJBi4RSPd4mdrxLax6zg== cred-svc-unit-test",
				},
			},
			wantErr: true,
		},
		{
			name: "ssh-rsa 1024",
			args: args{
				cred: service.CredentialModel{
					Value: "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAAAgQDXgqtCtn1uUO6Xt6VVFxdOgrDWpj5kGSVfaHNb0t2I6xZfjLV0t2ZeXvMzChlQEu79ajWmjiDXht3JuQDWLORZWYQA8WKwyrF8qb4FG+lVOj/JLYvf4ykpj7YINnbLqCZxKQVMXUkEEbyFGs/0PR4gZyQgnxfyg6agvS5pUfU0dQ== cred-svc-unit-test",
				},
			},
			wantErr: true,
		},
		{
			name: "ssh-rsa 2047",
			args: args{
				cred: service.CredentialModel{
					Value: "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAGKsAIMSn48wE4scO1TK7DHTMA+UDftMkNAaTn8BnB+y5wW2dnTTO00b1Wo7RohGZjmcd4Mn4n4ekirXoN//sV4etdacUbWsHATdTkQSTwV30j2Bm0otvxpCkTjUMPSHzRErt5CfEhjQhpRx4oDGjDcGCKpoBpwKzoEpSzxz4wL64kSi1xfzgrHrxhvQDIgS2nOl5zbxinKsIm6/0LDfD23cYkBkxNlE8DU2kXyVbxvSVssxZSkDks/4Ri8yvVEPZPx21IPe+C8b8ffwMmjrXrQdiSIzzEnkaSbfYm4vBLtbmglR2WqI/hS14jcwU30f2p8ma9h3juYDjBCBK3mNWPE= cred-svc-unit-test",
				},
			},
			wantErr: false, // we only perform a simple check on the length of rsa key, so 2047 still passes
		},
		{
			name: "ssh-rsa 2047 no comment",
			args: args{
				cred: service.CredentialModel{
					Value: "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAFGLZjvF9l2bcqD5hX4Mq8pRNuAfuLafNdYDybPqSNlJRyNthE11lRe80tQu4FOOdIjIpJMKbl7a30usr9tTzNdN8qy6/M/pNmWNmjefqKoQPk/rxBC5bKmNCEcjcAYc8SJ36i6JCd+j0xLEpUS7uJLhric4GzgUsMN1BUCYJaBPqHpSi7XNSYup1jq8kLfa3khK3HYiU90OW1m25nGXROSVf6599ocZE6n3s4ZHpeGAhmRKwvR94l9O/N4XoDi61J3mW5tQbxDgtSwIOBYiqPaVbG/H9IW0yM88OfY+OwT2JJw1M0swalldLjar2yadNYtJQoA51L2elffPCZQ3GOs=",
				},
			},
			wantErr: false, // we only perform a simple check on the length of rsa key, so 2047 still passes
		},
		{
			name: "ssh-rsa 2048",
			args: args{
				cred: service.CredentialModel{
					Value: "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCt9iXu4ntmSJrHKDN40a5moXWQ4i1zhwv0ZZwHv59WNDr4L5Gw03FQj7NMpgwUwPOC3y/8PB0Eq+s0qAhDZ9UECm24BOk+Zw8RdeUEUSp++w8BeIAkiTSPeZO8sCfFtwmBrzfjpqSh9Ei3BeNClnDwldNPbqZgEpo55WfMh62J7GnUeHr6z/FbDcM3eM+bPs+ECWEAs0yxdusRrlvdkCf3olFlUT76GHRq3eWikGRcFUQckRH9cFe9vs4WRbwPYd3qvcjPV3YPYOt6lsy5s1rFLrgbBJ60BeZyv4A0//EDf8ozKlWLVxpF54pF8zzSdNwcQoi9ozEaBNcnDVgrfeyH cred-svc-unit-test",
				},
			},
			wantErr: false,
		},
		{
			name: "ssh-rsa 2048 no comment",
			args: args{
				cred: service.CredentialModel{
					Value: "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDLYXqVu9ituXi8KaQ+/dZnbjtMpDbZtnbxPQZLGIutF635rqMPn7CysOCrNXDIpd+35UvpHiJR6q/vXZT1P4/nFgXW2LfDbPI20blLDnyq+sXbIygxQx/m0oEMjip53puLv73GJ187L0GlvghCz4m0sR5PJJhUcvIcfAbCa9t0HWrU+LgaMC0AGXPe6MXM2MUj89j3K5QfdHGGU/JZr9nzmI+A97qeyHUT3n8meIXLqVjJfAWQqM0zjbAZgLWb9aC58FF5vmjDCu3HXwqWEzcOsR74cmINlyuUcFfSvNYFyZ2+he83K9IKm7WL8xHyVXVLQ636dpT9fZnp5cmZiBY7",
				},
			},
			wantErr: false,
		},
		{
			name: "ssh-rsa 4096",
			args: args{
				cred: service.CredentialModel{
					Value: "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDMDhh61ButetTYTVee8lxu5ulwwdbQkE8WDVQQcaqMWDPMueBkFgXlgDsBU2oPXgprdJE2T4nNDtYEvsvCRUpm3V+QT69lu2stQ5Ax4BHzKQXJw46GJl/LEsLSXD76fBw+JGvke+uN5Xtw3Sg9wS04a3IqJa3zFriVhjCGowH8VUB7flL6YO3Aud2cy1knGs8o7HLhgxfAmpYPr1iGnWt+JhQsz1SAF5gVn0on1/NV5/mrpoC4E5YGle+kqTeTocSMDbsus5as1uGN5jWb+aEVMdRbCVQCTmmszzFb1M9eYmGRBSF1/5EG9lNWUL3geT5te1GlPifH/EBpSXq/hNTpKOwx/mZhGQXl2yXKC62PJTmb41/q/xN8GR2HAGgWGXi2KC3+RzEaSamSMNu/jTIh++L8YDmFNoME0ncBQYgZCgBOs5XSX6y5z3MuMwqpbA+XvsbTlZhO2H3G4gQachS5T5NtC4VeB/VLVDwcitg0tIui1lyTtAtwLe3EVaLLJiZxL4eqDByYVt/qPDjbvzP0iIeVcAIQ1w3JTgaSeFTP18gRTTZQVQ3pS4yqpOMfyM19qlLa0Z5RHW7S2k3eNjEQQ/asPtNurCC/lXGDbqGrKiyshGooz2IY0l1jlsDDyO19OntVH74t+WYtaQh0fIGJ1a+NASsakZ06kmKHBm8euQ== cred-svc-unit-test",
				},
			},
			wantErr: false,
		},
		{
			name: "ssh-rsa 4096 no comment",
			args: args{
				cred: service.CredentialModel{
					Value: "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCsEpJPJTb7iMq8lvQSd4iRX70yCtDI7wz0YiRnlkmtPxFs/6XswHfNFOGW3vFjldWac9yHaNW8vBD+JKy2yl7vVvs8j0FqdfwwFFUJIJGjutCCbVBegUyoBvZD4eZtTXH9u6bLmaEkJJZ36X7OD3IBQPA97WbssBT+dRHGOvae0Nzj9+O2DKkSvQL0688sgMYsI2bxL18NZIFAU8vYJVBrOg5ceTnvImKDYcrDkKr/LOAKvPBam9+Zb28hBPHQ2/E06eaAQvNg+cR5xPiaSpQchAIrP3cfNY+9QbIvFgsCylkf3hoV2php7/14A8jJasIF7ZQiGheaMckAVSyWoHf/TVzUlS1VM8FpIL258U2JltYpeRjROETo5QhoG9LcIQvte8ElP0QNe8FWO6/8qtmTdl1GTYMI+YBKQG/2+ogGHHPudDDpUYg8oL8Hr8U1TLuFLmK9qERb7zUgzoXtWFQbBfekQRBh6DCnVDQvJN82iiIUTQA2oLonqW2Tn47jBe7fqvrnO0QldTFy4eYTxOeQP0sbSFIrTQ2Xa9O+Pg/g5ltRveBF03YdvADR4HwsYDuuJMstHpCf0AfmOrkGeduff3aCveIZPR09EundO2WuP2MDZZJeXvj4rD+7ZFtcUgHd4EiKZZmhVBliYhiUn15bmDKuCCGKBdVsVasAEfcijQ==",
				},
			},
			wantErr: false,
		},
		{
			name: "ssh-rsa 4096 w/ passphrase",
			args: args{
				cred: service.CredentialModel{
					Value: "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDDLbVUOXPcxWkoX7SlgGhkaE4ChxEbqayX6+lCct2+w1S7aE2yjCyURmpGCLVto7leAffiFf6VgkkWEUbTlaAwr1fl+BgD66arUmZ1lNeL/BLYV/ZrIi8ZfQtVIEmR0CAobqXw023K5/3gHPzYgU3Wg4J8cvTtfExq3qWEsfQQYojX9COXV3lwyM+2OVz55hk0NXWfhafHFUyFIw+Nq+SW9ZGRwr/eyvNQD2X++u70sMfFe9VYIHH4hoWWb4NaQ/kMEA1+q74kYRP2vSFQJzQT8Tkv9K4kJ/FykP2Y6qqjG71lVql4ZnL2ZNBw8nR5pqHvf97s5B0fYJIwdIrxTldwswKNiaPBbamRsD5PoQgpUvo5T38u4Pm/WzQGEHvsQLgVnA4kCfaALTnxjXQYJU1xjI+6wCgCKJpBR+DOxru142GdDqHOFoSodnyCOmlXfBSgSDtSJp7wEpnwcQyFDzV71JGq8tosTtWeXZuVWCpJM4W+4NkujtUha7BnZTAZNjoucFUTM3+cRhbt/GpD7+rl/5d003cubQ7RHY5kW8YRWDE3WWXcoInrBvNXAZNb8J1HNpoMoEsjaIOkTKzwSLxr7HPXQkuMEBzIFe+Q+fVJ8F/Rzwzpu4tiAgO5C0kWYY2OFtmAe76bR/haouJo+I9cD79vRAOj6razQyFsuPn8AQ== cred-svc-unit-test",
				},
			},
			wantErr: false,
		},
		{
			name: "ssh-ed25519",
			args: args{
				cred: service.CredentialModel{
					Value: "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPxGBQ+A6m7psGukFeM8QTNU0mpPoLA14lvMmQQi3DRi cred-svc-unit-test",
				},
			},
			wantErr: false,
		},
		{
			name: "incomplete ssh-rsa",
			args: args{
				cred: service.CredentialModel{
					Value: "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDMDhh61ButetTYTVee8lxu5ulwwdbQkE8WDVQQcaqMWDPMueBkFgXlgDsBU2oPXgprdJE2T4nNDtYEvsvCRUpm3V+QT69lu2stQ5Ax4BHzKQXJw46GJl cred-svc-unit-test",
				},
			},
			wantErr: true,
		},
		{
			name: "private ssh key",
			args: args{
				cred: service.CredentialModel{
					Value: `-----BEGIN OPENSSH PRIVATE KEY-----
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
-----END OPENSSH PRIVATE KEY-----`,
				},
			},
			wantErr: true,
		},
		{
			name: "private ssh key rsa key format",
			args: args{
				cred: service.CredentialModel{
					Value: `-----BEGIN RSA PRIVATE KEY-----
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
-----END RSA PRIVATE KEY-----`,
				},
			},
			wantErr: true,
		},
		{
			name: "empty value",
			args: args{
				cred: service.CredentialModel{
					Value: "",
				},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := validateSSH(tt.args.cred)
			if tt.wantErr {
				assert.Errorf(t, err, "validateSSH() error = %v, wantErr %v", err, tt.wantErr)
			} else {
				assert.NoErrorf(t, err, "validateSSH() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

// test validation for private ssh key
// this test requires ssh-keygen to be present
func Test_validateSSHPrivateKey(t *testing.T) {
	if ssh, ok := os.LookupEnv("CI_INTEGRATION_SSH"); !ok || ssh != "true" {
		t.Skip("CI_INTEGRATION_SSH is not set")
		return
	}

	generateSSHKey := func(keyType string, bits uint) string {
		temp, err := os.MkdirTemp("", "ssh-dir-*")
		if err != nil {
			panic(err)
		}
		var privateKeyPath = path.Join(temp, "id_test")
		switch keyType {
		case "rsa":
			fallthrough
		case "ecdsa":
			err = exec.Command("ssh-keygen", "-t", keyType, "-b", fmt.Sprintf("%d", bits), "-f", privateKeyPath).Run()
			if err != nil {
				panic(err)
			}
		default:
			err = exec.Command("ssh-keygen", "-t", keyType, "-f", privateKeyPath).Run()
			if err != nil {
				panic(err)
			}
		}
		privateKey, err := os.ReadFile(privateKeyPath)
		if err != nil {
			panic(err)
		}
		return string(privateKey)
	}
	tests := []struct {
		name    string
		keyType string
		bits    uint
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name:    "rsa-4096",
			keyType: "rsa",
			bits:    4096,
			wantErr: assert.NoError,
		},
		{
			name:    "rsa-2048",
			keyType: "rsa",
			bits:    2048,
			wantErr: assert.NoError,
		},
		{
			name:    "rsa-2047",
			keyType: "rsa",
			bits:    2047,
			wantErr: assert.Error,
		},
		{
			name:    "rsa-1024",
			keyType: "rsa",
			bits:    1024,
			wantErr: assert.Error,
		},
		{
			name:    "ed25519",
			keyType: "ed25519",
			bits:    0,
			wantErr: assert.NoError,
		},
		{
			name:    "ecdsa-521",
			keyType: "ecdsa",
			bits:    521,
			wantErr: assert.NoError,
		},
		{
			name:    "ecdsa-384",
			keyType: "ecdsa",
			bits:    384,
			wantErr: assert.NoError,
		},
		{
			name:    "ecdsa-256",
			keyType: "ecdsa",
			bits:    256,
			wantErr: assert.NoError,
		},
		{
			name:    "dsa",
			keyType: "dsa",
			bits:    0,
			wantErr: assert.Error, // we don't want to support dsa keys
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.wantErr(t, validateSSHPrivateKey(service.CredentialModel{Value: generateSSHKey(tt.keyType, tt.bits)}), fmt.Sprintf("validateSSHPrivateKey(%v, %v)", tt.keyType, tt.bits))
		})
	}
}
