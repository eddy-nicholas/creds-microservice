package validation

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/service"
	"testing"
)

func Test_validateGitCredential(t *testing.T) {
	type args struct {
		cred service.CredentialModel
	}
	tests := []struct {
		name    string
		args    args
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "valid",
			args: args{
				cred: service.CredentialModel{
					Value: `
{
  "GIT_USERNAME": "foo",
  "GIT_PASSWORD": "bar"
}
`,
				},
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.NoError(t, err)
			},
		},
		{
			name: "empty string",
			args: args{
				cred: service.CredentialModel{
					Value: "",
				},
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.Error(t, err)
			},
		},
		{
			name: "bad JSON",
			args: args{
				cred: service.CredentialModel{
					Value: "{",
				},
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.Error(t, err)
			},
		},
		{
			name: "empty username & empty password",
			args: args{
				cred: service.CredentialModel{
					Value: `
{
  "GIT_USERNAME": "",
  "GIT_PASSWORD": ""
}
`,
				},
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.Error(t, err)
			},
		},
		{
			name: "missing username key",
			args: args{
				cred: service.CredentialModel{
					Value: `
{
  "GIT_PASSWORD": "foo"
}
`,
				},
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.Error(t, err)
			},
		},
		{
			name: "missing password key",
			args: args{
				cred: service.CredentialModel{
					Value: `
{
  "GIT_USERNAME": "foo",
}
`,
				},
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.Error(t, err)
			},
		},
		{
			name: "empty password",
			args: args{
				cred: service.CredentialModel{
					Value: `
{
  "GIT_USERNAME": "foo",
  "GIT_PASSWORD": ""
}
`,
				},
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.Error(t, err)
			},
		},
		{
			name: "empty username",
			args: args{
				cred: service.CredentialModel{
					Value: `
{
  "GIT_USERNAME": "",
  "GIT_PASSWORD": "foo"
}
`,
				},
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.Error(t, err)
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.wantErr(t, validateGitCredential(tt.args.cred), fmt.Sprintf("validateGitCredential(%v)", tt.args.cred))
		})
	}
}
