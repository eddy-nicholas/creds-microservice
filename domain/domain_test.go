package domain

import (
	"context"
	"fmt"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	portsmocks "gitlab.com/cyverse/creds-microservice/ports/mocks"
	"gitlab.com/cyverse/creds-microservice/types"
	"sync"
	"testing"
	"time"
)

type QueryMsgPair struct {
	Query         types.QueryMessage
	ExpectedReply types.QueryMessage
}

func TestDomain_processQueryWorker(t *testing.T) {
	const defaultQueryCtxTimeout = time.Millisecond * 50
	type fields struct {
		MockStorage *portsmocks.PersistentStoragePort
	}
	type args struct {
		// the timeout for the ctx being passed to processQueryWorker(), the method is expected to return after ctx timeout
		ctxTimeout time.Duration
		msgPairs   []QueryMsgPair
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "1 get",
			fields: fields{
				MockStorage: func() *portsmocks.PersistentStoragePort {
					storage := portsmocks.NewPersistentStoragePort(t)
					storage.On("Get", service.CredentialModel{
						Session: service.Session{
							SessionActor:    "testuser-123",
							SessionEmulator: "",
						},
						Username: "testuser-123",
						ID:       "cred-123",
					}).Return(service.CredentialModel{
						Session: service.Session{
							SessionActor:    "testuser-123",
							SessionEmulator: "",
						},
						Username: "testuser-123",
						Value:    "cred-value-123",
						ID:       "cred-123",
					}, nil).Once()
					return storage
				}(),
			},
			args: args{
				ctxTimeout: defaultQueryCtxTimeout,
				msgPairs: []QueryMsgPair{
					{
						Query: types.QueryMessage{
							Type: service.NatsSubjectCredentialsGet,
							Obj: service.CredentialGetRequest{
								Session: service.Session{
									SessionActor:    "testuser-123",
									SessionEmulator: "",
								},
								Username: "testuser-123",
								ID:       "cred-123",
							},
							Response: make(chan types.QueryMessage, 1),
						},
						ExpectedReply: types.QueryMessage{
							Type: service.NatsSubjectCredentialsGet + types.QueryReplyTypePostfix,
							Obj: service.CredentialGetReply{
								Session: service.Session{
									SessionActor:    "testuser-123",
									SessionEmulator: "",
								},
								Username: "testuser-123",
								Value:    "cred-value-123",
								ID:       "cred-123",
							},
							Response: nil,
						},
					},
				},
			},
		},
		{
			name: "2 get same ID",
			fields: fields{
				MockStorage: func() *portsmocks.PersistentStoragePort {
					storage := portsmocks.NewPersistentStoragePort(t)
					storage.On("Get", service.CredentialModel{
						Session: service.Session{
							SessionActor:    "testuser-123",
							SessionEmulator: "",
						},
						Username: "testuser-123",
						ID:       "cred-123",
					}).Return(service.CredentialModel{
						Session: service.Session{
							SessionActor:    "testuser-123",
							SessionEmulator: "",
						},
						Username: "testuser-123",
						Value:    "cred-value-123",
						ID:       "cred-123",
					}, nil).Twice()
					return storage
				}(),
			},
			args: args{
				ctxTimeout: defaultQueryCtxTimeout,
				msgPairs: []QueryMsgPair{
					{
						Query: types.QueryMessage{
							Type: service.NatsSubjectCredentialsGet,
							Obj: service.CredentialGetRequest{
								Session: service.Session{
									SessionActor:    "testuser-123",
									SessionEmulator: "",
								},
								Username: "testuser-123",
								ID:       "cred-123",
							},
							Response: make(chan types.QueryMessage, 1),
						},
						ExpectedReply: types.QueryMessage{
							Type: service.NatsSubjectCredentialsGet + types.QueryReplyTypePostfix,
							Obj: service.CredentialGetReply{
								Session: service.Session{
									SessionActor:    "testuser-123",
									SessionEmulator: "",
								},
								Username: "testuser-123",
								Value:    "cred-value-123",
								ID:       "cred-123",
							},
							Response: nil,
						},
					},
					{
						Query: types.QueryMessage{
							Type: service.NatsSubjectCredentialsGet,
							Obj: service.CredentialGetRequest{
								Session: service.Session{
									SessionActor:    "testuser-123",
									SessionEmulator: "",
								},
								Username: "testuser-123",
								ID:       "cred-123",
							},
							Response: make(chan types.QueryMessage, 1),
						},
						ExpectedReply: types.QueryMessage{
							Type: service.NatsSubjectCredentialsGet + types.QueryReplyTypePostfix,
							Obj: service.CredentialGetReply{
								Session: service.Session{
									SessionActor:    "testuser-123",
									SessionEmulator: "",
								},
								Username: "testuser-123",
								Value:    "cred-value-123",
								ID:       "cred-123",
							},
							Response: nil,
						},
					},
				},
			},
		},
		{
			name: "2 get diff ID",
			fields: fields{
				MockStorage: func() *portsmocks.PersistentStoragePort {
					storage := portsmocks.NewPersistentStoragePort(t)
					storage.On("Get", service.CredentialModel{
						Session: service.Session{
							SessionActor:    "testuser-123",
							SessionEmulator: "",
						},
						Username: "testuser-123",
						ID:       "cred-123",
					}).Return(service.CredentialModel{
						Session: service.Session{
							SessionActor:    "testuser-123",
							SessionEmulator: "",
						},
						Username: "testuser-123",
						Value:    "cred-value-123",
						ID:       "cred-123",
					}, nil)
					storage.On("Get", service.CredentialModel{
						Session: service.Session{
							SessionActor:    "testuser-123",
							SessionEmulator: "",
						},
						Username: "testuser-123",
						ID:       "cred-123-diff",
					}).Return(service.CredentialModel{
						Session: service.Session{
							SessionActor:    "testuser-123",
							SessionEmulator: "",
						},
						Username: "testuser-123",
						Value:    "cred-value-123-diff",
						ID:       "cred-123-diff",
					}, nil).Once()
					return storage
				}(),
			},
			args: args{
				ctxTimeout: defaultQueryCtxTimeout,
				msgPairs: []QueryMsgPair{
					{
						Query: types.QueryMessage{
							Type: service.NatsSubjectCredentialsGet,
							Obj: service.CredentialGetRequest{
								Session: service.Session{
									SessionActor:    "testuser-123",
									SessionEmulator: "",
								},
								Username: "testuser-123",
								ID:       "cred-123",
							},
							Response: make(chan types.QueryMessage, 1),
						},
						ExpectedReply: types.QueryMessage{
							Type: service.NatsSubjectCredentialsGet + types.QueryReplyTypePostfix,
							Obj: service.CredentialGetReply{
								Session: service.Session{
									SessionActor:    "testuser-123",
									SessionEmulator: "",
								},
								Username: "testuser-123",
								Value:    "cred-value-123",
								ID:       "cred-123",
							},
							Response: nil,
						},
					},
					{
						Query: types.QueryMessage{
							Type: service.NatsSubjectCredentialsGet,
							Obj: service.CredentialGetRequest{
								Session: service.Session{
									SessionActor:    "testuser-123",
									SessionEmulator: "",
								},
								Username: "testuser-123",
								ID:       "cred-123-diff",
							},
							Response: make(chan types.QueryMessage, 1),
						},
						ExpectedReply: types.QueryMessage{
							Type: service.NatsSubjectCredentialsGet + types.QueryReplyTypePostfix,
							Obj: service.CredentialGetReply{
								Session: service.Session{
									SessionActor:    "testuser-123",
									SessionEmulator: "",
								},
								Username: "testuser-123",
								Value:    "cred-value-123-diff",
								ID:       "cred-123-diff",
							},
							Response: nil,
						},
					},
				},
			},
		},
		{
			name: "1 get, actor not owner",
			fields: fields{
				MockStorage: func() *portsmocks.PersistentStoragePort {
					storage := portsmocks.NewPersistentStoragePort(t)
					return storage
				}(),
			},
			args: args{
				ctxTimeout: defaultQueryCtxTimeout,
				msgPairs: []QueryMsgPair{
					{
						Query: types.QueryMessage{
							Type: service.NatsSubjectCredentialsGet,
							Obj: service.CredentialGetRequest{
								Session: service.Session{
									SessionActor:    "testuser-123",
									SessionEmulator: "",
								},
								Username: "diff-testuser-123",
								ID:       "cred-123",
							},
							Response: make(chan types.QueryMessage, 1),
						},
						ExpectedReply: types.QueryMessage{
							Type: service.NatsSubjectCredentialsGet + types.QueryReplyTypePostfix,
							Obj: service.CredentialGetReply{
								Session: service.Session{
									SessionActor:    "testuser-123",
									SessionEmulator: "",
									ErrorType:       "unauthorized access - only owner of the credential is allowed to access", // FIXME use service error instead
								},
								Username: "diff-testuser-123",
								ID:       "cred-123",
							},
							Response: nil,
						},
					},
				},
			},
		},
		{
			name: "1 get error",
			fields: fields{
				MockStorage: func() *portsmocks.PersistentStoragePort {
					storage := portsmocks.NewPersistentStoragePort(t)
					storage.On("Get", service.CredentialModel{
						Session: service.Session{
							SessionActor:    "testuser-123",
							SessionEmulator: "",
						},
						Username: "testuser-123",
						ID:       "cred-123",
					}).Return(service.CredentialModel{}, fmt.Errorf("get operation failed")).Once()
					return storage
				}(),
			},
			args: args{
				ctxTimeout: defaultQueryCtxTimeout,
				msgPairs: []QueryMsgPair{
					{
						Query: types.QueryMessage{
							Type: service.NatsSubjectCredentialsGet,
							Obj: service.CredentialGetRequest{
								Session: service.Session{
									SessionActor:    "testuser-123",
									SessionEmulator: "",
								},
								Username: "testuser-123",
								ID:       "cred-123",
							},
							Response: make(chan types.QueryMessage, 1),
						},
						ExpectedReply: types.QueryMessage{
							Type: service.NatsSubjectCredentialsGet + types.QueryReplyTypePostfix,
							Obj: service.CredentialGetReply{
								Session: service.Session{
									SessionActor:    "testuser-123",
									SessionEmulator: "",
									ErrorType:       "get operation failed", // FIXME use service error instead
								},
								Username: "testuser-123",
								ID:       "cred-123",
							},
							Response: nil,
						},
					},
				},
			},
		},
		{
			name: "1 empty list",
			fields: fields{
				MockStorage: func() *portsmocks.PersistentStoragePort {
					storage := portsmocks.NewPersistentStoragePort(t)
					storage.On("List", types.ListQueryData{
						Actor:    "testuser-123",
						Emulator: "",
						Username: "testuser-123",
					}).Return([]service.CredentialModel{}, nil).Once()
					return storage
				}(),
			},
			args: args{
				ctxTimeout: defaultQueryCtxTimeout,
				msgPairs: []QueryMsgPair{
					{
						Query: types.QueryMessage{
							Type: service.NatsSubjectCredentialsList,
							Obj: service.CredentialListRequest{
								Session: service.Session{
									SessionActor:    "testuser-123",
									SessionEmulator: "",
								},
								Username: "testuser-123",
							},
							Response: make(chan types.QueryMessage, 1),
						},
						ExpectedReply: types.QueryMessage{
							Type: service.NatsSubjectCredentialsList + types.QueryReplyTypePostfix,
							Obj: service.CredentialListReply{
								Session: service.Session{
									SessionActor:    "testuser-123",
									SessionEmulator: "",
								},
								Username: "testuser-123",
								List:     []service.CredentialModel{},
							},
							Response: nil,
						},
					},
				},
			},
		},
		{
			name: "1 list",
			fields: fields{
				MockStorage: func() *portsmocks.PersistentStoragePort {
					storage := portsmocks.NewPersistentStoragePort(t)
					storage.On("List", types.ListQueryData{
						Actor:    "testuser-123",
						Emulator: "",
						Username: "testuser-123",
					}).Return([]service.CredentialModel{
						{
							Session: service.Session{
								SessionActor:    "testuser-123",
								SessionEmulator: "",
							},
							Username: "testuser-123",
							Value:    "cred-value-123",
							ID:       "cred-123",
						},
					}, nil).Once()
					return storage
				}(),
			},
			args: args{
				ctxTimeout: defaultQueryCtxTimeout,
				msgPairs: []QueryMsgPair{
					{
						Query: types.QueryMessage{
							Type: service.NatsSubjectCredentialsList,
							Obj: service.CredentialListRequest{
								Session: service.Session{
									SessionActor:    "testuser-123",
									SessionEmulator: "",
								},
								Username: "testuser-123",
							},
							Response: make(chan types.QueryMessage, 1),
						},
						ExpectedReply: types.QueryMessage{
							Type: service.NatsSubjectCredentialsList + types.QueryReplyTypePostfix,
							Obj: service.CredentialListReply{
								Session: service.Session{
									SessionActor:    "testuser-123",
									SessionEmulator: "",
								},
								Username: "testuser-123",
								List: []service.CredentialModel{
									{
										Session: service.Session{
											SessionActor:    "testuser-123",
											SessionEmulator: "",
										},
										Username: "testuser-123",
										Value:    "cred-value-123",
										ID:       "cred-123",
									},
								},
							},
							Response: nil,
						},
					},
				},
			},
		},
		{
			name: "1 list error",
			fields: fields{
				MockStorage: func() *portsmocks.PersistentStoragePort {
					storage := portsmocks.NewPersistentStoragePort(t)
					storage.On("List", types.ListQueryData{
						Actor:    "testuser-123",
						Emulator: "",
						Username: "testuser-123",
					}).Return(nil, fmt.Errorf("list operation failed")).Once()
					return storage
				}(),
			},
			args: args{
				ctxTimeout: defaultQueryCtxTimeout,
				msgPairs: []QueryMsgPair{
					{
						Query: types.QueryMessage{
							Type: service.NatsSubjectCredentialsList,
							Obj: service.CredentialListRequest{
								Session: service.Session{
									SessionActor:    "testuser-123",
									SessionEmulator: "",
								},
								Username: "testuser-123",
							},
							Response: make(chan types.QueryMessage, 1),
						},
						ExpectedReply: types.QueryMessage{
							Type: service.NatsSubjectCredentialsList + types.QueryReplyTypePostfix,
							Obj: service.CredentialListReply{
								Session: service.Session{
									SessionActor:    "testuser-123",
									SessionEmulator: "",
									ErrorType:       "list operation failed",
									ErrorMessage:    "",
									ServiceError:    service.CacaoErrorBase{},
								},
								Username: "testuser-123",
								List:     nil,
							},
							Response: nil,
						},
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := Domain{
				VaultOut: tt.fields.MockStorage,
			}

			ctx, _ := context.WithTimeout(context.Background(), tt.args.ctxTimeout)

			var queryChan = pushQueryMsgToChannel(tt.args.msgPairs)

			var wg sync.WaitGroup
			wg.Add(1)

			d.processQueryWorker(ctx, queryChan, &wg)

			checkQueryReplyInChannel(t, tt.args.msgPairs)

			tt.fields.MockStorage.AssertExpectations(t)
		})
	}
}

func pushQueryMsgToChannel(msgPairs []QueryMsgPair) chan types.QueryMessage {
	var queryChan = make(chan types.QueryMessage, len(msgPairs))
	for _, pair := range msgPairs {
		queryChan <- pair.Query
	}
	return queryChan
}

// check reply in each response channel
func checkQueryReplyInChannel(t *testing.T, msgPairs []QueryMsgPair) {
	for i, msgPair := range msgPairs {
		select {
		case reply := <-msgPair.Query.Response:
			assert.Equalf(t, msgPair.ExpectedReply, reply, "msg %d didnt match", i)
		case <-time.After(time.Millisecond * 100):
			assert.Fail(t, "timed out, cannot check reply")
		}
	}
}

type EventMsgPair struct {
	Request          types.EventMessage
	ExpectedResponse EventResponse
}

type EventResponse struct {
	eventType common.EventType
	data      interface{}
	tid       common.TransactionID
}

func TestDomain_processEventWorker(t *testing.T) {
	const defaultEventCtxTimeout = time.Millisecond * 50
	type fields struct {
		MockEventOut MockEventOut
		MockStorage  *portsmocks.PersistentStoragePort
	}
	type args struct {
		// the timeout for the ctx being passed to processEventWorker(), the method is expected to return after ctx timeout
		ctxTimeout time.Duration
		msgPairs   []EventMsgPair
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "1 create, empty id",
			fields: fields{
				MockEventOut: MockEventOut{
					ReturnedError: nil,
					EventRespChan: make(chan EventResponse, 1),
				},
				MockStorage: func() *portsmocks.PersistentStoragePort {
					storage := portsmocks.NewPersistentStoragePort(t)
					storage.On("List", types.ListQueryData{
						Username: "testuser-123",
					}).Return([]service.CredentialModel{}, nil).Once()
					storage.On("Create", service.CredentialModel{ // same as request
						Session: service.Session{
							SessionActor:    "testuser-123",
							SessionEmulator: "",
						},
						Username:    "testuser-123",
						Name:        "cred-name-123",
						Value:       "{}",
						Type:        "type123",
						ID:          testCredentialID.String(),
						Description: "",
					}).Return(nil).Once()
					return storage
				}(),
			},
			args: args{
				ctxTimeout: defaultEventCtxTimeout,
				msgPairs: []EventMsgPair{
					{
						Request: types.EventMessage{
							Type: string(service.EventCredentialAddRequested),
							TID:  "tid-aaaaaaaaaaaaaaaaaaaa",
							Obj: service.CredentialCreateRequest{
								Session: service.Session{
									SessionActor:    "testuser-123",
									SessionEmulator: "",
								},
								Username:    "testuser-123",
								Name:        "cred-name-123",
								Value:       "{}",
								Type:        "type123",
								ID:          "", // empty ID in request
								Description: "",
							},
						},
						ExpectedResponse: EventResponse{
							eventType: service.EventCredentialAdded,
							data: service.CredentialCreateResponse{
								Session: service.Session{
									SessionActor:    "testuser-123",
									SessionEmulator: "",
								},
								Username: "testuser-123",
								ID:       testCredentialID.String(),
							},
							tid: "tid-aaaaaaaaaaaaaaaaaaaa",
						},
					},
				},
			},
		},
		{
			name: "1 create, non-empty id",
			fields: fields{
				MockEventOut: MockEventOut{
					ReturnedError: nil,
					EventRespChan: make(chan EventResponse, 1),
				},
				MockStorage: func() *portsmocks.PersistentStoragePort {
					storage := portsmocks.NewPersistentStoragePort(t)
					storage.On("List", types.ListQueryData{
						Username: "testuser-123",
					}).Return([]service.CredentialModel{}, nil).Once()
					storage.On("Create", service.CredentialModel{ // same as request
						Session: service.Session{
							SessionActor:    "testuser-123",
							SessionEmulator: "",
						},
						Username:    "testuser-123",
						Name:        "cred-name-123",
						Value:       "{}",
						Type:        "type123",
						ID:          testCredentialID.String(), // non-empty ID in request
						Description: "",
					}).Return(nil).Once()
					return storage
				}(),
			},
			args: args{
				ctxTimeout: defaultEventCtxTimeout,
				msgPairs: []EventMsgPair{
					{
						Request: types.EventMessage{
							Type: string(service.EventCredentialAddRequested),
							TID:  "tid-aaaaaaaaaaaaaaaaaaaa",
							Obj: service.CredentialCreateRequest{
								Session: service.Session{
									SessionActor:    "testuser-123",
									SessionEmulator: "",
								},
								Username:    "testuser-123",
								Name:        "cred-name-123",
								Value:       "{}",
								Type:        "type123",
								ID:          "cred-id-123",
								Description: "",
							},
						},
						ExpectedResponse: EventResponse{
							eventType: service.EventCredentialAdded,
							data: service.CredentialCreateResponse{
								Session: service.Session{
									SessionActor:    "testuser-123",
									SessionEmulator: "",
								},
								Username: "testuser-123",
								ID:       testCredentialID.String(),
							},
							tid: "tid-aaaaaaaaaaaaaaaaaaaa",
						},
					},
				},
			},
		},
		{
			name: "1 create, actor not owner",
			fields: fields{
				MockEventOut: MockEventOut{
					ReturnedError: nil,
					EventRespChan: make(chan EventResponse, 1),
				},
				MockStorage: func() *portsmocks.PersistentStoragePort {
					storage := portsmocks.NewPersistentStoragePort(t)
					return storage
				}(),
			},
			args: args{
				ctxTimeout: defaultEventCtxTimeout,
				msgPairs: []EventMsgPair{
					{
						Request: types.EventMessage{
							Type: string(service.EventCredentialAddRequested),
							TID:  "tid-aaaaaaaaaaaaaaaaaaaa",
							Obj: service.CredentialCreateRequest{
								Session: service.Session{
									SessionActor:    "testuser-123",
									SessionEmulator: "",
								},
								Username:    "diff-testuser-123",
								Name:        "cred-name-123",
								Value:       "{}",
								Type:        "type123",
								ID:          "cred-id-123",
								Description: "",
							},
						},
						ExpectedResponse: EventResponse{
							eventType: service.EventCredentialAddError,
							data: service.CredentialCreateResponse{
								Session: service.Session{
									SessionActor:    "testuser-123",
									SessionEmulator: "",
									ErrorType:       "unauthorized access - only owner is allowed to create",
									ErrorMessage:    "",
									ServiceError:    service.NewCacaoUnauthorizedError("only owner is allowed to create").GetBase(),
								},
								Username: "diff-testuser-123",
								ID:       testCredentialID.String(),
							},
							tid: "tid-aaaaaaaaaaaaaaaaaaaa",
						},
					},
				},
			},
		},
		{
			name: "1 create, bad credential, no actor",
			fields: fields{
				MockEventOut: MockEventOut{
					ReturnedError: nil,
					EventRespChan: make(chan EventResponse, 1),
				},
				MockStorage: func() *portsmocks.PersistentStoragePort {
					storage := portsmocks.NewPersistentStoragePort(t)
					return storage
				}(),
			},
			args: args{
				ctxTimeout: defaultEventCtxTimeout,
				msgPairs: []EventMsgPair{
					{
						Request: types.EventMessage{
							Type: string(service.EventCredentialAddRequested),
							TID:  "tid-aaaaaaaaaaaaaaaaaaaa",
							Obj: service.CredentialCreateRequest{
								Session: service.Session{
									SessionActor:    "", // empty actor
									SessionEmulator: "",
								},
								Username:    "",
								Name:        "cred-name-123",
								Value:       "{}",
								Type:        "type123",
								ID:          "cred-id-123",
								Description: "",
							},
						},
						ExpectedResponse: EventResponse{
							eventType: service.EventCredentialAddError,
							data: service.CredentialCreateResponse{
								Session: service.Session{
									SessionActor:    "",
									SessionEmulator: "",
									ErrorType:       "invalid parameter error - username(owner) of the credential cannot be empty",
									ErrorMessage:    "",
									ServiceError:    service.NewCacaoInvalidParameterError("username(owner) of the credential cannot be empty").GetBase(),
								},
								Username: "",
								ID:       testCredentialID.String(),
							},
							tid: "tid-aaaaaaaaaaaaaaaaaaaa",
						},
					},
				},
			},
		},
		{
			name: "1 create, bad credential, bad cred value (type ssh)",
			fields: fields{
				MockEventOut: MockEventOut{
					ReturnedError: nil,
					EventRespChan: make(chan EventResponse, 1),
				},
				MockStorage: func() *portsmocks.PersistentStoragePort {
					storage := portsmocks.NewPersistentStoragePort(t)
					return storage
				}(),
			},
			args: args{
				ctxTimeout: defaultEventCtxTimeout,
				msgPairs: []EventMsgPair{
					{
						Request: types.EventMessage{
							Type: string(service.EventCredentialAddRequested),
							TID:  "tid-aaaaaaaaaaaaaaaaaaaa",
							Obj: service.CredentialCreateRequest{
								Session: service.Session{
									SessionActor:    "testuser-123",
									SessionEmulator: "",
								},
								Username:    "testuser-123",
								Name:        "cred-name-123",
								Value:       "{}", // bad cred value
								Type:        "ssh",
								ID:          "cred-id-123",
								Description: "",
							},
						},
						ExpectedResponse: EventResponse{
							eventType: service.EventCredentialAddError,
							data: service.CredentialCreateResponse{
								Session: service.Session{
									SessionActor:    "testuser-123",
									SessionEmulator: "",
									ErrorType:       "invalid parameter error - ssh: no key found",
									ErrorMessage:    "",
									ServiceError:    service.NewCacaoInvalidParameterError("ssh: no key found").GetBase(),
								},
								Username: "testuser-123",
								ID:       testCredentialID.String(),
							},
							tid: "tid-aaaaaaaaaaaaaaaaaaaa",
						},
					},
				},
			},
		},
		{
			name: "1 create error",
			fields: fields{
				MockEventOut: MockEventOut{
					ReturnedError: nil,
					EventRespChan: make(chan EventResponse, 1),
				},
				MockStorage: func() *portsmocks.PersistentStoragePort {
					storage := portsmocks.NewPersistentStoragePort(t)
					storage.On("List", types.ListQueryData{
						Username: "testuser-123",
					}).Return([]service.CredentialModel{}, nil).Once()
					storage.On("Create", service.CredentialModel{
						Session: service.Session{
							SessionActor:    "testuser-123",
							SessionEmulator: "",
						},
						Username:    "testuser-123",
						Name:        "cred-name-123",
						Value:       "{}",
						Type:        "type123",
						ID:          testCredentialID.String(),
						Description: "",
					}).Return(fmt.Errorf("create operation failed")).Once()
					return storage
				}(),
			},
			args: args{
				ctxTimeout: defaultEventCtxTimeout,
				msgPairs: []EventMsgPair{
					{
						Request: types.EventMessage{
							Type: string(service.EventCredentialAddRequested),
							TID:  "tid-aaaaaaaaaaaaaaaaaaaa",
							Obj: service.CredentialCreateRequest{
								Session: service.Session{
									SessionActor:    "testuser-123",
									SessionEmulator: "",
								},
								Username:    "testuser-123",
								Name:        "cred-name-123",
								Value:       "{}",
								Type:        "type123",
								ID:          "cred-id-123",
								Description: "",
							},
						},
						ExpectedResponse: EventResponse{
							eventType: service.EventCredentialAddError,
							data: service.CredentialCreateResponse{
								Session: service.Session{
									SessionActor:    "testuser-123",
									SessionEmulator: "",
									ErrorType:       "create operation failed",
									ErrorMessage:    "",
									ServiceError:    service.NewCacaoGeneralError("create operation failed").GetBase(),
								},
								Username: "testuser-123",
								ID:       testCredentialID.String(),
							},
							tid: "tid-aaaaaaaaaaaaaaaaaaaa",
						},
					},
				},
			},
		},
		{
			name: "1 create, duplicate name",
			fields: fields{
				MockEventOut: MockEventOut{
					ReturnedError: nil,
					EventRespChan: make(chan EventResponse, 1),
				},
				MockStorage: func() *portsmocks.PersistentStoragePort {
					storage := portsmocks.NewPersistentStoragePort(t)
					storage.On("List", types.ListQueryData{
						Username: "testuser-123",
					}).Return([]service.CredentialModel{
						{
							Name: "cred-name-123", // name already exists
						},
					}, nil).Once()
					return storage
				}(),
			},
			args: args{
				ctxTimeout: defaultEventCtxTimeout,
				msgPairs: []EventMsgPair{
					{
						Request: types.EventMessage{
							Type: string(service.EventCredentialAddRequested),
							TID:  "tid-aaaaaaaaaaaaaaaaaaaa",
							Obj: service.CredentialCreateRequest{
								Session: service.Session{
									SessionActor:    "testuser-123",
									SessionEmulator: "",
								},
								Username:    "testuser-123",
								Name:        "cred-name-123",
								Value:       "{}",
								Type:        "type123",
								ID:          "cred-id-123",
								Description: "",
							},
						},
						ExpectedResponse: EventResponse{
							eventType: service.EventCredentialAddError,
							data: service.CredentialCreateResponse{
								Session: service.Session{
									SessionActor:    "testuser-123",
									SessionEmulator: "",
									ErrorType:       "invalid parameter error - credential name is not unique",
									ErrorMessage:    "",
									ServiceError:    service.NewCacaoInvalidParameterError("credential name is not unique").GetBase(),
								},
								Username: "testuser-123",
								ID:       testCredentialID.String(),
							},
							tid: "tid-aaaaaaaaaaaaaaaaaaaa",
						},
					},
				},
			},
		},
		{
			name: "1 delete",
			fields: fields{
				MockEventOut: MockEventOut{
					ReturnedError: nil,
					EventRespChan: make(chan EventResponse, 1),
				},
				MockStorage: func() *portsmocks.PersistentStoragePort {
					storage := portsmocks.NewPersistentStoragePort(t)
					storage.On("Delete", service.CredentialModel{ // same as request
						Session: service.Session{
							SessionActor:    "testuser-123",
							SessionEmulator: "",
						},
						Username: "testuser-123",
						ID:       "cred-id-123",
					}).Return(nil).Once()
					return storage
				}(),
			},
			args: args{
				ctxTimeout: defaultEventCtxTimeout,
				msgPairs: []EventMsgPair{
					{
						Request: types.EventMessage{
							Type: string(service.EventCredentialDeleteRequested),
							TID:  "tid-aaaaaaaaaaaaaaaaaaaa",
							Obj: service.CredentialDeleteRequest{
								Session: service.Session{
									SessionActor:    "testuser-123",
									SessionEmulator: "",
								},
								Username: "testuser-123",
								ID:       "cred-id-123",
							},
						},
						ExpectedResponse: EventResponse{
							eventType: service.EventCredentialDeleted,
							data: service.CredentialDeleteResponse{
								Session: service.Session{
									SessionActor:    "testuser-123",
									SessionEmulator: "",
								},
								Username: "testuser-123",
								ID:       "cred-id-123",
							},
							tid: "tid-aaaaaaaaaaaaaaaaaaaa",
						},
					},
				},
			},
		},
		{
			name: "1 delete, actor not owner",
			fields: fields{
				MockEventOut: MockEventOut{
					ReturnedError: nil,
					EventRespChan: make(chan EventResponse, 1),
				},
				MockStorage: func() *portsmocks.PersistentStoragePort {
					storage := portsmocks.NewPersistentStoragePort(t)
					return storage
				}(),
			},
			args: args{
				ctxTimeout: defaultEventCtxTimeout,
				msgPairs: []EventMsgPair{
					{
						Request: types.EventMessage{
							Type: string(service.EventCredentialDeleteRequested),
							TID:  "tid-aaaaaaaaaaaaaaaaaaaa",
							Obj: service.CredentialDeleteRequest{
								Session: service.Session{
									SessionActor:    "testuser-123",
									SessionEmulator: "",
								},
								Username: "diff-testuser-123", // actor not owner
								ID:       "cred-id-123",
							},
						},
						ExpectedResponse: EventResponse{
							eventType: service.EventCredentialDeleteError,
							data: service.CredentialDeleteResponse{
								Session: service.Session{
									SessionActor:    "testuser-123",
									SessionEmulator: "",
									ErrorType:       "unauthorized access - only owner is allowed to delete",
									ErrorMessage:    "",
									ServiceError:    service.NewCacaoUnauthorizedError("only owner is allowed to delete").GetBase(),
								},
								Username: "diff-testuser-123",
								ID:       "cred-id-123",
							},
							tid: "tid-aaaaaaaaaaaaaaaaaaaa",
						},
					},
				},
			},
		},
		{
			name: "1 delete, empty username",
			fields: fields{
				MockEventOut: MockEventOut{
					ReturnedError: nil,
					EventRespChan: make(chan EventResponse, 1),
				},
				MockStorage: func() *portsmocks.PersistentStoragePort {
					storage := portsmocks.NewPersistentStoragePort(t)
					return storage
				}(),
			},
			args: args{
				ctxTimeout: defaultEventCtxTimeout,
				msgPairs: []EventMsgPair{
					{
						Request: types.EventMessage{
							Type: string(service.EventCredentialDeleteRequested),
							TID:  "tid-aaaaaaaaaaaaaaaaaaaa",
							Obj: service.CredentialDeleteRequest{
								Session: service.Session{
									SessionActor:    "", // actor is the same as username, but empty
									SessionEmulator: "",
								},
								Username: "",
								ID:       "cred-id-123",
							},
						},
						ExpectedResponse: EventResponse{
							eventType: service.EventCredentialDeleteError,
							data: service.CredentialDeleteResponse{
								Session: service.Session{
									SessionActor:    "",
									SessionEmulator: "",
									ErrorType:       "invalid parameter error - username is empty",
									ErrorMessage:    "",
									ServiceError:    service.NewCacaoInvalidParameterError("username is empty").GetBase(),
								},
								Username: "",
								ID:       "cred-id-123",
							},
							tid: "tid-aaaaaaaaaaaaaaaaaaaa",
						},
					},
				},
			},
		},
		{
			name: "1 delete error",
			fields: fields{
				MockEventOut: MockEventOut{
					ReturnedError: nil,
					EventRespChan: make(chan EventResponse, 1),
				},
				MockStorage: func() *portsmocks.PersistentStoragePort {
					storage := portsmocks.NewPersistentStoragePort(t)
					storage.On("Delete", service.CredentialModel{ // same as request
						Session: service.Session{
							SessionActor:    "testuser-123",
							SessionEmulator: "",
						},
						Username: "testuser-123",
						ID:       "cred-id-123",
					}).Return(fmt.Errorf("delete operation failed")).Once()
					return storage
				}(),
			},
			args: args{
				ctxTimeout: defaultEventCtxTimeout,
				msgPairs: []EventMsgPair{
					{
						Request: types.EventMessage{
							Type: string(service.EventCredentialDeleteRequested),
							TID:  "tid-aaaaaaaaaaaaaaaaaaaa",
							Obj: service.CredentialDeleteRequest{
								Session: service.Session{
									SessionActor:    "testuser-123",
									SessionEmulator: "",
								},
								Username: "testuser-123",
								ID:       "cred-id-123",
							},
						},
						ExpectedResponse: EventResponse{
							eventType: service.EventCredentialDeleteError,
							data: service.CredentialDeleteResponse{
								Session: service.Session{
									SessionActor:    "testuser-123",
									SessionEmulator: "",
									ErrorType:       "delete operation failed",
									ErrorMessage:    "",
									ServiceError:    service.NewCacaoGeneralError("delete operation failed").GetBase(),
								},
								Username: "testuser-123",
								ID:       "cred-id-123",
							},
							tid: "tid-aaaaaaaaaaaaaaaaaaaa",
						},
					},
				},
			},
		},
		{
			name: "2 delete, same user diff ID",
			fields: fields{
				MockEventOut: MockEventOut{
					ReturnedError: nil,
					EventRespChan: make(chan EventResponse, 2),
				},
				MockStorage: func() *portsmocks.PersistentStoragePort {
					storage := portsmocks.NewPersistentStoragePort(t)
					storage.On("Delete", service.CredentialModel{
						Session: service.Session{
							SessionActor:    "testuser-123",
							SessionEmulator: "",
						},
						Username: "testuser-123",
						ID:       "cred-id-123",
					}).Return(nil).Once()
					storage.On("Delete", service.CredentialModel{
						Session: service.Session{
							SessionActor:    "testuser-123",
							SessionEmulator: "",
						},
						Username: "testuser-123",
						ID:       "diff-cred-id-123",
					}).Return(nil).Once()
					return storage
				}(),
			},
			args: args{
				ctxTimeout: defaultEventCtxTimeout,
				msgPairs: []EventMsgPair{
					{
						Request: types.EventMessage{
							Type: string(service.EventCredentialDeleteRequested),
							TID:  "tid-aaaaaaaaaaaaaaaaaaaa",
							Obj: service.CredentialDeleteRequest{
								Session: service.Session{
									SessionActor:    "testuser-123",
									SessionEmulator: "",
								},
								Username: "testuser-123",
								ID:       "cred-id-123",
							},
						},
						ExpectedResponse: EventResponse{
							eventType: service.EventCredentialDeleted,
							data: service.CredentialDeleteResponse{
								Session: service.Session{
									SessionActor:    "testuser-123",
									SessionEmulator: "",
								},
								Username: "testuser-123",
								ID:       "cred-id-123",
							},
							tid: "tid-aaaaaaaaaaaaaaaaaaaa",
						},
					},
					{
						Request: types.EventMessage{
							Type: string(service.EventCredentialDeleteRequested),
							TID:  "tid-aaaaaaaaaaaaaaaaaaaa",
							Obj: service.CredentialDeleteRequest{
								Session: service.Session{
									SessionActor:    "testuser-123",
									SessionEmulator: "",
								},
								Username: "testuser-123",
								ID:       "diff-cred-id-123",
							},
						},
						ExpectedResponse: EventResponse{
							eventType: service.EventCredentialDeleted,
							data: service.CredentialDeleteResponse{
								Session: service.Session{
									SessionActor:    "testuser-123",
									SessionEmulator: "",
								},
								Username: "testuser-123",
								ID:       "diff-cred-id-123",
							},
							tid: "tid-aaaaaaaaaaaaaaaaaaaa",
						},
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			log.Warn(tt.name)
			d := Domain{
				EventsOut:       tt.fields.MockEventOut,
				VaultOut:        tt.fields.MockStorage,
				CredIDGenerator: testCredIDGenerator,
			}

			ctx, _ := context.WithTimeout(context.Background(), tt.args.ctxTimeout)

			var eventChan = pushEventMsgToChannel(tt.args.msgPairs)

			var wg sync.WaitGroup
			wg.Add(1)

			d.processEventWorker(ctx, eventChan, &wg)

			checkEventResponseInChannel(t, tt.args.msgPairs, tt.fields.MockEventOut.EventRespChan)

			tt.fields.MockStorage.AssertExpectations(t)
		})
	}
}

func pushEventMsgToChannel(msgPairs []EventMsgPair) chan types.EventMessage {
	var eventChan = make(chan types.EventMessage, len(msgPairs))
	for _, pair := range msgPairs {
		eventChan <- pair.Request
	}
	return eventChan
}

// check reply in each response channel
func checkEventResponseInChannel(t *testing.T, msgPairs []EventMsgPair, respChan <-chan EventResponse) {
	for i, msgPair := range msgPairs {
		select {
		case resp := <-respChan:
			assert.Equalf(t, msgPair.ExpectedResponse, resp, "msg %d didnt match", i)
		case <-time.After(time.Millisecond * 100):
			assert.Fail(t, "timed out, cannot check response")
		}
	}
}

// MockEventOut is a simple mock obj for OutgoingEventsPort that publish events into a channel
type MockEventOut struct {
	ReturnedError error
	EventRespChan chan EventResponse
}

// Init do nothing
func (m MockEventOut) Init(c types.Config) {}

// PublishEvent ...
func (m MockEventOut) PublishEvent(ev common.EventType, dcredential interface{}, transactionID common.TransactionID) error {
	resp := EventResponse{
		eventType: ev,
		data:      dcredential,
		tid:       transactionID,
	}
	select {
	case m.EventRespChan <- resp:
	default:
		panic("response channel is full, this is an extra event being published")
	}
	return m.ReturnedError
}

var testCredentialID = common.NewID("cred")

func testCredIDGenerator() common.ID {
	return testCredentialID
}
