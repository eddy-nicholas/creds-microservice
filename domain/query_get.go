package domain

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/creds-microservice/ports"
	"gitlab.com/cyverse/creds-microservice/types"
	"reflect"
)

func handleGetQuery(query types.QueryMessage, storage ports.PersistentStoragePort) {
	logger := log.WithFields(log.Fields{
		"package":  "domain",
		"function": "handleGetQuery",
	})
	request, ok := query.Obj.(service.CredentialGetRequest)
	if !ok {
		panic("the data in QueryMessage should be of type service.CredentialModel, " + reflect.TypeOf(query.Obj).Name())
	}
	logger = logger.WithFields(log.Fields{
		"actor":    request.SessionActor,
		"emulator": request.SessionEmulator,
		"username": request.Username,
		"id":       request.ID,
	})
	err := validateGetQuery(request)
	if err != nil {
		logger.WithError(err).Error("get query is invalid")
		replyQuery(query, errorReplyForGetQuery(request, err))
		return
	}

	logger.Debug("domain is getting data for user")

	fetchedCred, err := storage.Get(service.CredentialModel{
		Session: service.Session{
			SessionActor:    request.GetSessionActor(),
			SessionEmulator: request.GetSessionEmulator(),
		},
		Username: request.Username,
		ID:       request.ID,
	})
	if err != nil {
		logger.WithError(err).Error("fail to fetch cred")
		replyQuery(query, errorReplyForGetQuery(request, err))
		return
	} else {
		logger.WithField("value", fetchedCred.GetValue()).Trace("fetched credential")
	}
	replyQuery(query, successReplyForGetQuery(request.Session, fetchedCred))
}

func validateGetQuery(request service.CredentialGetRequest) error {
	err := getQueryOnlyAllowOwnerAccess(request)
	if err != nil {
		return err
	}
	if request.Username == "" {
		return service.NewCacaoInvalidParameterError("username is empty")
	}
	if request.ID == "" {
		return service.NewCacaoInvalidParameterError("credential ID is empty")
	}
	if len(request.Username) > types.MaxCredUsernameLength {
		return service.NewCacaoInvalidParameterError("username too long")
	}
	if len(request.ID) > types.MaxCredIDLength {
		return service.NewCacaoInvalidParameterError("credential ID too long")
	}
	return nil
}

func getQueryOnlyAllowOwnerAccess(queryRequest service.CredentialGetRequest) error {
	if queryRequest.GetSessionActor() != queryRequest.Username {
		return service.NewCacaoUnauthorizedError("only owner of the credential is allowed to access")
	}
	return nil
}

func successReplyForGetQuery(requestSession service.Session, fetched service.CredentialModel) types.QueryMessage {
	fetched.SessionActor = requestSession.GetSessionActor()
	fetched.SessionEmulator = requestSession.GetSessionEmulator()
	return types.QueryMessage{
		Type:     service.NatsSubjectCredentialsGet + types.QueryReplyTypePostfix,
		Obj:      service.CredentialGetReply(fetched),
		Response: nil,
	}
}

func errorReplyForGetQuery(request service.CredentialGetRequest, err error) types.QueryMessage {
	return types.QueryMessage{
		Type: service.NatsSubjectCredentialsGet + types.QueryReplyTypePostfix,
		Obj: service.CredentialGetReply{
			Session: service.Session{
				SessionActor:    request.SessionActor,
				SessionEmulator: request.SessionEmulator,
				ErrorType:       err.Error(),
				ErrorMessage:    "",
				ServiceError:    service.CacaoErrorBase{},
			},
			Username: request.Username,
			ID:       request.ID,
		},
		Response: nil,
	}
}

func replyQuery(query types.QueryMessage, reply types.QueryMessage) {
	query.Response <- reply
}
