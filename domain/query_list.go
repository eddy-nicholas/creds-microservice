package domain

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/creds-microservice/ports"
	"gitlab.com/cyverse/creds-microservice/types"
	"reflect"
)

func handleListQuery(query types.QueryMessage, storage ports.PersistentStoragePort) {
	logger := log.WithFields(log.Fields{
		"package":  "domain",
		"function": "handleListQuery",
	})
	request, ok := query.Obj.(service.CredentialListRequest)
	if !ok {
		panic("the data in QueryMessage should be of type types.ListQueryData, " + reflect.TypeOf(query.Obj).Name())
	}
	logger = logger.WithFields(log.Fields{
		"actor":    request.GetSessionActor(),
		"emulator": request.GetSessionEmulator(),
		"username": request.Username,
	})
	err := validateListQuery(request)
	if err != nil {
		replyQuery(query, errorReplyForListQuery(request, err))
		return
	}

	logger.Debug("domain is getting data for user")

	credList, err := storage.List(types.ListQueryData{
		Actor:    request.GetSessionActor(),
		Emulator: request.GetSessionEmulator(),
		Username: request.Username,
	})
	if err != nil {
		replyQuery(query, errorReplyForListQuery(request, err))
		return
	}
	replyQuery(query, successReplyForListQuery(request, credList))
}

func validateListQuery(request service.CredentialListRequest) error {
	err := listQueryOnlyAllowOwnerAccess(request)
	if err != nil {
		return err
	}
	if request.Username == "" {
		return service.NewCacaoInvalidParameterError("username is empty")
	}
	if len(request.Username) > types.MaxCredUsernameLength {
		return service.NewCacaoInvalidParameterError("username too long")
	}
	return nil
}

func listQueryOnlyAllowOwnerAccess(queryRequest service.CredentialListRequest) error {
	if queryRequest.GetSessionActor() != queryRequest.Username {
		return service.NewCacaoUnauthorizedError("only owner of the credential is allowed to access")
	}
	return nil
}

func successReplyForListQuery(request service.CredentialListRequest, fetched []service.CredentialModel) types.QueryMessage {
	return types.QueryMessage{
		Type: service.NatsSubjectCredentialsList + types.QueryReplyTypePostfix,
		Obj: service.CredentialListReply{
			Session: service.Session{
				SessionActor:    request.GetSessionActor(),
				SessionEmulator: request.GetSessionEmulator(),
			},
			Username: request.Username,
			List:     fetched,
		},
		Response: nil,
	}
}

func errorReplyForListQuery(request service.CredentialListRequest, err error) types.QueryMessage {
	return types.QueryMessage{
		Type: service.NatsSubjectCredentialsList + types.QueryReplyTypePostfix,
		Obj: service.CredentialListReply{
			Session: service.Session{
				SessionActor:    request.GetSessionActor(),
				SessionEmulator: request.GetSessionEmulator(),
				ErrorType:       err.Error(),
			},
			Username: request.Username,
			List:     nil,
		},
		Response: nil,
	}
}
