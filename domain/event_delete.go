package domain

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/creds-microservice/ports"
	"gitlab.com/cyverse/creds-microservice/types"
	"reflect"
)

func handleDeleteEvent(event types.EventMessage, storage ports.PersistentStoragePort, eventOut ports.OutgoingEventsPort) {
	logger := log.WithFields(log.Fields{
		"package":  "domain",
		"function": "handleDeleteEvent",
	})
	request, ok := event.Obj.(service.CredentialDeleteRequest)
	if !ok {
		panic("the data in EventMessage should be of type service.CredentialModel, " + reflect.TypeOf(event.Obj).Name())
	}
	logger = logger.WithFields(log.Fields{
		"actor":    request.SessionActor,
		"emulator": request.SessionEmulator,
		"username": request.Username,
		"id":       request.ID,
	})
	err := validateDeletionRequest(request)
	if err != nil {
		publishDeleteErrorEvent(logger, eventOut, request, err, event.TID)
		return
	}

	logger.Debug("domain is deleting a secret")
	err = storage.Delete(service.CredentialModel{
		Session: service.Session{
			SessionActor:    request.GetSessionActor(),
			SessionEmulator: request.GetSessionEmulator(),
		},
		Username: request.Username,
		ID:       request.ID,
	})
	if err != nil {
		publishDeleteErrorEvent(logger, eventOut, request, err, event.TID)
		return
	}
	publishDeleteSuccessEvent(logger, eventOut, request, event.TID)
}

func validateDeletionRequest(request service.CredentialDeleteRequest) error {
	if request.GetSessionActor() != request.Username {
		return service.NewCacaoUnauthorizedError("only owner is allowed to delete")
	}
	if request.Username == "" {
		return service.NewCacaoInvalidParameterError("username is empty")
	}
	if request.ID == "" {
		return service.NewCacaoInvalidParameterError("credential ID is empty")
	}
	if len(request.Username) > types.MaxCredUsernameLength {
		return service.NewCacaoInvalidParameterError("username too long")
	}
	if len(request.ID) > types.MaxCredIDLength {
		return service.NewCacaoInvalidParameterError("credential ID too long")
	}
	return nil
}

func publishDeleteSuccessEvent(logger *log.Entry, eventOut ports.OutgoingEventsPort, request service.CredentialDeleteRequest, tid common.TransactionID) {
	err := eventOut.PublishEvent(service.EventCredentialDeleted, service.CredentialDeleteResponse{
		Session: service.Session{
			SessionActor:    request.GetSessionActor(),
			SessionEmulator: request.GetSessionEmulator(),
		},
		Username: request.Username,
		ID:       request.ID,
	}, tid)
	if err != nil {
		logger.WithError(err).Error("fail to publish event")
	}
}

func publishDeleteErrorEvent(logger *log.Entry, eventOut ports.OutgoingEventsPort, request service.CredentialDeleteRequest, err error, tid common.TransactionID) {
	svcErr, ok := err.(service.CacaoError)
	if !ok {
		svcErr = service.NewCacaoGeneralError(err.Error())
	}
	eventOutErr := eventOut.PublishEvent(service.EventCredentialDeleteError, service.CredentialDeleteResponse{
		Session: service.Session{
			SessionActor:    request.GetSessionActor(),
			SessionEmulator: request.GetSessionEmulator(),
			ErrorType:       err.Error(), // populate the old error type, this won't be necessary after svc client is updated to use svc error
			ErrorMessage:    "",
			ServiceError:    svcErr.GetBase(), // populate the service error
		},
		Username: request.Username,
		ID:       request.ID,
	}, tid)
	if eventOutErr != nil {
		logger.WithError(eventOutErr).Error("fail to publish event")
	}
}
