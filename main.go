package main

import (
	"context"
	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/creds-microservice/adapters"
	"gitlab.com/cyverse/creds-microservice/domain"
	"gitlab.com/cyverse/creds-microservice/ports"
	"gitlab.com/cyverse/creds-microservice/types"
	"time"
)

func main() {

	log.SetReportCaller(true)

	log.Debug("main: setting up cancelable context")
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	var config = readConfigFromEnv()

	queryAdapter := adapters.NewNATSQueryAdapter()
	queryAdapter.Init(config)
	defer queryAdapter.Close()

	stanAdapter := adapters.NewStanEventAdapter()
	stanAdapter.Init(config)
	defer stanAdapter.Close()

	postgresStorage := adapters.NewPostgresStorage(utcTimeSrc)
	postgresStorage.Init(config)
	defer postgresStorage.Close()

	dmain := domain.Domain{
		QueryIn:         queryAdapter,
		EventsIn:        stanAdapter,
		EventsOut:       stanAdapter,
		VaultOut:        postgresStorage,
		CredIDGenerator: ports.GenerateCredentialID,
	}
	dmain.Init(config)
	dmain.Start(ctx)
}

func readConfigFromEnv() types.Config {
	var config types.Config
	err := envconfig.Process("", &config)
	if err != nil {
		log.WithError(err).Fatal("fail to read config from env var")
	}
	config.ProcessDefaults()
	return config
}

func utcTimeSrc() time.Time {
	return time.Now().UTC()
}
